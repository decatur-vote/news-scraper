<?php
/**
 *
 * Get all entries in the fair use index
 *
 * @param Source2 $source
 * @param \Taeluf\PHTML $doc
 *
 * @return array<
 */

$keys = [];
$tr_list = $doc->xpath('//table/thead/tr');
foreach ($tr_list[0]->children as $td){
    if ($td->nodeName!='th')continue;
    $keys[] = property_exists($td,'innerText') ? $td->innerText : $td->textContent;
}

$tr_list = $doc->xpath('//table/tbody/tr');

$data = [];
foreach ($tr_list as $tr_node){
    $tr = new \Taeluf\PHTML(''.$tr_node);
    $row = [];
    $tds = $tr->xpath('//td');
    $row['url'] = 'https://www.copyright.gov'.$tr->xpath('//td/a')[0]->href;
    foreach ($tds as $index => $td){
        $key = $keys[$index];
        $row[$key] = property_exists($td,'innerText') ? $td->innerText : $td->textContent;
    }
    $data[$row['url']] = new \DecaturVote\NewsScraper\Article2($row);
}

return $data;

