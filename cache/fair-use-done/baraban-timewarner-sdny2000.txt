Baraban v. Time Warner, Inc.,
No. 99 Civ. 1569-JSM (S.D.N.Y. April 6, 2000)

Year

2000

Court

United States District Court for the Southern District of New York

Key
Facts

Plaintiff Joe Baraban owned the copyright in a photograph of a dairy farmer
posing with a cow ina field of yellow clover. The U.S. Council for Energy
Awareness (USCEA) used the photograph with the author’s permission in an
advertisement supporting nuclear energy that ran in several national
newspapers and magazines. Defendant Time Warner, Inc. published
defendant Gerald Celente’s book, Trends 2000: How to Prepare for and
Profit from the Changes of the 21" Century (Trends). The book included the
advertisement along with commentary criticizing the nuclear energy industry.
Defendants only slightly altered the advertisement by making it black and
white, shrinking it to fit the page layout, and cropping it to exclude USCEA’s
information. Defendants alleged that they were unable to get permission
because USCEA apparently had ceased to exist, and they could not locate
plaintiff. Plaintiff Baraban claimed that using his photograph in the book
without permission infringed his copyright.

Issue

Whether the unauthorized reproduction of an advertisement including a
photographer’s image, in a book criticizing the underlying subject matter of
the advertisement, constituted fair use.

Holding

The court held that defendants’ use of the advertisement in Trends qualified
as fair use. The court concluded that defendants’ use was clearly for purposes
of criticism and comment as the book’s author used the photo to show the
nature of the nuclear energy industry’s advertisements and provided
additional commentary concerning those advertisements. The court rejected
plaintiff’s argument that the use was satirical, not for parody. According to
the court, Trends criticized the photo, at least in part, because it highlighted a
particular view —the “sunny” view of nuclear power— that the industry
intended to promote. The court found the second factor was neutral because
while the purposeful setting, angles, and other artistic elements made the
photo highly creative and weighed in plaintiff’s favor, the context of its use
was as part of an advertisement, and this weighed in favor of the defendants.
For the third factor, the court held that defendants’ slight modifications were
enough to give the reader the sense of the campaign without completely
copying the work exactly. Finally, the court held that the market value of the
photograph was not harmed by the publication in Trends, since it was unlikely
that a black-and-white, comparatively miniscule version would negatively
affect a market, the existence of which was already “dubious.”

Tags

Second Circuit; Photograph; Review/Commentary; Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

