Katz v. Google, Inc.
No. 14-14525 (11th Cir. Sept. 17, 2015)

Year

2015

Court

United States Court of Appeals for the Eleventh Circuit

Key
Facts

Plaintiff Katz, a “commercial real estate tycoon,” owns the copyright in a
photograph of himself that he has characterized as “compromising” and
“embarrassing.” Defendant Chevaldina, “a disgruntled former tenant in one
of Katz’s shopping centers,” found the photograph through a Google image
search and reproduced it in several blog articles “devoted to sharply
criticizing Katz” and his business practices. Katz initiated an infringement
action, alleging direct infringement against defendant Chevaldina and
contributory infringement against Google Inc. (Katz later dropped his claim
against Google). At the district court level, the United States District Court
for the Southern District of Florida ruled that Chevaldina’s reproduction of
the photograph was fair use and entered summary judgment against Katz.

Issue

Whether defendant’s unauthorized reproduction of a photograph in blog posts
critical of the photograph’s subject qualifies as fair use.

Holding

The Eleventh Circuit ruled that Chevaldina’s reproduction of the photograph
in her blog posts was fair use. The court found that the first factor, the
purpose and character of the use, weighed in Chevaldina’s favor. It held that
Chevaldina’s reproduction was non-commercial, “primarily educational,” and
served the transformative purpose of criticizing and satirizing Katz’s
character. The court also weighed the second factor, the nature of the work,
in defendant’s favor, finding the photograph to be “primarily a factual work,”
explaining that it was “merely a candid shot in a public setting” and there was
no evidence that the photographer “attempted to convey ideas, emotions, or in
any way influence Katz’s pose, expression, or clothing.” The court found that
the third factor, the amount of the work used, was neutral. Despite
Chevaldina having reproduced the entire photograph in many of her blog
posts, the court held that copying any less of the photograph “would have
made the picture useless to [Chevaldina’s] story.” For the fourth factor, the
effect of the use on the potential market for the work, the court found that
“Ts]ince there is no evidence Chevaldina's use of the [p]hoto had or would
have any impact upon any actual or potential market, the fourth factor weighs
in favor of fair use.”

Tags

Eleventh Circuit; Education/Scholarship/Research; Photograph;
Review/Commentary

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

