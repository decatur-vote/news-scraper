Solid Oak Sketches, LLC v. 2K Games, Inc.,

No. 16-CV-724-LTS-SDA, 2020 U.S. Dist. LEXIS 53287 (S.D.N.Y. Mar. 26, 2020)

Year

2020

Court

United States District Court for the Southern District of New York

Key Facts

Defendants, 2K Games, Inc. and Take Two Entertainment, are videogame
developers that annually update and release the NBA 2K basketball simulation
video game that depicts NBA basketball players, including their tattoos, with
realistic renderings. Plaintiff, Solid Oak Sketches, LLC, alleged that Defendants
infringed its copyrighted works— five tattoo designs inked on three NBA players—
by publicly displaying renderings of these tattoos in multiple versions of the video
game. Defendants asserted several counterclaims, including one seeking a
declaration that their use of the tattoos was a fair use. After the court denied a
motion for judgment on the pleadings, Defendants moved for summary judgment.

Issue

Whether inclusion of a tattoo design in a videogame to realistically depict the
likeness of an athlete who is inked with that tattoo constitutes fair use.

Holding

The court decided that the first factor, the purpose and character of the use, weighed
in favor of Defendants. First, the court held that Defendants transformed the tattoos
by using them for “general recognizability” of players in the game, rather than the
original purpose for which they were created, which was to allow the players to
express themselves through body art. Further, due to the reduced size of the tattoos
and myriad of other effects, the expressive details of the tattoos were clearly visible
only when users were choosing their players and not during the gameplay portion of
the game. Moreover, the tattoos constitute an inconsequential portion of the game,
appearing on three out of 400 players and comprising only 0.000286% to

0.00043 1% of total game data. Although the inclusion of the tattoos in a
commercial video games is a commercial use, the tattoos are not featured on
marketing materials and are incidental to the games’ commercial value. The court
determined the second factor, the nature of the copyrighted work, also favored
Defendants because the tattoos were previously published and the designs are more
“factual than expressive” because they were based on common motifs or
photographs, not uniquely “expressive” or “creative” designs. The court found the
third factor, the amount and substantiality of the work used, again favored
Defendants despite the tattoos having been copied in their entirety because it was
necessary to use the entirety of the tattoos to serve the purpose of accurately
depicting the players. Lastly, the court determined the fourth factor, the effect of the
use on the potential market for or value of the work, favored Defendants as well.
Plaintiff had conceded that the tattoos as featured in the game did not serve as a
substitute for use of the tattoos in any other medium. There was also no evidence
that there was a market for licensing tattoos for use in video games or other media
or that such a market is likely to develop. Weighing the factors together, the court
concluded that Defendants established that no reasonable fact finder could conclude
their use was not fair use, and granted summary judgment for Defendants.

Tags

Computer program; Film/Audiovisual; Painting/Drawing/Graphic.

Outcome

Fair use found.

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

