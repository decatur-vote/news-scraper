Ferdman vy. CBS Interactive Inc.
No. 17 Civ. 1317 (PGG) (S.D.N.Y. Sept. 24, 2018)

Year

2018

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff Steven Ferdman is a photographer who licenses his photographs to
publishers for a fee. GameSpot, an online publication of Defendant CBS
Interactive Inc., features information about video games and related entertainment.
While the movie Spider-Man: Homecoming was being filmed, Ferdman took
photographs of the production, including of the actor Tom Holland. Ferdman
uploaded the photographs to a licensing service. Around that same time, GameSpot
published two articles about the movie, each featuring at least one photograph
taken by Plaintiff. The first article included a photograph of Holland that was taken
by Plaintiff and that Holland had posted to his Instagram account (“Holland
Photograph”). The second article featured a gallery of images, including seven of
Plaintiff's photographs, which GameSpot claimed it believed “had been made
available for use by the media” (“Gallery Photographs’”’).

Issue

Whether a website’s use of copyrighted photographs in online articles is a fair use.

Holding

The District Court granted Plaintiff's motion for summary judgment on
Defendant’s fair use defense as to the Gallery Photographs. The court concluded
that the first factor, purpose and character of the use, weighed in favor of Ferdman.
The court rejected Defendant’s argument that its use was transformative because
the photographs were used for news reporting and commenting, holding that, rather
than providing new information or insight, the intention of the news article was
“simply [to show] that the photographs exist.” On the Holland photograph, the
court concluded that the use may have been “somewhat transformative” because
GameSpot “injected some ‘new meaning or message’ into the photograph by
reporting that the actor himself had posted the photograph and had provided
commentary on it,” but was not so transformative to constitute fair use as a matter
of law. Relatedly, the court also found that Defendant’s status as a for-profit entity
weighed against a finding of fair use but was not dispositive. The second factor,
nature of the copyrighted work, supported a fair use finding. Plaintiff was merely
“photographing the events around him as they occurred.” The court also noted that
the photographs had been published at the time of Defendant’s use. The third
factor, amount and substantiality of the portion used, was neutral. The court noted
that a photograph is inherently harder to excerpt than other types of works. The
final factor, effect of the use upon the potential market, weighed against fair use.
With respect to both categories of photographs, “Defendant’s use of the
photographs in its articles is a clear substitute for the market use of Plaintiff's
photographs.” Balancing the factors, the court held that “fair use is unavailable as a
matter of law” as to the Gallery Photographs. With respect to the Holland
Photograph, the court held that material questions of fact exist with respect to
whether the use was transformative, which could potentially tip the balance of
factors toward a finding of fair use.

Tags

Second Circuit, Photograph, Review/Commentary, Internet/Digitization, News
reporting

Outcome

Preliminary ruling, fair use not found, mixed result

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

