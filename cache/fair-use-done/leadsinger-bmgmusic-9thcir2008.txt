Leadsinger, Inc. v. BMG Music Publ’g,
512 F.3d 522 (9th Cir. 2008)

Year

2008

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Plaintiff Leadsinger, Inc. manufactured karaoke systems that projected lyrics
and still photographs onto a television screen to accompany songs in real
time. Defendant BMG Music Publishing issued plaintiff a compulsory
mechanical license to cover the musical compositions played on the karaoke
machines but also demanded that plaintiff pay a “lyric reprint fee” for copies
of lyrics occasionally included with the karaoke machines and a
“synchronization fee” for synchronizing the musical compositions in
audiovisual works. Plaintiff refused to pay the additional fees and sought a
judgment declaring that it did not have to pay them (1) because it was only
required to pay for a mechanical license or (2) because displaying and
printing the lyrics was fair use. Plaintiff appealed the district court’s order
dismissing the case for failure to state a claim.

Issue

Whether it was fair use for plaintiff to distribute copies of song lyrics in
conjunction with a karaoke system and synchronize the lyrics in an
audiovisual work without paying any fees beyond a compulsory license.

Holding

The court held that plaintiff’s publication and display of song lyrics was not
fair use. Rejecting the argument that a karaoke machine should be
considered a teaching tool, the court held that plaintiff’s use was non-
transformative and clearly commercial because the goal was to sell karaoke
devices. The court also held that plaintiff’s use of the song lyrics in their
entirety weighed against a finding of fair use. Regarding the effect of the use
on the potential market for defendant’s works, the court noted that when a use
is for commercial gain, the likelihood for market harm may be presumed.

Tags

Ninth Circuit; Education/Scholarship/Research; Film/Audiovisual; Music;
Photograph; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

