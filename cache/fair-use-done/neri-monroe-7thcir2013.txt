Neri v. Monroe,
No. 11-cv-429-slc (W.D. Wis. Feb. 26, 2014)

Year

2014

Court

United States District Court for the Western District of Wisconsin

Key
Facts

Defendants Melinda Monroe, president and co-owner of the Architectural
Building Arts (ABA) architectural and design firm, and others entered into an
agreement with plaintiff Quincy Neri, a glass-blowing artist, to create a
sculpture for an entryway ABA was designing. Plaintiff Neri installed special
lighting to create spiral shadows on the ceiling and walls, which Neri
contended was an integral feature of the artwork. Upon completion and
installation of the sculpture, defendants photographed “before” and “after”
images of the renovation with the entryway owner’s permission. Defendants
used these photographs to advertise and promote their firm and as part of their
application materials for “Contractor of the Year” awards. Neri brought this
action, alleging that defendants had taken and displayed photographs of the
sculpture without her authorization. Plaintiff appealed the district court’s
ruling in defendants’ favor to the Seventh Circuit Court of Appeals which
remanded the case back to the district court for further consideration.

Issue

Whether defendants’ unauthorized use of photographs of plaintiff’s glass
ceiling sculpture for the purposes of promoting the firm’s work constituted
fair use.

Holding

The court held that defendants’ use of the photographs constituted fair use.
The court concluded that the pictures were highly transformative images of
the glass ceiling because, while the sculpture was a three-dimensional,
impressionistic composition used to decorate an entryway, the photographs
were two-dimensional, realistic images of an interior space used to inform the
public about ABA’s work. Despite the commercial nature of the photographs
as advertisements and promotional tools, the court found that advertising
ABA’s design and construction was a new purpose, also weighing in favor of
fair use. The court concluded that the second factor favored plaintiff because
the sculpture was a highly creative work. However, on the third factor, the
court noted that because the sculpture was mounted onto another work, a
newly constructed barrel vault ceiling specifically designed to hold the
sculpture, it was therefore impossible to photograph the entire room/entryway
without also including the ceiling, and that the photographs only incidentally
included images of portions of the sculpture as a component of the home’s
overall design scheme. Finally, the court found that no evidence existed to
demonstrate that the photographs had an adverse market impact on either
Neri’s ability to be commissioned for future works or to sell photographs of
the sculpture.

Tags

Seventh Circuit; Photograph; Sculpture

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

