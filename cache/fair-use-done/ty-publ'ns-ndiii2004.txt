Ty, Inc. v. Publ'ns Int'l, Ltd.,
333 F. Supp. 2d 705 (N.D. Ill. 2004)

Year

2004

Court

United States District Court for the Northern District of Illinois

Key
Facts

Plaintiff Ty, Inc. created the popular Beanie Babies series of beanbag toy
animals and registered them as sculptural works. Plaintiff alleged that
defendant Publications International, Ltd. infringed its copyrights by
publishing several collector’s guides that included photographs of Beanie
Babies. The guides also contained descriptive text that was sometimes
critical of plaintiff. Defendant appealed the district court’s first ruling, which
was in plaintiff’s favor. On appeal, the circuit court remanded the case back
to the district court for further consideration.

Issue

Whether defendant’s unauthorized use of photographs of Beanie Babies in
collector’s guides was fair use.

Holding

On remand, the district court found defendant liable for infringement. The
court found that the defendant’s photographs of plaintiff’s copyrighted works
were non-transformative because both the toys and the photographs served
the same decorative, aesthetic purposes. The court also found that
defendant’s works were primarily commercial and the unauthorized use of the
photographs served no broader public purpose than to avoid paying the
customary price plaintiff charged for licensing images of its works.
Additionally, the court found that defendant copied more of the work than
was necessary to achieve the goal of comment or criticism of the works. The
court then determined that defendant’s books harmed the market for
plaintiff’s products or licensed derivatives by functioning as market
substitutes for these licensed works. The court then rejected defendant’s
argument that the photographs were necessary to create a competitive
collector’s guide because, even if defendant had a right to produce such a
guide, it still copied more of plaintiff’s work than was necessary.

Note: This case was before the court on remand from the U.S. Court of
Appeals for the Seventh Circuit. See: Ty, Inc. v. Publ’ns Int’l, Ltd., 292 F.3d
512 (7th Cir. 2002).

Tags

Seventh Circuit; Photograph; Review/Commentary; Sculpture

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

