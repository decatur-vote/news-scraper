Brammer v. Violent Hues Productions, LLC
No. 18-1763 (4th Cir. Apr. 26, 2019)

Year

2019

Court

United States Court of Appeals for the Fourth Circuit

Key Facts

Plaintiff Russell Brammer is a photographer who shot a time-lapse photograph of
the Adams Morgan neighborhood in Washington, D.C. Brammer posted the image
on several image-sharing websites and his personal website with the phrase “© All
rights reserved” beneath it. Defendant Violent Hues Productions, a film festival
organizer, used a cropped version of Brammer’s photograph on its website
alongside information about things to do in the D.C. area. After Brammer contacted
Violent Hues to request compensation for the unauthorized use, defendant removed
the photograph from its website but refused to compensate Brammer. Brammer
initiated a copyright infringement action. The district court granted summary
judgment to Violent Hues, holding that its use was a fair use. Brammer appealed.

Issue

Whether the use of a cropped stock photograph on a film festival website to
illustrate a list of nearby tourist attractions is a fair use.

Holding

The court found the first factor, the purpose and character of the use, weighed
against fair use because the use was not transformative. Unlike technological or
documentary uses that involve contextual change, Violent Hues used the photograph
precisely for its content, that is, to depict Adams Morgan. Although Violent Hues
claimed that its use provided film festival attendees with “information” regarding
Adams Morgan, this use “would not be hindered if it had to comply with Brammer’s
copyright.” Further, using the photograph to illustrate a website promoting a for-
profit festival without paying for a license was commercial use. In addition, because
the defendant, at best, acted negligently, the panel rejected the district court’s
finding that Violent Hues’ use was in “good faith.” The second factor, the nature of
the copyrighted work, also weighed against fair use because Brammer’s photograph
is a “stylized image, with vivid colors and a bird’s-eye view” infused with “creative
choices” that entitle it to “thick” copyright protection. In the court’s view, the fact
that the image had previously been published was of “no effect” in the context of
photography. The third factor, the amount and substantiality of the work used,
weighed against fair use because Violent Hues used roughly half the photograph,
removing only the negative space and keeping the most expressive features. The
fourth factor, the effect of the use on the potential market for or value of the work,
weighed against fair use as well. The court applied a presumption of market harm
because Violent Hues’ use was commercial and not transformative. Brammer also
introduced evidence showing that he twice licensed the photograph—a licensing
market which would be “dampened” if Violent Hues’ conduct were widespread.
Considering the four factors together, the court concluded that “the copying here
fails the ‘ultimate test’ of fair use: Violent Hues’ online display of Brammer’s photo
does not serve the interest of copyright law.” Indeed, while some content “sharing”
online may be fair, “[i]f the ordinary commercial use of stock photography
constituted fair use, professional photographers would have little financial incentive
to produce their work.” Accordingly, the panel reversed and remanded the case.

Tags

Fourth Circuit; Photograph; Internet/Digitization

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

