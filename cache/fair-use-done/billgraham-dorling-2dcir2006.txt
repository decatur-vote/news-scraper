Bill Graham Archives v. Dorling Kindersley Ltd.,
448 F.3d 605 (2d Cir. 2006)

Year

2006

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Plaintiff Bill Graham Archives owned copyrights in artistic images originally
used on concert posters and tickets to promote events for the rock band the
Grateful Dead. Defendant Dorling Kindersley Publishing (DK) sought
permission from plaintiff to reproduce images of seven such posters and
tickets in a 480-page cultural history of the group titled Grateful Dead: The
Illustrated Trip. After the parties failed to reach an agreement, DK
nevertheless reproduced and displayed the posters and tickets. When DK
refused to pay license fees for use of the posters and tickets after publishing
the book, plaintiff sued for infringement. Plaintiff appealed the district
court’s ruling that DK’s reproduction of the images in the book was fair use.

Issue

Whether it was fair use for defendants to reproduce Grateful Dead concert
posters and tickets for publication in a book on the band’s history without
permission from the copyright owner of the artwork that appears on the
posters and tickets.

Holding

The court upheld the district court’s ruling that defendants’ copying of the
images was fair use. It reasoned that defendant’s use of the concert posters
and tickets “as historical artifacts of Grateful Dead performances is
transformatively different from the original expressive purpose of [the]
copyrighted images.” The court also reasoned that the fact that the images
were copied in their entirety did not weigh against fair use because “the
reduced size of the images” was consistent with plaintiff’s transformative
purpose. Additionally, the court found that the plaintiff’s use did not harm
the market for defendant’s sale of the copyrighted artwork.

Tags

Second Circuit; Painting/Drawing/Graphic

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

