Cambridge Univ. Press v. Patton,
769 F.3d 1232 (11th Cir. 2014)

Year

2014

Court

United States Court of Appeals for the Eleventh Circuit

Key
Facts

Plaintiffs Cambridge University Press and other publishing houses sued Georgia
State University officials for infringing their copyrights by allowing unlicensed
portions of their works to be posted on university systems for students to obtain
electronically. After the United States District Court for the Northern District of
Georgia granted defendants’ summary judgment on plaintiffs’ claims of direct and
vicarious infringement, it considered plaintiffs’ allegations of continuing contributory
infringement under defendants’ revised fair use policy, which was implemented in
February 2009 after the suit commenced. The revised policy required professors to
complete a form to determine whether the fair use doctrine permitted them to post
materials electronically for students to access. After establishing some general fair
use guidelines for its analysis, the district court individually reviewed 74 violations
plaintiffs alleged were the result of continuing infringement under the revised 2009
fair use policy. The court held that the revised policy did not provide adequate
guidelines, which caused ongoing and continuing misuse of the fair use defense
and resulted in five incidents of infringement.

Issue

Whether Georgia State University’s adoption of the 2009 copyright policy caused
ongoing and continuing misuse of the fair use doctrine and resulted in infringement
of plaintiffs’ works.

Holding

The Eleventh Circuit Court of Appeals reversed the district court’s decision and
remanded for further proceedings consistent with its opinion, which the
court summarized as follows:

In sum, we hold that the District Court did not err in performing a work-
by-work analysis of individual instances of alleged infringement in order
to determine the need for injunctive relief. However, the District Court did
err by giving each of the four fair use factors equal weight, and by treating
the four factors mechanistically. The District Court should have
undertaken a holistic analysis which carefully balanced the four factors in
the manner we have explained.

The District Court did not err in holding that the first factor—the purpose
and character of the use—favors fair use. Although Defendants’ use was
nontransformative, it was also for nonprofit educational purposes, which
are favored under the fair use statute. However, the District Court did err
in holding that the second fair use factor—the nature of the copyrighted
work—favors fair use in every case. Though this factor is of
comparatively little weight in this case particularly because the works at
issue are neither fictional nor unpublished, where the excerpts in question
contained evaluative, analytical, or subjectively descriptive material that
surpasses the bare facts, or derives from the author’s own experiences or
opinions, the District Court should have held that the second factor was
neutral or even weighed against fair use where such material dominated.



------------

With regard to the third factor—the amount used in relation to the
copyrighted work as a whole—the District Court erred in setting a 10
percent-or-one-chapter benchmark. The District Court should have
performed this analysis on a work-by-work basis, taking into account
whether the amount taken—dqualitatively and quantitatively—was
reasonable in light of the pedagogical purpose of the use and the threat of
market substitution. However, the District Court appropriately measured
the amount copied based on the length of the entire book in all cases,
declined to give much weight to the Classroom Guidelines, and found that
the Defendants’ educational purpose may increase the amount of
permissible copying.

With regard to the fourth factor—the effect of Defendants’ use on the
market for the original—the District Court did not err. However, because
Defendants’ unpaid copying was nontransformative and they used
Plaintiffs’ works for one of the purposes for which they are marketed, the
threat of market substitution is severe. Therefore, the District Court should
have afforded the fourth fair use factor more significant weight in its
overall fair use analysis. Finally, the District Court erred by separating two
considerations from its analysis of the first and fourth fair use factors ....

Because the District Court’s grant of injunctive relief to Plaintiffs was
predicated on its finding of infringement, which was in turn based on the
District Court’s legally flawed methodology in balancing the four fair use
factors and erroneous application of factors two and three, we find that the
District Court abused its discretion in granting the injunction and the
related declaratory relief. Similarly, because the District Court’s
designation of Defendants as the prevailing party and consequent award of
fees and costs were predicated on its erroneous fair use analysis, we find
that the District Court erred in designating Defendants as the prevailing
party and awarding fees and costs to Defendants.

Tags

Eleventh Circuit; Education/Scholarship/Research; Internet/Digitization; Textual
work

Outcome

Preliminary ruling, mixed result, or remand

Source: U.S.

Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-use/index.html.

