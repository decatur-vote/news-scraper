Peterman v. Republican National Committee
No. CV 17-66-M-DLC (D. Mont. Feb. 22, 2019)

Year

2019

Court

United States District Court for the District of Montana

Key Facts

Plaintiff Erika Peterman, a photographer, was hired and paid a $500 fee by the
Montana Democratic Party (“MDP”) to take photographs of Democratic candidate
and singer-songwriter Rob Quist at an event. Peterman granted an unrestricted
royalty-free license to the MDP and Quist campaign to use the resulting
photographs. Both the MDP and Quist campaign subsequently posted one
photograph (the “Work”) to Facebook without attribution or copyright information.
Peterman later learned that the Republican National Committee (“RNC”) had sent
out a mailer in support of Quist’s opponent that used the Work to negatively depict
Quist. The RNC mailer was prepared by a vendor who downloaded the Work from
the Quist campaign’s Facebook page, photoshopped it, and added a treble clef and
text reading “For Montana Conservatives, / Liberal Rob Quist / Can’t Hit the Right
Note.” The court denied the RNC’s motion to dismiss Peterman’s copyright claim
because disputed issues of material fact remained as to whether the RNC’s use was
a fair use.

Issue

Whether use of a copyrighted photograph by a political party in a mailer to criticize
an opponent is a fair use.

Holding

On cross-motions for summary judgment, the court found that the balance of factors
favored fair use. On the first factor, purpose and character of the use, the court
found the vendor’s two “minimal alterations” to the Work alone were insufficiently
transformative. Yet, by “us[ing] Quint’s musicianship to criticize his candidacy,”
the mailer “changed the function and meaning of the Work by connoting a critical
message not inherent in the Work itself.” This purpose, along with the
noncommercial nature of the mailer, favored fair use. The court rejected the RNC’s
First Amendment argument, commenting that First Amendment principles are
incorporated into the fair use factors and the First Amendment “does not present an
additional layer of protection for unauthorized uses.” On the second factor, nature of
the copyrighted work, the court changed its analysis from its opinion on the motion
to dismiss from weighing against fair use to “inconclusive” in light of additional
facts that the Work had been published and shared by the MDP, Quist campaign,
and Peterman, despite the Work being “unequivocally creative.” The third factor,
amount and substantiality of the portion used, weighed against fair use because “the
RNC copied essentially the entirety of the Work” and “could have made its point as
effectively without incorporating the Work into its mailer.” Finally, the court
concluded that the fourth factor, effect of the use upon the potential market for or
value of the original, favored fair use because “[t]he Work has no recognizable
value outside of the . .. campaign, and that value has been fully realized by
Peterman,” who had been paid by the MDP. The first and fourth factors being
“determinative” in this case, the court granted summary judgment for the RNC on
its fair use defense.

Tags

Ninth Circuit, Photograph, Review/Commentary

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

