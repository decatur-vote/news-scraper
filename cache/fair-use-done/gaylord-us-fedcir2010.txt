Gaylord v. United States,
595 F.3d 1364 (Fed. Cir. 2010)

Year

2010

Court

United States Court of Appeals for the Federal Circuit

Key
Facts

Plaintiff Frank Gaylord created a sculpture known as The Column for the
Korean War Veterans Memorial in Washington, D.C., which consisted of 19
stainless steel statues representing a platoon of American soldiers in the
Korean War walking in formation. Defendant the U.S. Postal Service issued
a stamp commemorating the 50" anniversary of the Korean War armistice
that included a picture showing 14 of the 19 soldier statues. Plaintiff
appealed the Federal Claims Court ruling that defendant was not liable
because the Post Office made fair use of plaintiff’s work.

Issue

Whether the U.S. Postal Service’s unauthorized use of an image of plaintiffs
sculpture on a stamp was fair use.

Holding

The appellate court held that putting an image of plaintiff’s sculpture on a
stamp without permission was not fair use. Though the stamp altered colors
and added snow, the court did not find it transformative because it shared a
common purpose with the memorial—to honor Korean War veterans. The
court found that the stamp also had a clearly commercial purpose in light of
the fact that defendant received over $17 million from sales. The court
pointed out that the nature of The Column, being expressive and creative, and
the amount of the memorial depicted—fourteen of the nineteen soldier
sculptures—weighed against fair use. The court, however, noted that
defendant’s work harmed neither the value of plaintiff’s work nor the market
for derivatives based on this work, analogizing the stamp to the thumbnail
images in Kelly v. Arriba Soft Corp., 336 F.3d 811 (2003). Nonetheless, the
court determined that this was not enough to overcome the other factors that
weighed against a fair use finding.

Tags

Federal Circuit; Sculpture

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

