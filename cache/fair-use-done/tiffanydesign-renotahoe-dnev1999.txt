Tiffany Design, Inc. v. Reno-Tahoe Specialty, Inc.,
55 F. Supp. 2d 1113 (D. Nev. 1999)

Year

1999

Court

United State District Court for the District of Nevada

Key
Facts

Both parties, plaintiff Tiffany Design, Inc. and defendant Reno-Tahoe
Specialty, Inc., produced and distributed postcards and other “novelty items.”
Plaintiff Tiffany Design claimed to own the copyright in a digitally rendered
image of the Las Vegas Strip that it incorporated into certain products.
Defendant Reno-Tahoe reproduced Tiffany Design’s image of the Las Vegas
Strip and incorporated portions of it into its own products.

.

Issue

Whether Reno-Tahoe’s unauthorized reproduction and incorporation of
portions of Tiffany Design’s image into its products was fair use.

Holding

The court held that Reno-Tahoe failed to “meet its burden in establishing the
applicability of the defense of fair use to its alleged misconduct.” In reaching
its conclusion, the court weighed its determination that Reno-Tahoe’s use was
commercial and not transformative against a finding of fair use. The court
also relied on its determination that Reno-Tahoe’s use “might have great
effect upon commercial demand for [Tiffany’s] depictions of the Las Vegas
Strip.”

Tags

Ninth Circuit; Painting/Drawing/Graphic; Photograph

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

