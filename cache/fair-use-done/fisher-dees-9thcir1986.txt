Fisher v. Dees,
794 F.2d 432 (9th Cir. 1986)

Year

1986

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Defendant disc jockey Rick Dees wanted to use all or part of When Sunny
Gets Blue, a song composed and owned by plaintiffs Marvin Fisher and Jack
Segal, to create a parodic version of it. Fisher refused to give Dees
permission to use the song, but Dees soon released a comedy album that
contained a parody of the song—When Sonny Sniffs Glue—which contained
the first six of the original song’s thirty-eight bars of music and constituted
twenty-nine seconds of the approximately forty-minute album. Plaintiffs
appealed the district court’s ruling that defendant’s parodic version of their
song was fair use.

Issue

Whether defendant’s unauthorized use of a portion of plaintiffs’ song to
create a parody constituted fair use.

Holding

The court held that defendant’s song was a parody deserving of fair use
protection. Before weighing the fair use factors, the court rejected plaintiffs’
argument that defendant’s song was not actually a parody, noting that
defendant’s version was intended to “poke fun” at the original song. Further,
the court found that defendant’s use of the original song after being refused
permission did not demonstrate bad faith because parodists are seldom
granted permission and because the parody defense exists to allow uses that
generally cannot be licensed. In response to plaintiffs’ claim that immoral
parodies are not protected by the fair use doctrine, the court held that,
“[a]ssuming without deciding that an obscene use is not a fair use,” the
parody, while innocuous or silly, was not immoral.

While the use was commercial in nature, thus creating a presumption against
fair use, the court noted that the presumption could be rebutted if the parody
did not unfairly diminish the economic value of the original. The court held
that the parody was not likely to function as a commercial substitution on the
open market, as the two works did not fulfill the same consumer demand.
The court also held that the parody used no more of plaintiffs’ work than was
necessary to “conjure up” the original song and accomplish its parodic
purpose, finding that a song is difficult to parody effectively without exact or
near-exact copying since any more of a variation would have made the song
unrecognizable to the general audience.

Tags

Ninth Circuit; Music; Parody/Satire; Review/Commentary

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

