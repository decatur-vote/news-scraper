Chapman v. Maraj
No. 2:18-cv-09088-VAP-SS (C.D. Cal. Sept. 16, 2020)

Year

2020

Court

United States District Court for the Central District of California

Key Facts

Plaintiff Tracy Chapman owns the copyright for the song Baby Can I Hold You (the
“Composition”). Defendant Onika Tanya Maraj, professionally known as Nicki
Minaj, experimented on a remake of the song Sorry, which she believed was an
original work by Shelly Thunder but which her representatives later learned was a
cover of the Composition. Minaj knew she needed to obtain a license to publish a
remake of the Composition because her remake incorporated a large number of lyrics
and vocal melodies from the Composition. Minaj made multiple requests to Chapman
for a license, but Chapman denied each request. Minaj did not include her remake of
Sorry on her album. Minaj contacted DJ Aston George Taylor, professionally known
as DJ Flex, and asked if he would premiere a record that was not on her album. Minaj
denies sending DJ Flex a copy of her remake of Sorry and she posted a message
instructing him not to play any songs not on her album. Somehow DJ Flex obtained a
copy of Minaj’s remake of Sorry and played it on the radio. Chapman sued Minaj,
asserting copyright infringement. On cross-motions for summary judgment, Chapman
asserted Minaj violated her copyright by creating and distributing a derivative work
based on the Composition, while Minaj contended that her creation of the remake
qualifies as fair use.

Issue

Whether a recording artist’s use of lyrics and vocal melodies from a musical work for
artistic experimentation and for the purpose of securing a license from the copyright
owner is a fair use.

Holding

The court found that the first factor, the purpose and character of the use, favored fair
use. The court concluded that the initial purpose of Minaj’s new work was
experimentation. Given that Minaj “never intended to exploit the work without a
license” and excluded the new work from her album, Minaj’s use was not purely
commercial. In addition, the court observed that “artists usually experiment with
works before seeking licenses and rights holders typically ask to see a proposed work
before approving a license.” The court expressed concern that “uprooting . . . [these]
common practices would limit creativity and stifle innovation within the music
industry.” The second factor, the nature of the copyrighted work, disfavored fair use
because the Composition is a musical work, which is “the type of work that is at the
core of Copyright’s protective purpose.” The third factor, the amount and
substantiality of the portion used in relation to the work as a whole, favored fair use.
Although Minaj’s new work incorporated many lyrics and vocal melodies from the
Composition, the material Minaj used “was no more than necessary to show Chapman
how [Minaj] intended to use the Composition in the new work.” The fourth factor, the
effect of the use upon the potential market for or value of the copyrighted work,
favored fair use because “there is no evidence that the new work usurps any potential
market for Chapman.” Considering the factors together, the court found that Minaj’s
use was fair and granted partial summary judgment in Minaj’s favor that her use did
not infringe Chapman’s right to create derivative works.

Tags

Music

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/fair-

index.html.

