New Era Publ’ns Int’!, ApS v. Carol Publ’g Grp.,
904 F.2d 152 (2d Cir. 1990)

Year

1990

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Plaintiff New Era Publications International, ApS (New Era) was the
exclusive licensee of all Church of Scientology founder L. Ron Hubbard’s
writings. Defendant Carol Publishing Group intended to publish an
approximately 527-page manuscript titled A Piece of Blue Sky: Scientology,
Dianetics and L. Ron Hubbard Exposed (A Piece of Blue Sky), “an
unfavorable biography of Hubbard and a strong attack on Scientology,”
written by a former member of the Church. In both the text and at the
beginning of many chapters, the author quoted widely from Hubbard’s
substantial body of published works. Defendant publisher appealed the
district court’s ruling that the author’s use of Hubbard’s writing was not a fair
use.

Issue

Whether the unauthorized quotation of published works in a biography
constitutes fair use.

Holding

The court found that defendant’s use of Hubbard’s works was fair use. The
court first found that defendant’s unfavorable biography fit comfortably
within the categories of uses that could be fair namely criticism, scholarship,
and research— because A Piece of Blue Sky was designed “to educate the
public about Hubbard, a public figure who sought public attention,” and used
quotes to further that purpose rather than to unnecessarily appropriate
Hubbard’s literary expression. The court also found that defendant’s book
quoted from a small portion of plaintiff’s writings and did not take the heart
of the works. Further, the quoted works were all published, which the court
pointed out allows for a “greater amount of copying.” Even though plaintiff
argued that defendant’s biography harmed any future biography they may
authorize, the court concluded that there was not a negative effect on
plaintiff’s market because potential customers for an authorized favorable
biography would likely not be deterred from buying it on the basis that an
unfavorable biography used the same sources. Furthermore, the court found
that even if A Piece of Blue Sky’s negative critique did dissuade customers
from purchasing an authorized biography, such harm “would not result from
unfair infringement forbidden by the copyright laws, but rather from a
convincing work that effectively criticizes Hubbard, the very type of work
that the Copyright Act was designed to protect and encourage.”

Tags

Second Circuit; Education/Scholarship/Research; Review/Commentary;
Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

