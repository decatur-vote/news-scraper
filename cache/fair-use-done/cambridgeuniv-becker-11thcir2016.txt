Cambridge University Press vy. Mark P. Becker
No. 1:08-cv-01425-ODE (N.D. Ga. Mar. 31, 2016)
appeal docketed, Aug. 26, 2016

Year

2016

Court

United States District Court for the Northern District of Georgia

Key
Facts

In 2008, plaintiffs Cambridge University Press and other publishing houses sued
Georgia State University (GSU) officials for contributorily infringing their
copyrights due to a fair use policy (and checklist) that allowed GSU faculty to
post unlicensed portions of the publishers’ works on university systems for
students to obtain electronically. Plaintiffs pursued 74 allegations of
infringement (initially 99, but dropped 25 at trial) and provided evidence that
GSU could have purchased licenses for some of the works at issue.

In 2012, of the 74 allegations at issue, the District Court for the Northern District
of Georgia found that plaintiffs has not established a prima facie case of
infringement for 26 allegations, and of the remaining 48 allegations, GSU had
only infringed plaintiffs’ works in five instances.. As such, the district court held
that GSU’s fair use policy led to the unlicensed and infringing use of five
excerpts of plaintiffs’ works because the policy “did not limit copying in those
instances [of infringement] to decidedly small excerpts . . . [and it] did not
provide sufficient guidance in determining the ‘actual or potential effect on the
market or the value of the copyrighted work.’”

Plaintiff's appealed the district court’s decision as to the 48 allegations of
infringement and the Eleventh Circuit reversed the judgement and remanded for
further proceedings because of the district court’s flawed method of fair use
analysis. In particular, the Eleventh Circuit found that the district court erred in
“giving each of the four fair use factors equal weight,” “setting a 10 percent-or-
one-chapter benchmark” for the third factor, and should “have afforded the fourth
fair use factor more significant weight in its overall fair use analysis.” On
remand, the district court revaluated the specific instances of alleged
infringement according to guidance from the Eleventh Circuit that required a
more holistic balancing of the four fair use factors.

Issue

Whether Georgia State University’s adoption of the 2009 copyright policy
caused ongoing and continuing misuse of the fair use doctrine and resulted in
infringement of plaintiffs’ works.

Holding

As directed by the Eleventh Circuit, the district court conducted a revised
four-step analysis of each of the 48 allegation of infringement for which the
plaintiffs had met their prima facie burden. In its revised analysis, the district
court found that the GSU fair use policy led to infringing use of plaintiffs’
works, this time, in four as opposed to five instances. The court found that
the remaining 44 uses qualified as fair use. Although the weight and outcome
of the factors varied for each alleged instance of infringement, the district
court generally concluded that: (1) the first factor, purpose and character of
the use, weighed in favor of fair use despite the nontransformative nature of
the use because GSU is a nonprofit educational institution and the excerpts
were used for the purpose of teaching students; (2) the second factor, the
nature of the work, was “of comparatively little weight in this case,
particularly because the works at issue are neither fictional nor unpublished;”
(3) the third factor, the amount of work used, must take into account “the
effect of the favored nonprofit educational purpose of the use under factor
one,” while considering “the impact of market substitution as recognized



------------

under factor four, in determining whether the quantity and substantiality . . .
of [d]efendants’ unlicensed copying was excessive;” and (4) the fourth factor,
effect of the use on the potential market for the work, “concern[ed] not the
market for Plaintiffs’ original works . . . but rather a market for licenses” to
use excerpts, which initially favored plaintiffs where evidence of digital
licensing was available. Taking into account the Eleventh Circuit’ guidance to
afford “the fourth fair use factor more significant weight in its overall fair use
analysis,” the district court estimated the initial weight of each of the four
factors as follows: “25% for factor one, 5% for factor two, 30% for factor
three and 40% for factor four.” Based on these findings of infringement, the
court ordered plaintiffs to submit proposed text for injunctive and declaratory
relief aimed at preventing future infringement of their works.

Tags 

Eleventh Circuit; Education/Scholarship/Research; Internet/Digitization; Textual work

Outcome 

Preliminary ruling, mixed result, or remand

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

