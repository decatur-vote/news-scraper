Rimini St. v. Oracle Int’! Corp.
473 F. Supp. 3d 1158 (D. Nev. 2020)

Year

2020

Court

United States District Court for the District of Nevada

Key Facts

Plaintiff and counter-defendant Rimini Street, Inc. is a third-party service provider for
enterprise software. Defendants and counter-claimants Oracle International Corp. and
Oracle America, Inc. (collectively, “Oracle”) develop and license enterprise software.
In a separate litigation, Rimini was found liable for copyright infringement due to its
unauthorized copying of Oracle’s software, specifically by creating patches and
updates developed for “one customer’s software to support other customers.” Rimini
made changes to its internal company policies for servicing enterprise software in
response to a court order, but the parties continued to dispute whether Rimini’s
servicing activities complied with the terms of its customers’ licenses with Oracle.
Rimini brought a declaratory judgment action against Oracle, seeking a declaration
that its conduct under its new policies was not infringing. Oracle asserted
counterclaims, including that Rimini infringed its copyrights through unauthorized
copying. One of Rimini’s defenses was that its copying was fair use.

Issue

Whether it is fair use to make random-access memory (RAM) copies of a client’s
licensed enterprise software for the purpose of servicing and developing software
updates for the enterprise software.

Holding

Analyzing whether Rimini’s servicing activities were authorized, the court found that
although copying Oracle software into RAM and developing updates was permissible
under the terms of the license, Rimini exceeded the scope of the license by creating
prototypes that Rimini used for multiple customers and did not restrict its services to
the “internal data processing operations” of each customer. Considering fair use, the
court found the first factor, the purpose and character of the use, weighed against fair
use. The court first determined that Rimini’s use is commercial in nature.
Furthermore, Rimini’s use is not transformative because, although Rimini creates new
code for its software updates, the updates are implemented into the original software,
which still functions in the same way and for the same purpose as the original. The
court distinguished this case from reverse-engineering cases that involved copying to
research interoperability because Rimini creates updates that only “work within and
can only be used with the existing . . . software.” The second factor, the nature of the
copyrighted work, also weighed against fair use because the “unique business
enterprise software” was “clearly protected.” Unlike cases where disassembly was
necessary to determine the functional aspects of a program, Rimini had no similar
need as it was “permitted to make copies . . . to make updates and fixes to the
software” so long as the activity was “within the scope of the license.” The third
factor, the amount and substantiality of the work taken, also cut against fair use
because the RAM copies contain a substantial portion of Oracle’s software, and the
portion copied is essential for creating and testing updates. Lastly, the fourth factor,
the effect of the use on the market for or value of the copyrighted work, weighed
against fair use because Oracle intended to occupy the market for aftermarket support
for its software and Rimini’s “creation, testing, and distribution of . . . derivative
works will undoubtedly impact Oracle’s ability to function in that market.” Weighing
the factors together, the court concluded Rimini’s uses were not fair.

Tags

Computer Program

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

