Arrow Prods., Ltd. v. The Weinstein Co., LLC,
44 F. Supp. 3d 359 (S.D.N.Y. 2014)

Year

2014

Court

United States District Court for the Southern District of New York

Key
Facts

Plaintiff entertainment company owns the copyrights to a pornographic film
titled Deep Throat. In 2013, defendant produced the film Lovelace, which is
based on the biography of Linda Lovelace, the star of Deep Throat. Plaintiff
alleged that three scenes in defendant’s film infringed corresponding scenes
in Deep Throat.

Issue

Whether defendant’s recreation and use of three scenes from plaintiffs
copyright protected film in a new film about the star of plaintiff's film was
fair use.

Holding

The court held that defendant’s recreation and use of three scenes from Deep
Throat in a film based on the biography of Ms. Lovelace was fair use. The
court found defendant’s use transformative, as it added a “new, critical
perspective” on the life of Ms. Lovelace. The court also found that, despite
the creative nature of Deep Throat, the defendant only copied elements of
three short segments from the film and did not copy the heart of the film,
which weighed in favor of a finding of fair use.

Tags

Second Circuit; Film/Audiovisual; Review/Commentary

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

