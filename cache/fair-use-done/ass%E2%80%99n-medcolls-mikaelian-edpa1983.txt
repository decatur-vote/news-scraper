Ass’n of Am. Med. Colls. v. Mikaelian,
571 F. Supp. 144 (E.D. Pa. 1983)

Year

1983

Court

United States District Court for the Eastern District of Pennsylvania

Key
Facts

Plaintiff Association of American Medical Colleges (AAMC) owned the
Medical College Admission Test (MCAT) copyright. Defendant Viken
Mikaelian owned and operated defendant Multiprep, Inc., which prepared
students to take the MCAT by providing them with practice materials. Up to
50% of MCAT questions are used in more than one version of the test.
Defendant Mikaelian personally took the MCAT eight separate times, and
AAMC demonstrated that 879 of the questions in the practice materials that
defendants prepared (about 90% of the practice the materials) were identical
to questions on the tests administered to defendant Mikaelian. AAMC also
alleged that the practice materials gave defendant’s students an unfair
advantage, thus potentially harming the MCAT’s credibility. AAMC sought
a preliminary injunction against plaintiff’s continued use of actual MCAT
questions.

Issue

Whether the use of actual MCAT questions in preparing students to take the
MCAT qualifies as fair use.

Holding

The court granted plaintiff a preliminary injunction. It held that the MCAT
questions were not merely scientific facts within the public domain, and that
defendant’s use of its questions did not constitute fair use. The purpose factor
did not favor defendants: Multiprep may have had a training function, but its
use of the MCAT materials was highly commercial, and the materials were
not assembled “to advance scientific knowledge among Philadelphia-area
undergraduates.” Regarding the nature of the work, MCAT questions were
created and used under strict security, and were copyrighted specifically to
“prevent their use as teaching aids.” The questions were not the type of
material that could be fairly used. Concerning the amount and substantiality
of the work, nearly 90% of the Multiprep materials were used verbatim,
making it unlikely that fair use could apply. Finally, concerning market
effect, Multiprep’s use of the materials would make the MCAT questions
worthless to AAMC, as continued use could “destroy the accuracy and
comparability of the MCAT because Multiprep students will have an unfair
advantage over others taking the MCAT.”

Tags

Third Circuit; Education/Scholarship/Research; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

