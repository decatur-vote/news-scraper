Golden vy. Michael Grecco Prods.

No. 19-CV-3156 (NGG) (RER), 2021 U.S. Dist. LEXIS 43701 (E.D.N.Y. Mar. 9, 2021)

Year

2021

Court

United States District Court for the Eastern District of New York

Key Facts

Plaintiff, Lee Golden III, owns and operates a pop culture blog. In 2015, Golden
published a blogpost about rumors of a possible reboot of the television show Xena:
Warrior Princess (the “Post”). Golden’s post included a photograph of Lucy Lawless
as Xena (the “Photograph”), which was taken by professional photographer Michael
Grecco in 1997 for a photoshoot for the original television show. Grecco was paid for
the images he took and he retained all copyrights to the photographs. Since 2009,
Grecco had licensed Photograph through Getty Images. Although disputed by Grecco,
Golden points to evidence showing the Photograph was licensed eleven times between
2010 and 2013, generating $3.94 in revenue. Golden’s blog earns money from
displaying banner ads, but Golden asserts he earned no money from the Post. In 2018,
Grecco discovered the Post and informed Golden of the alleged infringement of the
Photograph. Golden responded by apologizing and removing the photograph, but after
Grecco threatened legal action, Golden brought an action seeking a declaratory
judgment that he did not violate Grecco’s copyright or, in the alternative, that he was
an innocent infringer. Grecco asserted a counterclaim for copyright infringement to
which Golden asserted fair use and other defenses.

Issue

Whether the use of a promotional photograph from a commercial photoshoot in a blog
post reporting on the television show is fair use.

Holding

Considering the first factor, the purpose and character of the use, the court found that
Golden’s use of the Photograph was not transformative, which weighed against fair
use. Although the Post could be considered “news reporting” about the potential
reboot, Golden used the Photograph as an “illustrative aid,” not to provide criticism or
commentary on the Photograph itself. The court also found that the Post did not
transform the purpose of the Photograph from “promotion to historical artifact.” The
second factor, the nature of the copyrighted work, disfavored fair use because the
Photograph is “[a] portrait photograph that is the clear product of the photographer’s
artistic choices.” The third factor, the amount and substantiality of the portion used,
disfavored fair use as well because Golden used the entire image, unaltered. The court
found the fourth factor, the effect of the use upon the potential market for or value of
the copyrighted work, also disfavored fair use. Although there was limited demand for
licensing the Photograph, the court reasoned that the “secondary market would be
meaningless if entertainment websites could use the image without paying the
licensing fee, even if few or no customers showed interest in [the Photograph].” The
court weighed these factors together and concluded that Golden’s use of the
Photograph was not fair use.

Tags

Internet/Digitization; News reporting; Photograph

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

