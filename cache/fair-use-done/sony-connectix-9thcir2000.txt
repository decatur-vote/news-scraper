Sony Computer Entm’t, Inc. v. Connectix Corp.,
203 F.3d 596 (9th Cir. 2000)

Year

2000

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Plaintiff Sony Computer Entertainment, Inc. produced and marketed the Sony
PlayStation video game console. Sony owned the copyright to BIOS, the
software program that operated the PlayStation. Defendant Connectix
Corporation made and sold a software program called “Virtual Game
Station.” The purpose of the Virtual Game Station was to emulate on a
regular computer the functioning of the Sony PlayStation console, so that
computer owners who buy the Virtual Game Station software can play Sony
PlayStation games on their computers. In order to create the Virtual Game
Station, Connectix “reverse engineered” Sony’s BIOS program. As part of
the reverse engineering process, Connectix made several intermediate copies
of the BIOS program. Sony sued Connectix for copyright infringement.

The district court concluded that Sony was likely to succeed on its
infringement claim because Connectix’s “intermediate copying” was not a
protected fair use. The court also enjoined Connectix from selling the Virtual
Game Station and copying or using Sony’s BIOS program in the development
of other Virtual Game Station products.

Issue

Whether Connectix’s intermediate copying of a copyright protected computer
program for reverse engineering purposes qualified as fair use.

Holding

The circuit court reversed the district court’s ruling and remanded the case
with instructions to dissolve the injunction against Connectix. The court
concluded that the intermediate copies Connectix made and used during the
course of its reverse engineering of the BIOS program were protected fair use,
necessary to permit Connectix to make its non-infringing Virtual Game
Station function with PlayStation games. In reaching its conclusion, the court
found that three of four fair use factors—the purpose and character of the use
the nature of the copyrighted work, and the effect of the use upon the
potential market for the work—all weighed in favor of fair use. Although the
court found that Connectix copied the entire BIOS program, it concluded that
this factor warranted “very little weight” in cases of “intermediate
infringement” where “the final product does not itself contain infringing
material.”

Tags

Ninth Circuit; Computer program

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

