Atari Games Corp. v. Nintendo of Am. Inc.,
975 F. 2d 832 (Fed. Cir. 1992)

Year 

1992

Court 

United States Court of Appeals for the Federal Circuit

Key Facts

Nintendo of America Inc., and Nintendo Co., Ltd. manufactured the Nintendo Entertainment
Facts System (NES). NES was a home video game console that allowed individuals to play video
game programs stored on video game cartridges. Nintendo owned the copyrights in the
source and object code for a program called 10NES. The 10NES program facilitated a
“lock” and “key” mechanism that allowed Nintendo to prevent its NES console from reading
and playing video game programs on video game cartridges other than those that Nintendo
had authorized. Because of the 10NES program, Nintendo was able to exact licensing fees
from manufacturers that sought to develop video game programs capable of playing on the
NES console.

Nintendo’s competitors, Atari Games Corporation and its wholly-owned subsidiary, Tengen,
Inc., replicated the 1ONES program’s object and source code, thereby making it possible to
manufacture and sell video game cartridges that were compatible with the NES console
without paying licensing fees to Nintendo. To replicate the 10NES object code, Atari
obtained authorized copies of NES game cartridges and consoles and “reverse engineered”
the object code contained in the microchips therein. Atari also gained access to an
unauthorized copy of the 10NES source code by submitting false information to the U.S.
Copyright Office. While in the process of “reverse engineering” the 10NES program, Atari
made multiple intermediate copies of both the program’s source and object code.

Atari sued Nintendo for, among other things, antitrust violations, and Nintendo sued for,
among other things, copyright infringement of the 1ONES program. After consolidating the
two cases, the district court preliminarily enjoined Atari from exploiting Nintendo’s 10NES
program.

Issue 

Whether Atari’s intermediate copying of copyright protected source and object code for
reverse engineering purposes qualified as fair use.

Holding 

The circuit court affirmed the district court’s imposition of a preliminary injunction, finding
Nintendo demonstrated a likelihood of success on its copyright infringement claims.
Regarding the 10NES source code, the court found that because Atari was not authorized to
possess the copy of the source code that it obtained from the U.S. Copyright Office, any
intermediate or derivative copying of the code did not qualify as a fair use. Regarding the
10NES object code, the court found that the intermediary copies of the code that Atari made
while examining microchips in its “rightful possession” for the purpose of “reverse
engineering” the code did not violate Nintendo’s copyright. However, the court also
determined that, based on the district court’s findings that the program Atari developed as a
result of its reverse engineering of the 1ONES program included similarities between the
programs “beyond the similarities necessary to accommodate the programming environment,
or similarities necessary to embody the unprotectable idea, process, or method of the 1ONES
program,” Nintendo was likely to succeed on the merits of its infringement claim.
Specifically the court stated that the fair use doctrine “did not give Atari more than the right
to understand the 10NES program and to distinguish the protected from the unprotected
elements of the 1ONES program” and that “Atari could not use reverse engineering as an
excuse to exploit commercially or otherwise misappropriate protected expression.”

Tags 

Federal Circuit; Computer program

Outcome 

Preliminary ruling, mixed result, or remand

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-use/index.html.

