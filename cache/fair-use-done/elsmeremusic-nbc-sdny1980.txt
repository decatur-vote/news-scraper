Elsmere Music, Inc. v. NBC,
482 F. Supp. 741 (S.D.N.Y. 1980),
aff’d, 623 F.2d 252 (2d. Cir. 1980) (per curiam)

Year

1980

Court

United States District Court for the Southern District of New York

Key
Facts

Plaintiff Elsmere Music, Inc. alleged that defendant NBC, Inc. infringed its
copyright in the popular jingle I Love New York. The New York State
Department of Commerce had used this song when it launched a 1977
advertising campaign to promote New York City. On May 20, 1978, NBC’s
popular weekly comedy show Saturday Night Live used the song for a
comedy sketch that depicted officials in the biblical city of Sodom attempting
to recast its image in a more positive light by adopting a song based on I Love
New York titled I Love Sodom.

Issue

Whether defendant’s unauthorized, allegedly satirical use of plaintiff’s
advertising jingle for a comedy routine was fair use.

Holding

The court held that the defendant’s use of plaintiff’s jingle for satirical
purposes was fair use. It determined that defendant’s sketch and version of
the song clearly satirized the attempt to improve New York City’s tarnished
image. The court noted that I Love Sodom was an upbeat tune intended to
distract potential visitors’ attention from Sodom’s reputation for vice, just as I
Love New York was intended to alter outsiders’ perceptions of New York
City. The court then found that defendant’s use of the song did not interfere
with plaintiff’s market. Finally, the court found that defendants did not
appropriate more than was necessary for the satirical purpose of the sketch.

Tags

Second Circuit; Film/Audiovisual; Parody/Satire

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

