Otto v. Hearst Communications, Inc.
No. 1:17-cv-4712-GHW (S.D.N.Y. Dec. 10, 2018)

Year

2018

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff Jonathan Otto attended a friend’s wedding at the Trump National Golf
Club in New Jersey in June 2017. President Donald Trump surprised the celebrants
when he stopped by the wedding unannounced. Otto took a picture on his iPhone
of President Trump at the wedding. Otto texted the picture to another wedding
guest. The next day, Otto learned that his photograph had been published by
multiple media outlets, including Defendant Hearst Communications, which had
published Otto’s photograph as part of an article about the wedding on
Esquire.com. It does not cost money to access Esquire.com, but the page
displaying the article about the wedding featured ads from which Hearst earned
revenue.

Issue

Whether a news organization’s unauthorized publication of a photograph in
connection with an article about the U.S. President is a fair use.

Holding

The district court granted Plaintiffs motion for summary judgment on Defendant’s
fair use defense. The court found that the first factor, purpose and character of the
use, weighed in favor of Plaintiff. It reasoned that Hearst’s use was not
transformative because Otto took the photograph and Hearst used the photograph
for the same purpose: to show a noteworthy event. Although the court recognized
that in an “extraordinary case,” the public interest in news reporting justify
reproduction of an original work without significant alteration, it found that this
was not such a case. The court also determined that Defendant’s use was
commercial, which weighed against finding a fair use, because Hearst received
advertising revenue from the article. The second factor, the nature of the
copyrighted work, favored Hearst. Because Otto did not stage or pose the
photograph, the court concluded that the photograph was “more factual than
creative.” The photograph had also been widely disseminated prior to Hearst’s use,
which weighed in favor of Heart. The third factor, amount and substantiality of the
portion used, weighed in favor of Plaintiff, because “Hearst used a slightly cropped
but otherwise unedited version of Otto’s photograph.” The court rejected
Defendant’s argument that the news reporting purpose of the use required Hearst to
use the complete photograph. The final factor, effect of the use upon the potential
market, also weighed in favor of Plaintiff. The court observed that “[p]Jublishing the
[p]hotograph without permission essentially destroy[ed] the primary market for its
use.” Although he was not a professional photographer, “Otto had the right to try to
sell the [p]hotograph to media outlets, if he decided to do so.” Hearst’s use of a
nearly identical version of Otto’s photograph supplanted Otto’s market. Weighing
the factors together, the court concluded that Hearst’s publication of Otto’s
photograph was not fair use as a matter of law.

Tags

Second Circuit, Internet/Digitization, News reporting, Photograph

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

