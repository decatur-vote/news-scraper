O’Neil v. Ratajkowski
No. 19 CIV. 9769 (AT), 2021 WL 4443259 (S.D.N.Y. Sept. 28, 2021)

Year

2021

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff, Robert O’Neil, a paparazzi photographer, photographed professional model
and actress Defendant Emily Ratajkowski leaving a flower shop holding a bouquet
covering her face on September 13, 2019. O’Neil registered the copyright in the
photograph and uploaded the photograph to his agency Splash News, which posted the
photograph online for licensing. The photograph earned O’Neil minimal income.
Ratajkowski posted the photograph to her Instagram Stories and added the caption
“mood forever” to the bottom of the post. The post automatically deleted in 24 hours.
O’Neil filed a claim for copyright infringement and the parties each moved for
summary judgment on whether posting the photograph was fair use.

Issue

Whether a celebrity’s use of an unlicensed paparazzi photograph of herself in a
captioned social media post that disappeared after 24 hours is fair use.

Holding

Considering the first fair use factor, the purpose and character of the use, the court
found there to be a genuine issue of material fact as to whether Ratajkowski’s use was
transformative. The court observed that a jury could either view the photograph and
Ratajkowski’s caption as commentary on her attempts to hide from paparazzi, which
would be transformative, or as a photograph that “merely showcases [her] clothes,
location, and pose at that time,” which would serve the same purpose as the original.
The court noted that while Ratajkowski did not earn any money from the post, her
Instagram account is a commercial enterprise. Considering bad faith, the court noted
there was no evidence that Ratajowski personally removed copyright attribution from
the photograph, knew that it was copyrighted, or that she took it directly from Splash
News. The second factor, the nature of the copyrighted work, marginally disfavored
fair use because while photographs are creative, this particular photograph was
“essentially factual in nature” because O’Neil captured Ratajkowski in public and did
not direct her in any way to achieve his artistic vision. The third factor, the amount
and substantiality of the portion used, slightly disfavored fair use because Ratajkowski
used more of the photograph than necessary for her claimed purpose of commenting
on intrusive paparazzi. However, the court noted this factor is given less weight
because Ratajkowski posted the photograph to her Instagram Stories, where it
appeared for only 24 hours. On the fourth factor, the effect of the use upon the
potential market for or value of the copyrighted work, the court found issues of
material fact remained because there was no information in the record regarding the
market for individuals licensing paparazzi photographs to post on social media.
Because the court found issues of material fact with respect to the first and fourth
factors and found the second and third factors weighed only marginally against fair
use, the court denied both parties’ motions for summary judgment regarding fair use.

Tags

Internet/Digitization; Photograph; Review/Commentary

Outcome

Preliminary finding; fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

