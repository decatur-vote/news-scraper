Lish v. Harper’s Magazine Found.,
807 F. Supp. 1090 (S.D.N.Y. 1992)

Year

1992

Court

United States District Court for the Southern District of New York

Key
Facts

Plaintiff Gordon Lish, a well-known writer of fiction novels and professor of
an infamous creative writing class, composed a letter to forty-nine prospective
students introducing them to his class. Defendant Harper’s Magazine
Foundation (Harper’s), publisher of a monthly literary magazine, included an
edited version of the letter in its magazine (about 48% of the original) under
the title A Kind of Magnificence. Lish alleged that Harper’s infringed his
copyright by publishing the letter.

Issue

Whether a scholarly magazine’s unauthorized publication of an abridged
version of a letter written by a well-known author/professor who limited the
letter’s distribution to prospective students qualified as fair use.

Holding

The court held that the unauthorized publication of plaintiff’s letter did not
constitute fair use. For the first fair use factor, the court found that defendant
copied plaintiff’s letter without providing additional commentary, copied
more than was necessary, and used it for commercial gain, all weighing
against finding fair use. For the second factor, the court determined that
defendant’s use of unpublished, predominantly creative material further
weighed against fair use. For the third factor, the court ruled that defendant’s
use of a “substantial portion” of the letter, constituting the “heart” of the
work, weighed against fair use. Finally, the court concluded that the fourth
factor weighed in favor of fair use because plaintiff failed to demonstrate any
adverse market impact resulting from the use. The court stated that it did not
believe that, even if plaintiff were to later publish a book of his writings
including the letter, a potential buyer would be dissuaded from buying the
book because defendant had previously published the letter.

Tags

Second Circuit; News Reporting; Review/Commentary; Textual work;
Unpublished

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

