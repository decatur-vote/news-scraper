Apple Inc. y. Corellium, LLC

Case No. 9:19-cv-81160-RS, 2020 U.S. Dist. LEXIS 249945 (S.D. Fla. Dec. 29, 2020)

Year

2020

Court

United States District Court for the Southern District of Florida

Key Facts

Plaintiff Apple Inc. owns the copyright in various versions of iOS, the mobile
operating system that powers many Apple devices, including the iPhone. Defendant
Corellium, LLC developed a product that permits users to create virtual models of
iPhones by using iOS files that are available for download from Apple at no cost.
Corellium’s product has limited functionality and lacks some features of interest for
average consumers buying iPhones, including the ability to make phone calls, use
the camera, send text messages, or access the App store. The product is intended to
provide an environment that facilitates technology security research. Corellium
screens customers before permitting them to use the product. After a potential deal
to acquire Corellium fell apart, Apple filed suit alleging that Corellium infringed
Apple’s copyrights in iOS and circumvented its security measures in violation of the
federal Digital Millennium Copyright Act (“DMCA”). Corellium moved for
summary judgment on fair use and the DMCA claim; Apple moved for summary
judgment on its DMCA claim.

Issue

Whether the use of a copyrighted operating system to create a simulated security
research and testing environment is fair use.

Holding

For the first fair use factor, the purpose and character of the use, the court concluded
the use was transformative because the Corellium product incorporates its own code
and includes additional features not included in the iOS to create a new product that
serves the transformative purpose of enabling security research. Although
Corellium’s product was sold commercially, the profit motivation did not
undermine a finding of fair use, “particularly considering the public benefit of the
product.” For the second fair use factor, the nature of the copyrighted work, the
court noted the limitations on copyright protection for functional software but found
the second factor rarely played a significant role in the fair use analysis. The court
concluded that the third factor, the size and significance of the portion of the
copyrighted work used, weighed in favor of fair use. The court found that
Corellium’s use of iOS, which excluded consumer iOS device features like the App
Store and camera, was “proportional and necessary to achieve Corellium’s
transformative purpose.” For the fourth factor, the effect on the potential market or
value of the original, the court concluded that this favored fair use. The court did not
find any significant market impact, rejecting the arguments that Corellium’s product
competed with Apple’s security research product and harmed the licensing market
for iOS. In addition, the court considered and did not find a “lack of good faith and
fair dealing” because Corellium vetted its customers and held them to no lesser
standard than Apple itself imposed in its security research program. Weighing all
these factors, the court found Corellium’s use of iOS to be a fair use and dismissed
the copyright claim. The court went on to deny summary judgment on Apple’s
DMCA anti-circumvention claim, rejecting the argument that Corellium’s fair use
defense also absolved it of liability for circumvention and concluding that genuine
issues of material fact remained concerning this claim.

Tags

Computer Program; Education/Scholarship/Research; Internet/Digitization

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

