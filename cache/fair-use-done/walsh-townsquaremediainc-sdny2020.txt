Walsh v. Townsquare Media, Inc.

No. 19-CV-4958 (VSB), 2020 U.S. Dist. LEXIS 96090 (S.D.N.Y. June 1, 2020)

Year

2020

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff, Rebecca Fay Walsh, a photographer who licenses her photographs to online
and print media, asserted a claim of copyright infringement relating to the unlicensed
publication of Walsh’s photograph of rapper Cardi B at New York Fashion Week in
an article on the website XXL Mag (“XXL”), which is owned and operated by
Defendant Townsquare Media. The article, entitled “Cardi B Partners with Tom Ford
for New Lipstick Shade,” mentions the collaboration, but focuses on the ensuing
reaction to the announcement by fans of Cardi B and a rival rapper on Instagram.
XXL embedded three Instagram posts in the article: (1) a post by the Instagram
account tomfordbeauty to advertise the lipstick; (2) a post by Cardi B reproducing the
tomfordbeauty post and thanking Ford for the collaboration; and (3) a post by Cardi B
containing a composite image of the lipstick next to Walsh’s photograph under a
header announcing the lipstick had sold out, the number of “likes” received, and Cardi
B’s caption “Sorry :/” (the “Post’’). Defendant moved for judgment on the pleadings,
asserting a fair use defense.

Issue

Whether embedding a social media post containing an unlicensed photograph in an
online news article discussing the post is fair use.

Holding

The court found that the first factor, the purpose and character of the use, strongly
favored a finding of fair use. XXL’s use of the photograph “was for an entirely
different purpose than originally intended” because XXL “did not use the Photograph
as a generic image of Cardi B to accompany an article about Cardi B,” but rather
“published the Post, which incidentally contained the Photograph, because the Post—
or put differently, the fact that Cardi B had disseminated the Post—was the very thing
the Article was reporting on.” Further, without the embedded Post, the article “would
have been nonsensical in appearance as well as potentially impossible, given that the
Post is embedded and hyperlinked, rather than inserted as an image.” XXL’s potential
profit from advertisements appearing with the article was outweighed by the
transformative, news reporting use. The second factor, the nature of the copyrighted
work, favored fair use because the Photograph contained both informational and
creative elements, rendering the overall creativity “neutral,” and because the work was
previously published by Cardi B. The third factor, the amount and substantiality of the
portion used in relation to the copyrighted work as a whole, also weighed in favor of
fair use because there was “no reasonable substitute” for using the actual Instagram
post in an article discussing the Instagram post. The fourth factor, the effect of the use
upon the potential market for or value of the copyrighted work, also weighed in favor
of fair use. Because the Post depicted the Photograph alongside another image and
included the Instagram text and tags, it is “implausible” that XXL’s use would
compete with Walsh’s licensing business or affect the market value for her
photograph. Overall, the court held the use was fair and dismissed the case.

Tags

Internet/Digitization; News reporting; Photograph

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright. gov/fair-use/.

