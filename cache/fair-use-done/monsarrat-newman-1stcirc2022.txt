Monsarrat v. Newman
28 F.4th 314 (1st Cir. 2022)

Year

2022

Court

United States Court of Appeals for the First Circuit

Key Facts

In 2010, Plaintiff Jonathan Monsarrat created a post on a community forum discussion
thread hosted on the Russian-owned social networking platform, LiveJournal.
Monsarrat’s post responded to allegedly defamatory posts about him on the thread by
threatening to report the other users for violations of LiveJournal’s harassment policy,
which he quoted and linked to in the post. Monsarrat subsequently registered his post
with the U.S. Copyright Office in 2012. In 2017, LiveJournal revised its terms of
service to comply with Russian law, which permitted censorship. As a result,
Defendant Ron Newman moved the forum, including Monsarrat’s registered post, to
another platform, Dreamwidth, by copying the forum’s discussion threads and
reposting them on Dreamwidth. Monsarrat sued for copyright infringement and
Newman moved to dismiss the claim, asserting fair use. The district court held that
Newman’s reproduction of the post as part of the discussion thread was fair use.
Monsarrat appealed.

Issue

Whether copying a post from one social networking platform to another is fair use.

Holding

Considering the first fair use factor, the purpose and character of the use, the court
found that Newman’s reproduction was “at least marginally” transformative because
Newman “reproduced Monsarrat’s work for a fundamentally different reason than that
which led to its creation.” The court noted that Monsarrat’s original purpose was to
“encourage users . . . to immediately stop harassing him,” which could not have been
Newman’s purpose in reproducing the post several years later. The court also found
that Newman’s use was noncommercial as there was no indication that Newman
reproduced the post “to accrue any profit,” a conclusion supported by Monsarrat’s
“repeated concession” that the post had no commercial value. Although favoring fair
use, the court gave the first factor little weight. The second factor, the nature of the
copyrighted work, weighed strongly in favor of fair use because the public post was
“factual and informational,” consisting primarily of a verbatim quote from the
LiveJournal harassment policy and “brief workaday prose.” The third factor, the
amount and substantiality of the portion used, favored neither side. The court, while
acknowledging that copying the entire post could weigh against fair use, found that in
this case it would have made “scant sense” for Newman to selectively copy only part
of the post as that would have “misrepresented” what Monsarrat wrote. The court
found the fourth factor, the effect of the use upon the potential market for or value of
the copyrighted work, weighed strongly in favor of fair use. The court relied in part on
Monsarrat’s admission that there was no potential market for the post. And although
the court recognized that a work’s value is not limited to monetary terms, it rejected
Monsarrat’s argument that the copyright itself bestowed the work with intrinsic value
because “such a value would be present in every case, and thus prove to be largely
beside the point in differentiating one case from another.” Balancing the factors, the
court found fair use and affirmed the judgment dismissing Monsarrat’s claim.

Tags

Internet/Digitization; Textual Work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

