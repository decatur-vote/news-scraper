Dlugolecki v. Poppel

CV 18-3905-GW (GJSx), 2019 U.S. Dist. LEXIS 149404 (C.D. Cal. Aug. 22, 2019)

Year

2019

Court

United States District Court for the Central District of California

Key Facts

In the 1990s, plaintiff John Dlugolecki (“Dlugolecki”) photographed American
actress Meghan Markle while she was a high school student; and these photographs
were published in her school yearbooks. Following the announcement of Markle’s
engagement to Prince Harry of Great Britain in 2017, defendant American
Broadcasting Companies, Inc. (“ABC”) used several of Dlugolecki’s photographs
during ABC programs covering news of the engagement. In total, ABC displayed
five photographs for a total of forty-nine seconds during eight hours of combined
broadcast time. The photographs were also shown in previews and social media
promotions for the broadcasts. Dlugolecki brought suit against ABC alleging
copyright infringement. ABC moved for summary judgment, arguing its use of the
photographs was fair use.

Issue

Whether using high school yearbook photographs in news broadcasts for
biographical purposes constitutes fair use.

Holding

The first factor, the purpose and character of the use, was described by the court as
being “in equipoise.” The court identified that although “news reporting” is
mentioned in the preamble to Section 107 of the Copyright Act, this does not mean
that any unauthorized use of a work in a news report is given a special “leg up.”
While the court found that ABC’s use of the photographs in television news was
somewhat transformative because the photographs were created for the purpose of
appearing in a yearbook, it was not “considerably, or overwhelmingly”
transformative. This slight transformativeness was balanced out by ABC’s “definite
commercial purpose or association” in using the photographs. On the second factor,
the nature of the copyrighted work, the court declined to “make any fine distinctions
between creative and factual works,” accepting that there was a minimal measure of
creativity in the photographs that slightly benefited Dlugolecki. Addressing the third
factor, the amount and substantiality of the work, the court did not accept ABC’s
argument that it only used the amount it needed of the photographs to fulfill its
biographical purpose because ABC could have achieved its goals of identifying
Markle without using Dlugolecki’s photographs at all. The photographs were not
themselves the subject of the broadcasts, nor were they necessary for the programs,
so the court concluded that the third factor did not demonstrably favor ABC. Lastly,
the court found the fourth factor, the effect of the use on the potential market for or
value of the work, favored Dlugolecki. The court found that there was a market for
licensing the photographs once Markle’s engagement was announced given our
“tabloid-fueled and celebrity-obsessed — and, more particularly, much of the
world's British royal family-obsessed — culture,” even if Dlugolecki had not
previously licensed the photographs. Weighing the factors together and finding that
none of them clearly favored ABC, the court concluded ABC failed to establish its
fair use defense and denied the motion.

Tags

Ninth Circuit; Photograph; News Reporting; Film/Audiovisual

Outcome

Preliminary finding; fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

