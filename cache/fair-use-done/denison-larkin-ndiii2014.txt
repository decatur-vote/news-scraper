Denison v. Larkin,
64 F. Supp. 3d (N.D. Ill. 2014)

Year

2014

Court

United States District Court for the Northern District of Illinois

Key
Facts

Plaintiff, a licensed attorney in the state of Illinois, operated a blog on which
she allegedly published false statements about judges and other lawyers.
Defendants, officers of the Illinois Attorney Registration and Disciplinary
Commission (IARDC) and IARDC itself, used software provided by co-
defendant Nextpoint to copy large swaths of the blog for use in disciplinary
proceedings against plaintiff. Plaintiff brought an infringement action
claiming that defendants infringed the copyrights in her blog posts by copying
“hundreds of pages from her blog” and that [ARDC improperly reproduced
“15 paragraphs of text” from her blog in a complaint filed against her in a
professional misconduct proceeding.

Issue

Whether it was fair use for defendants to copy large swaths of
plaintiff's blog and to reproduce specific portions of the blog for use in related
disciplinary proceedings against plaintiff.

Holding

The court ruled that defendants’ copying and reproduction of plaintiffs blog
for use in disciplinary proceedings was fair use. In reaching its conclusion,
the court pointed to legislative history and Seventh Circuit precedent naming
the reproduction of copyright protected works for litigation or other judicial
proceedings as an example of fair use. The court also found that all four fair
use factors weighed in favor of a finding of fair use.

Tags

Seventh Circuit; Internet/Digitization; Textual work; Used in government
proceeding

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

