Kelly v. Arriba Soft Corp.,
336 F.3d 811 (9th Cir. 2003)

Year

2003

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Plaintiff Leslie Kelly, a professional photographer, alleged that defendant
Arriba Soft Corp.’s search engine infringed Kelly’s photographs. Defendant
operated a visual search engine that “crawled” the internet searching for
images that it copied and then generated as smaller, lower-resolution
“thumbnail” copies for display on a search results page. Defendant
reproduced thirty-five of plaintiff’s photographs and displayed them as
thumbnails in response to search requests. Plaintiff appealed the district
court’s ruling that defendant’s use of plaintiff’s photographs in its search
engine was fair use.

Issue

Whether defendant’s display of thumbnail versions of copyright protected
images on an internet visual search engine constituted fair use.

Holding

The court held that defendant’s reproduction of plaintiff’s photos as
thumbnail images was fair use. The court deemed the use transformative
because the thumbnails served an entirely different function than the original
images. While plaintiff’s images were artistic works, the court found that
defendant’s use of them was “unrelated to any aesthetic purpose” and that the
search engine instead “functions as a tool to help index and improve access to
images on the internet and their related web sites.” The court also found that
the lower-resolution thumbnail images did not harm the market for or value of
plaintiff’s images.

Tags

Ninth Circuit; Internet/Digitization; Photograph

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

