Blackwell Publ’g, Inc. v. Excel Research Grp., LLC,
661 F. Supp. 2d 786 (E.D. Mich. 2009)

Year

2009

Court

United States District Court for the Eastern District of Michigan

Key
Facts

Plaintiffs, major publishing houses, alleged that defendant Excel Research
Group, LLC’s computerized copy shop infringed their copyrights in thirty-
three works. Defendants obtained course materials directly from professors
and copied those materials to create “coursepacks,” which were offered to
students at a price much lower than that of the separate publications.
Defendants’ unique business model also enabled it to charge less than
traditional copy shops and the university’s photocopy machines. Students
would make the copies themselves, using Excel’s “master” copy and Excel’s
machines. Students would fill out a declaration reading, “I am a student in
this class and am making a copy for educational purposes.” Excel paid no
licensing fees for the material. The publishers filed a motion for partial
summary judgment.

Issue

Whether the copying of coursepacks, as performed by students and facilitated
by defendants’ maintenance and lending of the master copies, constituted a
fair use.

Holding

The court held that defendants’ use of the protected material was not fair and
granted summary judgment in favor of plaintiffs. In reaching its decision, the
court relied on its findings that the purpose of defendants’ use was to facilitate
the operation of a “for-profit commercial business.” The court also
determined that the thirty-three works included in the coursepacks, as selected
by professors, were qualitatively significant even though only the necessary
amount of the work was reproduced. Finally, the court found that defendants’
use had an adverse impact on the marketplace for the works because
defendants were able to charge less than competitors for coursepacks, given
that they did not pay a licensing fee to plaintiffs.

Tags

Sixth Circuit; Education/Scholarship/Research; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

