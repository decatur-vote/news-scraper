Ringgold v. Black Entm’t Television, Inc.,
126 F.3d 70 (2d Cir. 1997)

Year

1997

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Plaintiff Faith Ringgold complained that defendants Black Entertainment
Television, Inc. and Home Box Office, Inc. infringed her copyright by using a
poster depicting her Church Picnic Story Quilt (Church Picnic) in a set
designed for a sitcom television series that aired on their networks. Church
Picnic is a “silk screen on silk quilt” painting that depicts the thoughts of a
parishioner at a 1909 Sunday school picnic held by the Freedom Baptist
Church in Atlanta, Georgia. The actual work is owned by The High Museum
of Art in Atlanta, to which plaintiff granted a non-exclusive license to
reproduce Church Picnic as a poster. In the television episode at issue, the
poster appears several times in a church scene where characters are waiting
for a recital. Plaintiff appealed the district court’s ruling that including the
poster in the set design without permission was fair use.

Issue

Whether defendants’ unauthorized use of a poster depicting plaintiff’s work to
decorate a television set constituted fair use.

Holding

Reversing the district court, the court of appeals ruled that defendants’ use of
the poster depicting plaintiff’s work was not fair use. The appeals court
criticized the district court’s flawed analysis of the impact on the market for
the work, saying it wrongly required plaintiff to show actual harm, such as a
drop in licensing requests, where she was only required to identify a
traditional, reasonable or likely-to-be-developed market for licensing her
work. For its analysis, the appeals court determined that defendants’ use was
not transformative since the work was used in the manner that plaintiff
originally intended —as decorative art work. The court further found that
defendants’ use could have a substantially adverse impact on the potential
market for the original work, especially given that plaintiff presented evidence
of the existence of a market for the exact use at issue, licensing the work as a
poster for dressing set designs.

Tags

Second Circuit; Film/Audiovisual; Painting/Drawing/Graphic; Photograph

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

