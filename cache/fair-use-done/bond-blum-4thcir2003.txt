Bond v. Blum,
317 F.3d 385 (4th Cir. 2003),
cert. denied 540 U.S. 820

Year

2003

Court

United States Court of Appeals for the Fourth Circuit

Key
Facts

Plaintiff William Bond wrote an autobiography in which he recounted the
true story of how he murdered his father. The autobiography was titled: Self-
Portrait of a Patricide: How I got Away with Murder. Years later, when
plaintiff’s wife was in a child-custody battle with her former husband, the
former husband introduced plaintiff’s manuscript as evidence that her current
home with plaintiff was not suitable for children. Plaintiff appealed the
district court’s ruling that the former husband’s use of the manuscript as
evidence in court was protected by fair use.

Issue

Whether reproducing a copyrighted manuscript to introduce as evidence in a
child-custody proceeding constituted a “fair use” of the manuscript.

Holding

The court held that the former husband’s use of the work was a permissible
fair use. The court based its decision on its finding that the former husband’s
purpose for using the work was narrowly related to the manuscript’s
evidentiary value. The court also ruled that, by introducing the work as
evidence, the former husband was relying on the “historical facts” therein, not
the “mode of expression” or any other creative element. Finally, the court
held that there was no evidence that admitting the manuscript into evidence
would adversely affect the work’s marketability.

Tags

Fourth Circuit; Textual work; Unpublished; Used in government proceeding

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

