Int’] Code Council, Inc. v. UpCodes, Inc.

17 Civ. 6261 (VM), 2020 U.S. Dist. LEXIS 92324 (S.D.N.Y. May 27, 2020)

Year

2020

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff, International Code Council, Inc. (“ICC”), develops a variety of model
codes for use in the construction industry. ICC provides free, read-only access to its
codes and licenses its codes and derivative works, such as supplemental training
materials and services. Upcodes, Inc., founded by Garrett and Scott Reynolds
(collectively “Defendants”), is a start-up company that aims to provide convenient
online access to materials used in the architecture, engineering and construction
industries, including ICC’s codes. At various times, Defendants posted on its website
free verbatim copies of ICC codes that were adopted into law by state or city
governments (“I-Codes as Adopted”), and offered paying customers Defendants also
make available to paying subscribers versions of ICC codes that depict in redline
sections of the model codes that were not adopted into law (“I-Code Redlines”). ICC
moved for summary judgment on copyright infringement of forty I-Codes; and
Defendants moved for partial summary judgment on their counterclaim for a
declaration of noninfringement, asserting the I-Codes are in the public domain as
well as defenses of merger, fair use, and collateral estoppel.

Issue

Whether it is a fair use to (1) post online free verbatim copies of privately-developed
codes that were adopted into law and (2) post online for paying customers copies of
those codes that include text that has not been adopted into law.

Holding

Having concluded that the I-Codes as Adopted are likely in the public domain, the
court proceeded with a fair use analysis of both the I-Code Redlines and I-Codes as
Adopted. The court found the posting of the I-Codes as Adopted was a fair use. The
first factor, the purpose and character of the use, weighed heavily in favor of fair use
because Defendants’ posting served a transformative purpose of “disseminating
enacted laws for public awareness.” The second factor, the nature of the work, also
weighed in favor of fair use because the adopted codes are “clearly factual” and
posted “in their capacity as laws.” The third factor, the amount and substantiality of
the work used, did not weigh against fair use because, although the copying was
substantial, “accurate copying” entails posting “ten-tenths of the law.” The fourth
factor, the effect of the use upon the potential market for or value of the work, could
weigh against fair use; however, there was no clear evidence of market harm and
such harm was unlikely to be determinative “given the combined weight of the other
three factors.” In contrast, disputes regarding material issues of fact prevented the
court from deciding whether the posting of I-Code Redlines was a fair use. The first
factor did not favor either party. Defendants argued that the I-Code Redlines help
educate the public; however, the court found the posting of these materials was
“debatable” because the ability to view unadopted text was of questionable value and
offering these materials “only to paying customers” could “offset any transformative
use.” The second factor weighed in favor of fair use “given the Redlines’
predominantly factual quality.” The third factor likely weighed against fair use
because Defendants’ purpose could have been accomplished without including the
unadopted text. Finally, there were factual disputes regarding the fourth factor
market effect. The parties’ cross-motions were denied.

Tags

Education/Scholarship/Research; Textual Work; Used in government proceeding

Outcome

Mixed result

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/fair-

index.html.

