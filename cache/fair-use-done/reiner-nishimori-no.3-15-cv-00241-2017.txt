Reiner v. Nishimori
No. 3:15-cv-00241 (M.D. Tenn. Apr. 28, 2017)

Year

2017

Court

United States District Court for the Middle District of Tennessee

Key
Facts

Plaintiff TC Reiner is a photographer who entered into an independent contractor agreement
with SuperStock, Inc. (“SuperStock”), a stock photo company, which paid Reiner to create
photographs for its use. SuperStock owned the copyrights to any photographs created under
the agreement. Reiner created a photograph called “Casablanca” which he provided to
SuperStock during the time period covered by the agreement, but lacked documentation as to
who paid the expenses for the shoot. In 2008, a graphic design professor at Defendant
Watkins Institute (“Watkins”) included Casablanca among photographs given to students,
including Defendant Ryon Nishimori, who were assigned to create mock advertisements using
the images. Nishimori uploaded a mock advertisement using Casablanca to his Flickr account
to archive it. Watkins did not use Casablanca or Nishimori’s mock advertisement for
advertising or promotional purposes, and did not introduce it into any market. Reiner filed
suit for direct and contributory copyright infringement against Watkins, copyright
infringement against Nishimori, and a violation of section 1202 of the DMCA against both.

Issue

Whether use by a university and student for an academic assignment of a photograph
constitutes fair use.

Holding

On Defendants’ motion for summary judgment, the court held that there was a disputed issue
of fact as to ownership, as Reiner and SuperStock disagreed as to whether Reiner had
transferred his copyright in Casablanca to SuperStock. But regardless, Defendants were not
liable for infringement because their uses were fair. With respect to the claims against
Watkins, as to the first factor, purpose and character of the use, the court held the use was for
“nonprofit educational purposes,” because the photograph was not being offered as material
for the students to learn, but rather to use to learn how to make mock advertisements. The
court held that the second factor, the nature of the copyrighted work, weighs slightly against
fair use, because Casablanca is “more creative than factual.” As to the third factor, the
amount of work used, the court held that it weighs against fair use because Watkins used the
entire photograph. Lastly, as to fourth factor, the effect on the market for the copyrighted
work, the court found Reiner did not establish any market harm because he did not prove “that
widespread use of photographs in the manner of Watkins or Nishimori would adversely affect
any potential market for his work.” As to Nishomori, the court analyzed his use of the
photograph for his mock advertisement, and on his Flickr account for storage. Its analysis
was largely the same, though noting that Nishimori transformed Casablanca into an
advertisement. Ultimately, the court held that “[t]he fourth factor weighs strongly in favor of
fair use” as to Nishimori. Summary judgment was therefore appropriate for Defendants,
which rendered the contributory infringement claim moot. The court granted summary
judgment on the DMCA claims as well, because, as to Nishimori, he did not know or have
reasonable grounds to know that removing Reiner’s information would “induce, enable,
facilitate, or conceal an infringement of the federal copyright laws”; as to Watkins, it had no
financial interest in the infringement.

Tags

Sixth Circuit; Photograph; Education/Scholarship/Research

Outcome

Fair use found

Source: U.S.

Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

