Benny v. Loew's, Inc.,
239 F.2d 532 (9th Cir. 1956),
aff’d by an equally divided court, 356 U.S. 43 (1958)

Year

1958

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Plaintiffs were the theater chain Lowe’s, Inc. (at that time, parent company of
Metro-Goldwyn-Myer Studios) and an English author, Patrick Hamilton, who
wrote the 1938 play Gas Light. In 1942, Hamilton gave Loew’s the exclusive
film rights for Gas Light, which film director Alfred Hitchcock adapted into a
popular movie of the same name. In 1952, without plaintiffs’ permission,
defendant CBS, Inc. broadcast Autolight, a burlesque of Hitchcock’s Gas
Light, starring comedian Jack Benny, also a defendant. Defendants asserted
that the burlesque was a fair use. Observing that this was a case of first
impression, the district court held that defendants’ use of substantial portions
of plaintiffs’ work to create a burlesque was not fair use. Defendants appealed.

Issue

Whether using substantial portions of an underlying work, such as a movie, to
create a burlesque version is fair use.

Holding

The circuit court upheld the lower court’s finding that the defendants’
burlesque version of Gas Light infringed plaintiffs’ copyright and was not a
fair use. In particular, the court noted that the doctrine of fair use was not
applicable to copying for the purpose of creating a burlesque. In rejecting the
defendants’ fair use argument, the court rejected defendants’ assertion that
their version of Gas Light was a parody and found that a “burlesque
presentation of [a work] is no defense to an action for infringement of
copyright.” Note: This holding was invalidated by the U.S. Supreme Court’s
decision in Campbell v. Acuff-Rose Music, Inc., 510 U.S. 569 (1994).

Tags

Ninth Circuit; Film/Audiovisual; Parody/Satire; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

