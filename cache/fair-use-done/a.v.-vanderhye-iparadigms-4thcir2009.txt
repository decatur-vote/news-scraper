A.V. ex rel. Vanderhye v. iParadigms, L.L.C.,
562 F.3d 630 (4th Cir. 2009)

Year

2009

Court

United States Court of Appeals for the Fourth Circuit

Key
Facts

Defendant iParadigms, LLC offered subscriptions to a computerized system
called Turnitin Plagiarism Detection Service (Turnitin) that enabled schools
to monitor for plagiarism by digitally comparing student work. Schools could
elect to archive student work in Turnitin’s database for continued use in these
digital comparisons. Plaintiffs, high school students, claimed that archiving
their schoolwork without their permission infringed their copyrights in those
works. The district court ruled that such archiving did not infringe plaintiffs’
copyrights and constituted fair use. Plaintiffs appealed.

Issue

Whether unauthorized digital archiving of student papers for purposes of
preventing plagiarism constituted fair use.

Holding

The appeals court upheld the district court: archiving student work for the
purpose of detecting plagiarism constituted fair use. It deemed the use
transformative because it was unrelated to the works’ expressive content and
was instead aimed at detecting and discouraging plagiarism. The court further
held that the use did not undermine plaintiffs’ right of first publication,
because iParadigms did not publicly disseminate the works or make them
available to any third party except the school. Finally, regarding the effect on
the market for student papers, the court found that Turnitin did not create a
market substitute for the papers. It did suppress demand for reuse of the
papers by later students, but copyright law does not protect against this kind
of harm.

Tags

Fourth Circuit; Computer program; Education/Scholarship/Research;
Internet/Digitization; Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

