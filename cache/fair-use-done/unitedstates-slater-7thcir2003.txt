United States v. Slater,
348 F.3d 666 (7th Cir. 2003)

Year

2003

Court

United States Court of Appeals for the Seventh Circuit

Key
Facts

Criminal defendants were members of an internet piracy group, “Pirates With
Attitudes” (PWA), which made pirated software freely available for use
without permission. In return for downloading pirated software, PWA
required members to participate in some aspect of its operations, such as
procuring the software, eliminating internal copyright protections, or testing
software before it could be uploaded to the group’s servers. Ultimately, the
FBI seized the group’s server, and several members were indicted for
criminal copyright infringement. At trial, the district court denied defendants’
request to instruct the jury on the fair use defense.

Issue

Whether it was proper for the district court to refuse to instruct the jury on fair
use in a criminal case involving software piracy over the Internet.

Holding

The circuit court affirmed the lower court’s refusal to instruct the jury on fair
use, finding defendants’ arguments—that their site was for noncommercial,
educational, and entertainment purposes only—“barely pass the straight-face
test.” While the court acknowledged that “[l]imited copying may be
permissible for certain noncommercial, educational purposes, taking into
account the nature of the copyrighted work and market considerations,” it
found that such factors “weigh against application of the fair use doctrine to
cases involving internet piracy.” The court concluded that it “is preposterous
to think that internet piracy is authorized by virtue of the fair use doctrine.”

Tags

Seventh Circuit; Computer program; Internet/digitization

Outcome

Preliminary ruling, mixed result, or remand

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

