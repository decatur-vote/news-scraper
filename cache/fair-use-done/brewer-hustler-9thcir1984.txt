Brewer v. Hustler Magazine, Inc.,
749 F.2d 527 (9th Cir. 1984)

Year

1984

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Plaintiff James Brewer created a photo using special effects and had it imprinted
on business cards, of which he distributed approximately 200 between 1975 and
1977. In 1980, he agreed to commercially distribute the photo on a postcard. In
1981, defendant Hustler Magazine, Inc. reproduced a portion of the postcard in a
magazine column without plaintiff’s permission. A jury found that Hustler had
infringed Brewer’s copyright. Hustler appealed, claiming among other things
that Hustler’s reproduction of the postcard constituted fair use.

Issue

Whether defendant’s reproduction of plaintiff’s postcard in its magazine was fair
use.

Holding

The Ninth Circuit concluded that there was sufficient evidence from which a jury
could have found that Hustler’s publication of Brewer’s photo was not fair use.
Regarding purpose, the jury could have reasonably concluded that the photo was
“used as a humorous feature designed to enhance readership, rather than as a
social commentary.” As to the nature of the work, the jury could have reasonably
found that the work was a creative product, rather than an information work for
which the fair use defense would have been broader. Regarding the amount of
the work, Hustler cropped off only a small portion of the photo, again supporting
the jury’s verdict. As to the market effect, the court looked to Sony Corp. v.
Universal City Studios, noting that harm to Brewer could be presumed because
the use was of a commercial nature. The court also observed that the jury could
have found that the value of the photograph as a novelty item could have
decreased due to overexposure.

Tags

Ninth Circuit; Photograph

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

