Bell v. Eagle Mt. Saginaw Indep. Sch. Distr.
27 F.4th 313 (5th Cir. 2022)

Year

2022

Court

United States Court of Appeals for the Fifth Circuit

Key Facts

Plaintiff Dr. Keith Bell, a sports psychologist, wrote and published Winning Isn’t
Normal, a book that provides strategies for success in athletics. Bell separately
registered the copyright for a 230-word excerpt from the book (the “WIN Passage’).
Bell sells merchandise that displays the WIN Passage online and licenses its use. In
December 2017, Chisholm Trail High School’s softball team and color guard posted
the WIN Passage to their Twitter accounts, crediting Bell as the author. In
November 2018, Bell notified Defendant Eagle Mountain Saginaw Independent
School District that the Twitter posts infringed his copyright. In response,
Defendant removed both posts, told Bell that the mistake was a “teachable
moment,” and announced a training program to avoid similar incidents. After
settlement negotiations failed, Bell brought an action for copyright infringement.
Defendant moved to dismiss, asserting that the school’s use was fair. The district
court granted Defendant’s motion and awarded it attorney’s fees. Bell appealed.

Issue

Whether use of a motivational passage excerpted from a book in social media posts
by public school athletics programs constitutes fair use.

Holding

Although Defendant conceded that the school’s use was not transformative, the
court nonetheless concluded that the first factor, the purpose and character of the
use, favored fair use because the use was noncommercial and in good faith. Despite
construing the pleadings in Bell’s favor, the court found the use noncommercial
because “[t]here is no logical theory for how tweeting Bell’s motivational message
to inspire students would enhance the reputations of [the school’s athletics]
programs” or provide Defendant a “tangible benefit.” The court held that the school
acted in good faith because Bell was credited in the posts and the school
immediately removed the posts after Bell complained. The second factor, the nature
of the work, disfavored fair use because, although comprised of “well-worn
truisms,” when viewed in the light most favorable to Bell, the WIN Passage is
“somewhat creative.” The court found that the third factor, the amount and
substantiality of the portion of the work used, was neutral because, even if the WIN
passage is the “essence of Bell’s book,” Bell himself makes the WIN passage
“freely accessible” to the public and the passage constitutes only a “small excerpt”
of the entire book. The fourth factor, the effect of the use upon the potential market
for or value of the work, favored fair use, as the court held that a short excerpt from
the book was not a competing substitute for the original. In fact, quoting a short
passage “might bolster interest in the book” and related merchandise. The court
found implausible the allegation that the school’s use could impact Bell’s ability to
license his work, noting that Bell failed to “allege that anyone has ever purchased a
license before posting the WIN Passage on social media” and that his “sole
authority” for the existence of a licensing market was his own litigation efforts.
Weighing the factors, the court held that the school’s use of the WIN Passage was
fair and affirmed the district court’s dismissal. It also affirmed the award of
attorney’s fees to deter Bell and other copyright holders from “suing public
institutions and nonprofit organizations over de minimis uses.”

Tags

Educational/Scholarship/Research; Internet/Digitization; Textual Work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use.

