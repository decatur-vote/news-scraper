Harper & Row Publishers, Inc. v. Nation Enterprises,
471 U.S. 539 (1985)

Year

1985

Court

Supreme Court of the United States

Key
Facts

Former President Gerald Ford sold plaintiff Harper & Row Publishers, Inc.
the right to publish his memoirs. Time magazine paid Harper & Row for the
exclusive right to serialize excerpts of the unpublished memoirs. Before Time
began serializing, defendant Nation Enterprises, publisher of The Nation
magazine, obtained an unauthorized copy of the unpublished memoirs and
published a 2,250-word article, at least 300-400 words of which constituted
verbatim quotes taken from the manuscript. Time magazine canceled its
agreement with Harper & Row, who sued The Nation for infringement.

Issue

Whether The Nation’s unauthorized reproduction of excerpts from the
unpublished memoirs was fair use.

Holding

The Court found defendant’s unauthorized reproduction of excerpts from the
unpublished memoirs was not fair use. The Court held that the unpublished
nature of a work is a key, though not necessarily determinative, factor tending
to negate a fair use defense. The Court also declined to expand fair use to
create “what amounts to a public figure exception to copyright” in response to
defendant’s First Amendment argument. Additionally, the Court held that
although the excerpts constituted a quantitatively insubstantial portion of the
memoirs, they represented “the heart of the book” and were qualitatively
substantial in view of their expressive value and their key role in the
infringing work. Finally, noting that the effect of the use upon the potential
market for the copyrighted work was the “single most important element of
fair use,” the Court found that the defendant’s use “directly competed for a
share of the market” and presented “clear-cut evidence of actual damage.”

Tags

U.S. Supreme Court; News reporting; Textual work; Unpublished

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

