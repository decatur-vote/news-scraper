New Line Cinema Corp. v. Bertlesman Music Grp., Inc.,
693 F. Supp. 1517 (S.D.N.Y. 1988)

Year

1988

Court

United States District Court for the Southern District of New York

Key
Facts

Plaintiffs, New Line Cinema Corporation and affiliated organizations (New
Line), owned the film rights for A Nightmare on Elm Street (Elm Street) and
subsequent sequels. New Line sought to license the Elm Street characters and
story for a music video based on the series, commencing negotiations with the
popular rap group, the Fat Boys. Representatives for defendants Bertlesman
Music Group and others contacted New Line about the possibility of using
D.J. Jazzy Jeff and The Fresh Prince instead for the song, sending a recording
by the duo called A Nightmare on My Street (My Street), but negotiations
between the parties ultimately failed. Later, the duo released an album which
included the song My Street. New Line subsequently finalized a deal with the
Fat Boys and ordered defendants to stop producing their record. Defendants
subsequently produced a music video for the song that was not broadcast,
after which the Fat Boys also released a licensed music video.

Issue

Whether the unauthorized use of a film’s story and character elements in a
music video was fair use.

Holding

The court ruled that defendants’ use of Elm Street in the music video My
Street was not fair use. The court found that defendants’ music video existed
solely as a video to promote the song, and defendants therefore stood to profit
financially by using elements from plaintiffs’ films without making the usual
licensing arrangements before they produced their video. Next, the court
found that Elm Street was a creative work of fiction or fantasy as opposed to a
factual work, which weighed against a finding of fair use. The court then
found that defendants had appropriated more of plaintiffs’ copyright-
protected works than was necessary for purposes of creating a purported
parody, thus weighing against a finding of fair use. Finally, the court found
that the My Street video, if released, would likely harm plaintiffs’ market for
licensing Elm Street character and story elements in the music video market
and would directly compete with the Fat Boys video.

Tags

Second Circuit; Film/Audiovisual; Music; Parody/Satire; Unpublished

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

