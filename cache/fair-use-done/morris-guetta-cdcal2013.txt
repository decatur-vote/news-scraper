Morris v. Guetta,
No. LA CV12-00684 JAK (RZX) (C.D. Cal. Feb. 4, 2013)

Year

2013

Court

United States District Court for the Central District of California

Key
Facts

Plaintiff Dennis Morris alleged that defendant Thierry Guetta infringed his
1977 photograph of Sid Vicious, former bassist for the Sex Pistols.
Defendant was an “appropriation artist” who created pop art pieces that
included modified pictures of celebrities. Defendant incorporated plaintiff’s
photo into seven pieces of artwork by altering the photo in different ways,
such as changing the color contrast and adding colors or other visual
elements.

Issue

Whether defendant’s unauthorized incorporation of plaintiff’s photograph into
art pieces constituted fair use.

Holding

The court granted plaintiff’s motion for partial summary judgment on the
issue of copyright infringement. The court found that defendant’s use did not
constitute fair use because the alterations were not sufficiently transformative.
The court found that the changes did not add something new, have a further
purpose, or give the photos a different character than the original, with new
expression, meaning, or message. As to the second factor, the court found
plaintiff’s photograph to be marginally creative, although it was more
documentary than aesthetic, weighing slightly against a finding of fair use.
For amount and substantiality of the portion used, because defendant relied on
and used the photograph itself to create the artworks, the court found this
factor weighed against fair use. Finally, on the fourth factor, though the court
found that there was no clear harm to the market for, or value of, the
photograph, it determined that even a lack of harm would not excuse an
unjustified use.

Tags

Ninth Circuit; Painting/Drawing/Graphic; Photograph

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

