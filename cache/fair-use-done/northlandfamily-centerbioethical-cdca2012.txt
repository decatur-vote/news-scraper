Northland Family Planning Clinic, Inc. v. Ctr. for Bio-Ethical Reform,

868 F. Supp. 2d 962 (C.D. Cal. 2012)

Year

2012

Court

United States District Court for the Central District of California

Key
Facts

Plaintiff Northland Family Planning Clinic (NFPC) produced videos for the
purpose of “outreach, counseling, and education in an effort to de-stigmatize
abortion.” Defendant Center for Bio-Ethical Reform (CBR) took NFPC clips
and narration to use in their own videos, juxtaposed with graphic images of
abortion procedures and aborted fetuses, and distributed their videos online.

Issue

Whether it was fair use for CBR to use clips from NFPC videos in its own
videos.

Holding

The court held that CBR’s use of the NFPC videos was fair use. It found that
the CBR video was a transformative parody that commented on the NFPC
videos. While the NFPC videos were “informational, functional, and
creative,” the court found this weighed only slightly in favor of plaintiff
because it determined that this factor was not “terribly significant” when
considering fair use, particularly in the context of parody. The court also
determined that, as a parody, the CBR video had more leeway regarding the
substantiality and amount of the original work used, and that CBR used no
more than necessary to convey its message. Lastly, the court found that there
would be no cognizable harm to the market for the original videos as a result
of CBR’s use, clarifying the difference between “biting criticism that merely
suppresses demand and copyright infringement, which usurps it.”

Tags

Ninth Circuit; Film/Audiovisual; Parody/Satire

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

