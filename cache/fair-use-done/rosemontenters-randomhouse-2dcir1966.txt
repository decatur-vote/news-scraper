Rosemont Enters., Inc. v. Random House, Inc.,
366 F.2d 303 (2d Cir. 1966)

Year

1966

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Plaintiff Rosemont Enterprises, Inc. was organized in 1965 by Howard
Hughes, who is known for pioneering work in aviation and motion pictures.
Plaintiff owned the copyright in a series of articles entitled The Howard
Hughes Story that appeared in three issues of Look Magazine in early 1954.
Defendants John Keats and Random House wrote and published the 1966
book Howard Hughes — a Biography by John Keats, which copied portions of
plaintiff’s articles. Defendants appealed the district court ruling, in the course
of granting a preliminary injunction, that they were likely not protected by the
fair use doctrine because a work published for commercial purposes that was
designed for the popular market precluded a fair use finding regardless of the
nature of the work involved.

Issue

Whether defendants’ unauthorized reproduction of plaintiff’s articles for use
in a biography constituted fair use.

Holding

The court reversed the district court’s preliminary injunction and found that
defendants’ claim of fair use was warranted. The court determined that the
district court erred by unjustifiably restricting the fair use defense to scholarly
works written and prepared for scholarly audiences. The court noted that “the
arts and sciences should be defined in their broadest terms” and include
biographies like defendants’. The court also stated that the district court
relied too heavily on the work’s commercial aspect when a commercial
motive or popular style of writing should be irrelevant to the determination of
whether a work offers a public benefit.

Tags

Second Circuit; Education/Scholarship/Research; Textual work

Outcome

Preliminary ruling, mixed result, or remand

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

