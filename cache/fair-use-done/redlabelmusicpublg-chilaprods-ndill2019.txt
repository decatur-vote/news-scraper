Red Label Music Publ’g v. Chila Prods.
18 C 7252, 2019 U.S. Dist. LEXIS 90159 (N.D. Ill. May 30, 2019)

Year

2019

Court

United States District Court for the Northern District of Illinois

Key Facts

Plaintiff Red Label Music Publishing, Inc. (“Red Label’) is a music publisher that
owns the copyrights in the words, music, sound recording, and video of the iconic
Super Bowl Shuffle, a hip-hop song and music video featuring members of the
Chicago Bears recorded en route to their victory in Super Bowl XX. Defendant
Chila Productions (“Chila”) produced a biographical documentary, ’85: The
Greatest Team in Football History, which featured eight seconds of the Super Bowl
Shuffle’s music and 16 separate short clips of the music video, totaling 59 seconds,
while discussing the Super Bowl Shuffle’s role in the Chicago Bears’ season. Red
Label brought suit against Chila and others involved with the film alleging
copyright infringement. Chila moved for judgment on the pleadings, or in the
alternative, summary judgment. Red Label moved to strike Chila’s fair use
affirmative defense and opposed summary judgment.

Issue

Whether using portions of a sound recording and a music video in a documentary
constitutes fair use.

Holding

Denying Red Label’s motion to strike, the court assessed the fair use defense. The
first factor, the purpose and character of the use, weighed in favor of fair use
because Chila did not use the Super Bowl Shuffle for its expressive content or
entertainment value, but rather for its factual content to tell a historical narrative. Its
presence in the film was “purely descriptive and designed merely to preserve a
specific aspect of [Bears] history.” Moreover, although the documentary was
produced for commercial gain, the use of the short clips from the Super Bowl
Shuffle was “incidental to the larger commercial enterprise of creating a historical
video for profit.” On the second factor, the nature of the copyrighted work, the court
noted that the Super Bowl Shuffle falls within the core of the Copyright Act because
it is “original, creative, and expressive.” Yet, because the portions of the Super Bowl
Shuffle were used to demonstrate historical facts, not the creativity of the original
expression, the court found the second factor neutral. Addressing the third factor,
the amount and substantiality of the work used, the court noted that Chila used only
two percent of the song and seventeen percent of the music video, which was “no
more than necessary” to serve as a historical reference point in the commentary.
Lastly, the court found the fourth factor, the effect of the use on the potential market
for or value of the work, did not weigh in either party’s favor. The court found the
plaintiffs demonstrated some licensing market for the clips, but failed to articulate
how defendants’ work would harm this market given that “the parties operate in
significantly different markets.” Moreover, the portions of the song in the
documentary were not a substitute for the original. Weighing the four statutory
factors together, the court determined that Chila’s use of the Super Bowl Shuffle was
fair and granted Chila’s motion for summary judgment.

Tags

Seventh Circuit; Film/Audiovisual; Music; Review/Commentary

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

