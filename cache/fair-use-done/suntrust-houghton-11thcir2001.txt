Suntrust Bank v. Houghton Mifflin Co.,
268 F.3d 1257 (11th Cir. 2001)

Year

2001

Court

United States Court of Appeals for the Eleventh Circuit

Key
Facts

Defendant publisher Houghton Mifflin owned the rights to the 2001 novel
The Wind Done Gone (TWDG), which was based on and critiqued Margaret
Mitchell’s well-known 1936 novel Gone With The Wind (GWTW). Plaintiff
Suntrust Bank, trustee for the Mitchell heirs, owned and managed GWTW’s
copyrights, including derivative works. Plaintiff alleged that TWDG violated
the trust’s copyright interests by explicitly referring to GWTW in the
foreword; copying core characters, traits, and relationships; copying and
summarizing famous scenes; and copying verbatim certain dialogues and
descriptions. Defendant appealed the district court’s ruling in plaintiffs
favor.

Issue

Whether a novel critiquing/parodying another novel through verbatim and
nonliteral copying of characters/plot was fair use.

Holding

The court held that defendant’s borrowing from GWTW was fair use. The
court first found that the defendant’s novel was in fact a parody because it
commented on or criticized an original work by appropriating its elements to
create a new artistic work. Specifically, TWDG criticizes GWTW’s depiction
of slavery and race relations in the antebellum South.

The court found that although TWDG clearly had a commercial purpose, its
significantly transformative nature outweighed that fact. According to the
court, TWDG added significant new expression and meaning by transforming
GWTW from a third-person epic to a first-person diary or memoir.
Additionally, the last half of TWDG told a completely new story that, while
incorporating GWTW characters and settings, featured plot elements found
nowhere in the original work. The court gave little weight to the “nature of
the work” factor, finding that while GWTW was an original work of fiction
entitled to the greatest degree of protection, parodies almost invariably copy
publicly known expressive works. Regarding the amount of GWTW used, the
court found that, based upon the record before it, it could not determine “in
any conclusive way” whether the quantity and value of the materials used was
reasonable in relation to the purpose of the copying. Finally, the court found
that plaintiff’s evidence failed to demonstrate that TWDG would have a
deleterious effect on the market for GWTW by displacing sales.

Tags

Eleventh Circuit; Parody/Satire; Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

