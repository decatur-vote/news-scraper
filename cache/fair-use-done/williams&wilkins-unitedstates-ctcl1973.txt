Williams & Wilkins Co. v. United States,
487 F.2d 1345 (Ct. Cl. 1973)

Year

1973

Court

United States Court of Claims

Key
Facts

Plaintiff Williams & Wilkins Co. was a major publisher of medical journals
and books. Defendants National Institutes of Health and National Library of
Medicine would, upon request, photocopy articles in medical journals
published by plaintiff and distribute them to researchers. Defendants did not
monitor the reason for the copy requests, and the requesting researcher could
retain the copy permanently. Plaintiff sued for copyright infringement, and
defendants raised the fair use defense.

Issue

Whether the defendants’ unauthorized photocopying of plaintiff’s articles for
use by medical researchers constituted fair use.

Holding

The court held that defendants’ unauthorized photocopying of plaintiffs
articles was fair use because the copies were made for the researchers’ own
professional use and not for profit or other gain. Defendants also placed strict
limitations on photocopying, which kept “the duplication within appropriate
confines.” The court based its fair use decision on three main determinations.
First, the court noted that plaintiff did not prove that it had been or would be
harmed substantially by defendants’ practices. Second, the court was
convinced that holding defendants’ practices to be copyright infringement
would “no doubt” harm medicine and medical research. Third, the court held
that balancing the interests of science with those of publishers required a
legislative solution.

Tags

Federal Circuit; Education/Research/Scholarship; Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

