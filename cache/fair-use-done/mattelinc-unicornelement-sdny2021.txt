Mattel, Inc. v. Entities Doing Bus. as Unicorn Element

21 Civ. 2333 (VM), 2021 U.S. Dist. LEXIS 106188 (S.D.N.Y. June 4, 2021)

Year

2021

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff Mattel, Inc. (“Mattel”) manufactures and sells Barbie dolls, doll clothes, and
doll accessories. It markets these products in various ways, including in a style guide
for a Barbie movie that contains an illustration of Barbie (the “Barbie Illustration”).
Defendants are various persons and entities engaged in the online sale and distribution
of doll clothes and accessories. Defendants used the Barbie Illustration in a small
section of the product packaging for a doll closet. Mattel filed an action against
Defendants, alleging copyright infringement for use of the Barbie Illustration and
trademark infringement for use of the “BARBIE” mark in the promotion and sale of
Defendants’ products. Defendants conceded that they used the Barbie Illustration on
the packaging of their products and that Mattel holds the exclusive rights to the
illustration, but argued that the use of the Barbie Illustration is fair use.

Issue

Whether incorporating an illustration on a small section of product packaging for a
related product constitutes fair use.

Holding

Considering the first fair use factor, the purpose and character of the use, Defendants
used the Barbie Illustration commercially, which weighed against finding fair use.
Defendants contended that their use was “highly transformative,” but the court found
Defendants’ use was the same as Mattel’s, that is, to sell doll-related merchandise.
Although it did not play a significant role in the court’s determination, the second
factor, the nature of the copyrighted work, weighed against fair use as Defendants did
not present a substantial argument that the Barbie Illustration was not the core type of
work that copyright is intended to protect. The third factor, the amount and
substantiality of the portion used, weighed against a finding of fair use because
although the Barbie Illustration is only a small portion of the Mattel style guide and of
Defendants’ product packaging, Defendants used the most important and identifying
portion of the Barbie Illustration, the face, and provided no convincing reason for
doing so. For the fourth factor, the effect of the use upon the potential market for or
value of the copyrighted work, the court was also persuaded that this factor weighed
against fair use because of the clear market harm to Mattel if Defendants and other
doll clothing and accessory manufacturers were permitted to engage in this conduct.
The court reasoned that if others were “given free rein” to copy Mattel’s work—even
a small amount—without justification or a license, Mattel would likely be forced to
continue to bring lawsuits to protect its intellectual property. Because all four factors
weighed against fair use, Mattel demonstrated a likelihood of success on the merits
and the Court granted Mattel’s motion for prejudgment attachment.

Tags

Internet/Digitization; Painting/Drawing/Graphic

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

