Yang v. Mic Network Inc.

Nos. 20-4097-cv(L), 20-4201-ev (XAP), 2022 U.S. App. LEXIS 8195 (2d Cir. Mar. 29, 2022)

Year

2022

Court

United States Court of Appeals for the Second Circuit

Key Facts

Plaintiff Stephen Yang (“Yang”) licensed a photograph he took of Dan Rochkind
(“Rochkind”) to the New York Post, which ran the photograph in an article about
Rochkind entitled “Why I Won’t Date Hot Women Anymore.” Defendant Mic
Network, Inc. (“Mic”) posted its own article entitled “Twitter Is Skewering the 'New
York Post' for a Piece on Why a Man ‘Won't Date Hot Women’.” The Mic article
included a screenshot of the Post article that captured the headline and a portion of
Yang’s photograph. Mic did not obtain a license to use the photograph. In response,
Yang sued Mic for copyright infringement, and Mic moved to dismiss, asserting fair
use. The district court granted Mic’s motion, concluding that its use of Yang’s
photograph was fair use. Yang appealed the order and judgment.

Issue

Whether using a screenshot from an article, including part of a photograph, to report
on and criticize the article constitutes fair use of the photograph.

Holding

On appeal, the court decided that the first factor, the purpose and character of the
use, weighed in favor of fair use. As an initial matter, the panel held that it was not
error for the district to decide transformativeness on a motion to dismiss in this case
because the only two pieces of evidence needed were the original and secondary
works. The court held that, in addition to identifying the subject of Mic’s criticism,
Mic, also transformed the photograph by critiquing and providing commentary on
the Post article. Mic did not use the photograph “merely as an illustrative aid,” and
thus its use was for different purpose than the original. The second factor, the nature
of the copyrighted work, had limited weight in the court’s analysis after it held that
the use was transformative and thus “d[id] not counsel against a finding of fair use.”
Likewise, the third factor, the amount and substantiality of the work used, did not
disfavor fair use as the court agreed with the district court’s conclusion that Mic’s
use of the image was reasonable to satirize the Post article. The court determined
that the fourth factor, the effect of the use on the potential market for or value of the
work, also favored fair use. The court concluded that Mic’s screenshot was not a
competing substitute for Yang’s work because Mic did not simply republish the
photograph, but instead used a screenshot consisting of a “significantly” cropped
version of the work along with the Post headline. Further, Yang failed to plausibly
allege that a market exists for “photographs that happen to be featured in news
articles criticizing the original article in which the photograph appeared.” Weighing
the factors together, the court concluded that the district court properly dismissed
Yang’s copyright infringement claim on fair use grounds.

Tags

News Reporting; Photography

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

