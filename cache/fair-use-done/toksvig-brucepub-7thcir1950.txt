Toksvig v. Bruce Pub. Co.,
181 F.2d 664 (7th Cir. 1950)

Year

1950

Court

United States Court of Appeals for the Seventh Circuit

Key
Facts

Plaintiff Signe Toksvig authored a biography of Hans Christian Andersen
entitled The Life of Hans Christian Andersen (1934). In authoring the
biography, plaintiff conducted three years’ worth of original research from
exclusively Danish sources, including primary sources such as Andersen’s
letters and interviews with Andersen’s contemporaries. Margaret Hubbard
authored a novel based on the life of Hans Christian Andersen entitled Flight
of the Swan (1946). In the year it took Hubbard to author her novel, she
consulted only English sources, including plaintiff’s biography. Plaintiff
alleged that Hubbard’s novel copied certain passages from the biography and
initiated an infringement action against defendant Bruce Publishing Co., the
company that published Hubbard’s novel. Defendant appealed the district
court’s ruling that Hubbard’s novel infringed plaintiff’s copyright in the
biography by copying twenty-four specific passages of translation from the
work.

Issue

Whether defendant’s copying of twenty-four passages of translation from
plaintiff’s work was fair use.

Holding

The Seventh Circuit affirmed the lower court’s ruling that defendant’s
copying was not a fair use. The court rejected defendant’s argument that the
information taken from the biography was factual, finding that defendant’s
use of plaintiff’s translated passages saved her considerable time, allowing
defendant to complete her novel much more quickly than if she had done the
research herself.

Tags

Seventh Circuit; Education/Scholarship/Research; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

