Norse v. Henry Holt & Co.,
847 F. Supp. 142 (N.D. Cal. 1994)

Year

1994

Court

United States District Court for the Northern District of California

Key
Facts

Defendant Henry Holt & Co. published defendant Ted Morgan’s biography of
author William S. Burroughs entitled Literary Outlaw: The Life and Times of
William S. Burroughs. Morgan quoted several short phrases, about fifty
words in total, from unpublished letters written by plaintiff Harold Norse.
Plaintiff sued for copyright infringement, and defendants asserted that
copying short phrases was not actionable under copyright law. On appeal, the
circuit court remanded the case for the district court to consider defendants’
fair use defense.

Issue

Whether defendants’ unauthorized use of excerpts from plaintiff’s
unpublished work constituted fair use.

Holding

On remand, the district court concluded that defendants’ copying of short
phrases was fair use. Although the defendants’ commercial use and the
unpublished nature of plaintiff’s letters weighed against a finding of fair use,
the court found that the use did not exploit the publicity value of the quoted
material and was only minimally commercial. Additionally, the court found
that no reasonable juror could conclude that the small amount of “copied
material goes to the ‘heart,’ if one can even be identified, of the copyrighted
letters.” Finally, the court found that even if there was a market for plaintiff’s
unpublished letters, defendants’ small and fragmentary “use of the copied
material in no way supplants that market.”

Tags

Ninth Circuit; Education/Scholarship/Research; Textual work; Unpublished

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

