Consumers Union of U.S., Inc. v. Gen. Signal Corp.,
724 F.2d 1044 (2d Cir. 1983)

Year

1983

Court

United Stated Court of Appeals for the Second Circuit

Key
Facts

Plaintiff Consumers Union of U.S., Inc. (CU) published Consumer Reports, a
magazine that provided consumer information about products and services,
including household items. CU alleged that defendants, manufacturers and
sellers of vacuum sweepers, infringed by quoting a CU article in two
television commercials for vacuum sweepers without plaintiff’s authorization.
Both commercials disclaimed any affiliation with CU or any endorsement by
CU. Defendants appealed the district court’s grant of a preliminary injunction
to plaintiff.

Issue

Whether defendants’ unauthorized quotation of a Consumer Reports article in
a television commercial for vacuum sweepers constituted fair use, requiring
that a preliminary injunction against defendants be vacated.

Holding

The Second Circuit reversed the district court and vacated the preliminary
injunction, holding that defendants’ use was a fair use. The court found that,
although defendants’ use of plaintiff’s work was obviously commercial in
nature, the advertisements conveyed useful information protected by the First
Amendment. Further, the court found that the defendants’ purpose was to
report factual information, weighing in favor of a finding of fair use. The
court then determined that the informational—as opposed to creative—nature
of plaintiff’s publication favored a finding of fair use. The court then noted
that defendants used only twenty-nine words out of the 2,100 words contained
in the relevant article, a relatively insubstantial use. Finally, regarding the
market effect, the Second Circuit rejected the district court’s emphasis on the
adverse impact of defendants’ use on plaintiff’s reputation as an unbiased
reviewer of consumer products. The fourth factor, the court reasoned, “is
aimed at the copier who attempts to usurp the demand for the original work,”
and there was no convincing evidence that defendants’ commercial would
substitute for back issues of Consumer Reports.

Tags

Second Circuit; Film/Audiovisual; Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

