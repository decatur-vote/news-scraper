Cable/Home Commc’n Corp. v. Network Prods., Inc.,
902 F.2d 829 (11th Cir. 1990)

Year

1990

Court

United States Court of Appeals for the Eleventh Circuit

Key
Facts

Plaintiffs Home Box Office, Inc. and Showtime/The Movie Channel, Inc.
alleged that defendants infringed the encryption software plaintiffs used to
limit access to their cable programming. Defendants, video production
company Network Productions, Inc. and its president Shaun Kenny, financed
the production and distribution of decryption software that incorporated
substantial portions (86%) of plaintiffs’ copyright protected software for the
express and widely publicized purpose of giving non-subscription viewers
access to plaintiffs’ programming. The district court ruled that defendants
were liable for willful, direct and contributory infringement. Defendants
appealed, asserting fair use.

Issue

Whether defendants’ use of plaintiffs’ software to create decryption software
that gave non-subscribers access to plaintiffs’ cable television programming
was fair use.

Holding

The Eleventh Circuit upheld the lower court’s finding that defendants’
decryption software infringed plaintiffs’ software and was not fair use. The
court held that none of the four statutory factors supported defendants’ fair
use defense. In particular, the court found that the defendants’ “flagrant
commercial purpose” in using virtually all (86%) of plaintiffs’ copyrighted
computer program to achieve the descrambling of plaintiffs’ encrypted
television signals did obvious damages to plaintiffs’ present and future
markets.

Tags

Eleventh Circuit; Computer program

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

