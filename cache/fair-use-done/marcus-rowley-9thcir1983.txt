Marcus v. Rowley,
695 F.2d 1171 (9th Cir. 1983)

Year

1983

Court

United States Court of Appeals for the Ninth Circuit

Key
Facts

Plaintiff public school teacher wrote a booklet on cake decorating that she
sold to students in adult education cake decorating classes. Defendant public
school teacher enrolled in plaintiff’s class and purchased the

booklet. Defendant later prepared her own booklet for use in food service
career classes. Defendant’s booklet used eleven pages taken verbatim from
plaintiff’s work. Defendant made fifteen copies of her booklet and put them
on file for her students’ use. Plaintiff appealed the district court’s ruling that
defendant’s copying for nonprofit educational purposes was fair use.

Issue

Whether defendant’s copying of another teacher’s copyrighted course
material for purposes of classroom teaching on the same subject constituted
fair use.

Holding

The court concluded that defendant’s copying was not fair use. Pointing out
that defendant’s nonprofit educational purpose did not automatically compel a
finding of fair use, the court stated that copying a work for the same intrinsic
purpose that the copyright owner intended weighs strongly against a finding
of fair use. The court found that both plaintiff’s and defendant’s booklets
were prepared for the identical purpose of teaching cake decorating. The
court also found that, qualitatively and quantitatively, defendant’s work
substantially copied the original because almost 50% of the work was a
verbatim copy and that 50% contained virtually all of the substance of
defendant’s booklet. Although the court did not find any effect on the
potential market or measurable pecuniary damages, the other factors were
enough to support a finding against fair use.

Tags

Ninth Circuit; Education/Scholarship/Research; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

