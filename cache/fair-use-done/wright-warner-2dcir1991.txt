Wright v. Warner Books, Inc.,
953 F.2d 731 (2d Cir. 1991)

Year

1991

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Defendant Margaret Walker, an author associated with the Chicago African-
American literary movement, wrote an unauthorized biography of renowned
author and poet Richard Wright. Defendant Warner Books, Inc. published the
biography, which paraphrased and quoted from a selection of Wright’s
unpublished works, such as journal entries and letters. Plaintiff Ellen Wright,
Wright’s widow, appealed the district court’s judgment that Walker’s use of
her deceased husband’s unpublished works was fair use.

Issue

Whether defendants’ unauthorized reproduction of excerpts and paraphrased
excerpts of an author’s unpublished works in a biography about that author
was fair use.

Holding

Agreeing with the district court, the circuit court held that defendants’
unauthorized reproduction of excerpts and paraphrased excerpts from the
unpublished works was fair use. To reach that conclusion, the court relied on
its findings that the “analytic research” contained in defendants’ work was
transformative because it “added value” to the original works. The court also
found that the excerpts used were neither the “heart” of the unpublished
works nor a quantitatively significant portion of the works. Finally, the court
found that defendants’ work did not pose a “significant threat to the potential
market” for the unpublished works.

Tags

Second Circuit; Education/Scholarship/Research; Textual work; Unpublished

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

