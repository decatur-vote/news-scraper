Erika Peterman y. Republican National Committee
No. 17-66-M-DLC (D. Mont. March 19, 2018)

Year

2018

Court

United States District Court for the District of Montana

Key Facts

Plaintiff Erika Peterman is a photographer who was hired by the Montana
Democratic Party to take photographs of Democratic candidate Rob Quist at an
event; she took the photographs and gave limited license to the Montana
Democratic Party to use the photographs. Two months later, Peterman was
informed that the Republican National Committee (“RNC”), a U.S. political
organization responsible for developing and promoting the Republican political
platform, had sent out a mass direct mailing, using one of her photographs of Quist
(“Work”), that was designed to negatively depict him. Peterman filed a complaint
alleging copyright infringement and intentional interference with economic
advantage based on RNC’s copying, using, and distributing her photograph. RNC
filed a motion to dismiss, alleging its use was a fair use.

Issue

Whether use of a copyrighted photograph by a political party for a political mailing
is a fair use.

Holding

The court determined that RNC’s use of Peterman’s photograph could not, at the
motion to dismiss stage, be found fair. The court concluded that the first factor,
purpose and character of the infringing work, “d[id] not favor a finding of fair use
at this stage since the Work’s purpose remains a disputed issue of fact and RNC’s
use was, at best, minimally transformative.” Indeed, “absent the inclusion of a
treble clef attached to the commentary, the entirety of the visual aspects of the
Work remain unaltered in RNC’s use.” The second factor, nature of the
copyrighted work, “weigh[ed] against a finding of fair use” because the Work
“includes elements in its framing that are not factual in nature,” and rather
“creatively and visually develops a portrait of Quist and his candidacy.” The third
factor, amount and substantiality of the portion used, “weigh[ed] against a finding
of fair use” because RNC “copied the entirety of Peterman’s Work quantitatively
and qualitatively for use in its political mailers.” The final factor, effect of the use
upon the potential market, could not be assessed because it “remains a disputed
issue of fact;” “it is uncertain whether the use of the Work in a political mailing
criticizing Quist is likely to diminish the potential sale of the Work, interfere with
the marketability of the Work, or fulfill the demand for the Work.” Weighing the
factors, and resolving all issues in favor of Peterman, the court found that “disputed
issues of material fact remain on whether RNC’s use of Peterman’s Work
constituted a fair use.” The court thus denied RNC’s motion to dismiss the
copyright infringement claim.

Tags

Ninth Circuit, Photograph, Review/Commentary

Outcome

Preliminary ruling, Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

