Philpot v. Media Research Center Inc.
No. 1:17-cv-822 (E.D. Va. Jan. 8, 2018)

Year

2018

Court

United States District Court for the Eastern District of Virginia

Key
Facts

Plaintiff Larry Philpot is a professional photographer who takes photographs of
musicians in concert. Defendant Media Research Center is a non-profit organization
that “publishes news and commentary regarding issues of public debate in order to
expose and critique media bias against American Judeo-Christian beliefs.” This case
involves Plaintiff's copyright interests in two photographs: (1) a photograph of
Kenny Chesney performing in concert, and (2) a photograph of Kid Rock performing
in concert. Plaintiff uploaded the photographs to Wikimedia, where they were
available for use, subject to a Creative Commons attribution license. Defendant used
the Chesney photograph in an article about pro-life celebrities and the Kid Rock
photograph in an article about his bid for Senate, without attributing either
photograph to Plaintiff. Plaintiff brought a copyright infringement action against
Defendant, who filed a motion for summary judgment, in relevant part arguing that
its use of the photographs was fair.

Issue

Whether Defendant’s use of plaintiff's photographs of musicians in articles
concerning the musicians’ political beliefs constitute fair use of the photographs.

Holding

After conducting the four-step analysis, the court concluded that Defendant’s use of
the photographs constituted fair use and granted the Defendant’s motion for summary
judgment. As to the first factor, the purpose and character of the use, the court found
the use of the photographs was transformative since “[D]efendant’s use of the
[p]hotographs [in the articles] is plainly different from [P]laintiff’s intended use of
the [p]hotographs.” Defendant used the photographs “for the purposes of news
reporting and commentary,” compared with Plaintiff's intent to “depict the musicians
in concert.” The court also found that the use of the photographs was essentially non-
commercial since Defendant received only minor amounts of revenue and donations
from the articles. With regard to the second factor, the nature of the copyrighted
work, the court found that the factor was neutral since the photographs “are likely
both factual and creative” in that they depict celebrities but Plaintiff made creative
choices in taking the photographs. As to the third factor, the amount and
substantiality of the use, the court found that “[D]efendant used all of [the] Chesney
Photograph and only slightly cropped [P]laintiff's photograph of Kid Rock.”
Therefore, this factor favored Plaintiff. Lastly, the fourth factor, the effect on the
potential market for the copyrighted work, weighed in favor of the Defendants,
because “there is no showing on this record of any impact on any economic market
for the Chesney or Kid Rock [p]hotographs.” Thus, the court held the use was fair.

Tags

Fourth Circuit; Photograph; Review/Commentary; Internet/Digitization

Outcome

Fair use found

Source: U.S.

Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

