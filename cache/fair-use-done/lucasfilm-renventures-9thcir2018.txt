Lucasfilm Ltd. LLC, et al. v. Ren Ventures Ltd., et al.
No. 17-cv-07249-RS (N.D. Cal. June 29, 2018)

Year

2018

Court

United States District Court for the Northern District of California

Key Facts

Plaintiffs Lucasfilm Ltd. LLC and Lucasfilm Entertainment Company Ltd. LLC
(collectively, “Lucasfilm”) own the copyrights covering at least three Star Wars
works (the “Works”). Defendants Ren Ventures and Sabacc Creative Industries
Ltd. are the creators and distributors of “Sabacc — The High Stakes Card Game,” a
mobile game app. The game allegedly mimics one that appears in Star Wars. To
promote the app on Facebook and Twitter, defendants utilized dialogue and images
from Lucasfilm’s Works. Lucasfilm sent a cease-and-desist letter to defendants,
after which Lucasfilm filed a complaint alleging copyright infringement, trademark
infringements, and two other claims. Lucasfilm moved for summary judgment on
its copyright claim.

Issue

Whether the use of copyrighted dialogue and images to promote an app is fair.

Holding

The court determined that defendants’ use of the Works was not fair. The court
concluded that the first factor, purpose and character of the infringing work,
weighed in favor of Lucasfilm. Because defendants “merely reposted images and
dialogue from original works” with only some minor alterations or additions, the
use was not transformative. Moreover, in posting the Works to promote their app,
defendants’ use was commercial. The second factor, nature of the copyrighted
work, favored defendants. While the Works are expressive, they have also been
“published extensively,” since images, illustrations, and similar quotes appear at
numerous locations online. The court observed: “As a result [of the publication of
the Works], the authors have likely realized their expressive and economic interests
to a great extent.” The third factor, amount and substantiality of the portion used,
weighed for a finding of fair use, because defendants used clips only seconds long.
In comparison to the Works, these clips “are quantitatively insignificant.” The final
factor, effect of the use upon the potential market, weighed against fair use. The
court found “these GIF images can . . . have an adverse effect on the derivative
market.” The court determined that because defendants use was non-transformative
and commercial, it could presume the likelihood of market harm. The court
concluded that since defendants did not “produce[] evidence to show lack of market
harm to Lucasfilm from their unlicensed use, this factor weighs against fair use.”
Weighing the factors, the court heavily considered “the more important first and
fourth factors” and concluded that defendants’ use was not fair. Therefore, the
court granted Lucasfilm’s motion for summary judgment on its claim of copyright
infringement.

Tags

Ninth Circuit, Film/Audiovisual, Internet/Digitization, Review/Commentary

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

