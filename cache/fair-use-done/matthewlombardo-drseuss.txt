Matthew Lombardo and Who’s Holiday LLC v. Dr. Seuss Enterprises, L.P.
No. 1:16-cv-09974-AKH (S.D.N.Y. Sept. 15, 2017)
aff’d, No. 17-2952-cv, 2018 WL 3323476 (2d Cir. July 6, 2018)

Year

2017, affirmed 2018

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiff Matthew Lombardo authored the play Who's Holiday, and Defendant Dr.
Seuss Enterprises owns the copyright in the book How the Grinch Stole Christmas!,
which was authored by popular children’s author Dr. Seuss. Who’s Holiday “make[s]
fun of’ and “criticize[s]” Grinch by incorporating its characters, plot elements, and
distinctive rhyming style into a “bawdy, off-color” Christmas comedy that imagines
Cindy Lou Who, a Grinch character, in middle-age. In 2016, Defendant sent
Plaintiffs a cease-and-desist letter alleging copyright infringement, after which
Plaintiffs halted production on Who’s Holiday and filed suit against Defendant,
seeking, among other claims, a declaratory judgment that the play constitutes fair use.
Defendant filed counterclaims alleging copyright and trademark infringement. The
court invited Plaintiffs to file a motion for judgment on the pleadings on the issue of
fair use, stating that fair use could be resolved by conducting a side-by-side
comparison of Who's Holiday and Grinch.

Issue

Whether Plaintiffs’ use of elements from Grinch in the play Who’s Holiday
constitutes fair use.

Holding

Following its four-step fair use analysis, the court held that Who’s Holiday is a fair
use, and it granted Plaintiffs’ motion for judgment on the pleadings and dismissed
Defendant’s counterclaims for copyright and trademark infringement. As to the first
factor (the purpose and character of the use), the court found that the work is a parody
since it “subverts the expectations of the Seussian genre,” and therefore it is
necessarily transformative. Because the work was deemed to be transformative, the
court gave less weight to the fact that the work is of a commercial nature, also a
consideration under the first factor. In discussing the second factor (the nature of the
copyrighted work), the court reasoned that Grinch is “closer to the core of intended
copyright protection” since it is sufficiently creative to merit parodying, but the court
noted, for that reason, the second factor is generally of little significance in a parody
case. As to the third factor (the amount and substantiality of the use), the court
explained that Who’s Holiday’s “use of Grinch is not excessive in relation to the
parodic purpose of the copying.” Lastly, the court held that the fourth factor (the
effect on the potential market for the copyrighted work), favored a finding of fair use,
because the intended “adult audience[]” for Who’s Holiday did not interfere with the
market for the original book or the licensing market for derivative works. While
Defendant claimed to have previously authorized works that included “themes and
jokes aimed at adult audiences,” the court reasoned that Defendant was unlikely to
license a parody referencing “bestiality, drug use, and other distinctly ‘un-Seussian’
topics.”

On appeal, the Second Circuit found that the district court had correctly analyzed
each factor. Regarding the third factor, the Second Circuit noted that Who’s Holiday
does not “copy verbatim or quote from the original book.” The Second Circuit



------------

affirmed the district court’s decision.

Tags 

Second Circuit; Parody/Satire; Textual work

Outcome 

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fairuse/index.html.

