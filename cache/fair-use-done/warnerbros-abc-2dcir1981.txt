Warner Bros., Inc. v. Am. Broad. Cos., Inc.,
654 F.2d 204 (2d Cir. 1981)

Year

1981

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Plaintiffs, Warner Bros., Inc. and D.C. Comics, Inc., owned copyrights for the
character “Superman” and works embodying Superman, including comic
books, animated and live-action television series, and a motion picture.
Defendant, ABC, Inc. created a television series called The Greatest
American Hero which starred the fictional superhero “Ralph Hinkley.”
Plaintiffs alleged that defendant’s Ralph Hinkley character and related
promotional campaign materials infringed its Superman copyrights. The
district court refused to grant plaintiffs a preliminary injunction and
temporary restraining order against defendants, because it determined that
plaintiffs were unlikely to prevail on the issue of whether the characters were
substantially similar. The district court also indicated that defendant’s
character was a parody that was protected by the fair use doctrine.

Issue

Whether defendant’s superhero character infringed plaintiffs’ copyright in the
Superman character.

Holding

The court found that the Superman and Ralph Hinkley characters were not
substantially similar, therefore defendant’s character did not infringe
plaintiffs’ copyrights. Although the fair use defense was not raised or at
issue, the court briefly addressed the district court’s discussion of the parody
defense. The court noted that while the defense may be available in “isolated
instances” where a nearly identical script or an express reference to a
character is made, it questioned “whether the defense could be used to shield
an entire work that is substantially similar to one in competition with the
copyrighted work.”

Note: See also Warner Bros., Inc. v. ABC, Inc., 720 F.2d 231 (2d Cir. 1983).

Tags

Second Circuit; Film/Audiovisual; Parody/Satire; Painting/Drawing/Graphic

Outcome

Preliminary ruling, mixed result, or remand

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

