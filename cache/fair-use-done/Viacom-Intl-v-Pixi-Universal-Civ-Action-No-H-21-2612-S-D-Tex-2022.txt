Viacom Int’! y. Pixi Universal

Civ. Action No H-21-2612, 2022 U.S. Dist. LEXIS 57400 (S.D. Tex. Mar. 25, 2022)

Year

2022

Court

United States District Court for the Southern District of Texas

Key Facts

Plaintiff Viacom International Inc. (“Viacom”) is the owner of the SpongeBob
SquarePants (“SpongeBob”) entertainment franchise and holds over 400 copyright
registrations for SpongeBob works, including the animated television series, feature
films, two-dimensional drawings, and stylebooks featuring extensive artwork from
the franchise (the “Works”). The SpongeBob television series features a fictional
fast-food restaurant called the “Krusty Krab,” which Viacom once recreated as an
immersive experience at a comics convention in 2019. Defendant Pixi Universal,
LLC (“Pixi’”) operates themed “pop-up” restaurants and bars. In 2021, Pixi recreated
the Krusty Krab as a pop-up called “The Rusty Krab,” which featured recreations of
the fictional restaurant and other elements of the SpongeBob series. Pixi charged a
fee for admission and for food and drink. After demanding that Pixi cease using
SpongeBob intellectual property, Viacom brought copyright and trademark
infringement claims and sought injunctive relief. Pixi asserted fair use as its sole
defense to the copyright claim.

Issue

Whether using elements of copyrighted works to create a themed “pop-up” business
based on those works is fair use.

Holding

The court decided that the first factor, the purpose and character of the use, weighed
against fair use. The court found that Pixi’s use was wholly commercial and that it
did not transform the Works by merely changing the medium of expression. Instead,
Pixi’s use shared the same purpose as the original, that is, “light-hearted
entertainment.” Rejecting Pixi’s “post-hoc characterization” of its pop-up as a
parody, the court found that Pixi embraced, replicated, and intended to profit from
the Works. The second factor, the nature of the copyrighted work, also weighed
against fair use because of the “fictional, imaginative nature” of the Works. The
third factor, the amount and substantiality of the work used, likewise weighed
against fair use. The court determined that, although Pixi did not copy all the details
of the SpongeBob universe or even specific episode storylines, Pixi’s use of
“central, principal characters and iconic locals and backgrounds from the series”
was nonetheless “substantial” as it used the “heart” of the Works. Lastly, the court
determined that the fourth factor, the effect of the use on the potential market for or
value of the work, also weighed against fair use. The court found that the type of
immersive experience that Pixi created, which was based exclusively on Viacom’s
SpongeBob franchise, affected the potential market for Viacom to venture into
creating or licensing derivative immersive works. Because the factors together
weighed against a finding of fair use, the court concluded that Viacom was likely to
succeed on its copyright infringement claim and granted its motion for preliminary
injunctive relief.

Tags

Painting/Drawing/Graphic; Parody/Satire

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

