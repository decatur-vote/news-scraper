Stewart v. Abend,
495 U.S. 207 (1990)

Year

1990

Court

Supreme Court of the United States

Key
Facts

At issue in this case were the rights to the 1954 film Rear Window, starring
James Stewart, and the short story on which the film was based. In 1945, the
short story’s author, Cornell Woolrich, assigned film rights for the first
twenty-eight-year copyright term to a motion picture production company,
promising to renew the copyright and assignment for the second twenty-eight-
year term. But in 1968, Woolrich died before he could complete the renewal
or assignment. A trust subsequently renewed the copyright in 1969 and
assigned the renewal rights to plaintiff Sheldon Abend, a literary agent. In
1971, defendants Jimmy Stewart, Alfred Hitchcock, and MCA, Inc.,
successors in interest to Woolrich’s 1945 assignment of film rights, broadcast
the film on ABC’s television network. They ignored plaintiff’s notice to stop
infringing his renewal rights. Plaintiff sued defendants in 1974 and the case
settled for $25,000. Years later, defendants re-released the picture through a
variety of media, including movie theaters, cable TV, and film rentals and
plaintiff sued again.

While the district court held that defendants’ continued exploitation of the
film was a fair use, the Ninth Circuit reversed and defendants petitioned to the
U.S. Supreme Court.

Issue

Whether the unauthorized use of an original story to create a derivative
motion picture work was fair use.

Holding

The Court held that defendants’ exploitation of the film based on Woolrich’s
original story “presents a classic example of an unfair use: a commercial use
of a fictional story that adversely affects the story-owner’s adaptation rights.”
Defendants’ use did not fall into any of the categories enumerated in 17
U.S.C. §107 or meet the four statutory factors. The production company
made $12 million from the re-release of the motion picture, making the use
commercial. Further, because the original work was fictional as opposed to
factual, the use was less likely to be deemed fair. Considering the amount
and significance of the use, the Court determined that the Woolrich story
constituted a substantial portion of the motion picture, as the film expressly
used its unique setting, plot, characters, and sequence of events. Finally, the
Court found that the film’s re-release hindered the plaintiff’s ability to market
new versions of the original story.

Tags

U.S. Supreme Court; Film/Audiovisual; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gow/fair-
use/index.html.

