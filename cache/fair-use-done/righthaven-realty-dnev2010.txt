Righthaven, L.L.C. v. Realty One Grp., Inc.,
No. 2:10-cv-1036-LRH-PAL. (D. Nev. Oct. 19, 2010)

Year

2010

Court

United States District Court for the District of Nevada

Key
Facts

Defendant Michael Nelson, a licensed realtor, operated an internet blog on
home ownership in Las Vegas, Nevada. The Las Vegas Review-Journal
(LVRJ) published a newspaper article that featured a mix of commentary and
factual reporting about the Las Vegas housing market. After the article
appeared in LVRJ, defendant copied the first eight of thirty sentences from the
article and posted them on his blog. The portion defendant used on his blog
contained only factual reporting. After the quoted portion appeared on
defendant’s blog, LVRJ assigned the copyright to plaintiff Righthaven, LLC.

Issue

Whether defendant’s unauthorized use of eight factual sentences from a news
story constituted fair use.

Holding

The court held that defendant’s use was a fair use. It found that, while
educational, defendant’s use was primarily commercial, which weighed
against fair use. However, the court further found that the value of the
original work was primarily in the LVRJ’s commentary rather than the eight
sentences of fact-based reporting. As such, the court determined that
defendant’s use of only the factual aspects of the article was not likely to
impact the value of the original article. Ultimately, the court ruled that the
weighing of all four factors supported a finding of fair use.

Tags

Ninth Circuit; Internet/Digitization; News reporting; Textual work

Outcome

Fair use found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

