Twin Peaks Prods., Inc. v. Publ’ns Int’I, Ltd.,
996 F.2d 1366 (2d Cir. 1993)

Year

1993

Court

United States Court of Appeals for the Second Circuit

Key
Facts

Plaintiff Twin Peaks Productions, Inc. owned the television serial drama Twin
Peaks that was first televised in 1990. Defendant Publications International,
Ltd. published a book titled “Welcome to Twin Peaks: A Complete Guide to
Who’s Who and What’s What.” Defendant’s book, based on the first eight
episodes of the television series, contained content discussing various aspects
of the series, including the series’ characters, creators, producers, location,
music, and popularity. Beyond this, the book contained a chapter devoted to
the detailed recounting of the first eight episodes’ plots. Defendant appealed
the district court’s ruling that its book infringed plaintiff’s work.

Issue

Whether defendant’s use of a copyright protected television series to create a
book based on the series was fair use.

Holding

The court held that defendant’s unauthorized use of aspects of the television
series was not fair use. The court found that defendant’s detailed recounting
of the show’s plotlines went far beyond merely identifying their basic outline
for the transformative purposes of comment or criticism. The court found that
the plot synopses contained in defendant’s book were essentially
“abridgments,” which are protected as derivative works. Because the plot
synopses were so detailed, and in fact lifted many sections verbatim from the
original scripts, the court found that defendant copied a substantial amount of
plaintiff’s original works. The court also found that the defendant’s book
affected the market or potential market for plaintiff’s protected work because
a viewer may potentially use defendant’s detailed plot synopses as a substitute
for viewing particular episodes of the series. Finally, there was evidence of
an active derivative market for the original copyrighted work—plaintiff had
licensed at least two “Twin Peaks” books and had plans to license more—
demonstrating that the defendant’s book competes in plaintiff’s markets.

Tags

Second Circuit; Education/Scholarship/Research; Film/Audiovisual;
Review/Commentary; Textual work

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

