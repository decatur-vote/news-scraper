Nat'l Acad. of TV Arts & Scis., Inc. v. Multimedia Sys. Design, Inc.

No. 20-CV-7269 (VEC), 2021 U.S. Dist. LEXIS 142733 (S.D.N.Y. July 30, 2021)

Year

2021

Court

United States District Court for the Southern District of New York

Key Facts

Plaintiffs, the National Academy of Television Arts and Sciences, Inc. and the
Academy of Television Arts & Sciences (collectively, the “Television Academies”),
own the copyright for the design of a gold statuette of a winged figure holding an
atom (the “Emmy Statuette”), which they use to present Emmy Awards to
distinguished members in television programming. Defendant Multimedia System
Design, Inc. (“MSDI’), which creates and posts content providing social and political
commentary, posted a video to its YouTube channel as part of its “Crony Awards,”
which celebrated countries that have downplayed the COVID-19 pandemic. The
video—as well as its thumbnail and social media posts promoting it—used an image
of the Emmy Statuette holding a model of the COVID-19 virus instead of an atom (the
“Crony Graphic”). In response to a DMCA takedown notice, YouTube promptly took
down the video. MSDI submitted a counter-notice claiming the use of the statuette
was fair use. Plaintiffs filed suit asserting copyright infringement and other claims.
Defendant moved to dismiss the copyright claim, arguing the use was either de
minimis or fair use.

Issue

Whether the use of a modified version of a statuette used in an award show in
connection with a different award show that provides political commentary is fair use.

Holding

Considering the first factor, the purpose and character of the use, the court found that
the use was not transformative, which weighed against fair use. Although MSDI made
some alterations to the Emmy Statuette, the “dominant and essential aesthetic
elements” of the original design remained. The court further found that both the
Emmy Statuette and the Crony Graphic were used for the same purposes: to
“represent awards” and “promote . . . award shows.” The court also rejected MSDI’s
claim that the Crony Graphic was a parody because it did not direct any criticism
towards the Emmy Statuette or the Television Academies. The court found the use to
be commercial because, while MSDI did not derive direct income from the YouTube
video, it included links to portals where viewers could pay for the content. The second
factor, the nature of the copyrighted work, weighed against fair use because MSDI
conceded that the Emmy Statuette is “arguably creative.” The third factor, the amount
and substantiality of the portion used, disfavored fair use as well because the two
images were “identical other than the replacement of the atom with the COVID-19
virus,” and MSDI had not explained why it was necessary to use the Emmy Statuette
specifically. The court found the fourth factor, the effect of the use upon the potential
market for or value of the copyrighted work, also slightly disfavored fair use. While
the markets for the Emmy Awards and for MDSI’s video do not “meaningfully
overlap,” the court concluded that the Television Academies adequately alleged that
they suffered actual and reputational harm by having the Emmy Statuette associated
with misinformation about the COVID-19 pandemic. Balancing the factors, the court
held that the use was not fair use as a matter of law and denied the motion to dismiss.

Tags

Film/Audiovisual; Internet/Digitization; Parody/Satire; Photograph; Sculpture

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see https://www.copyright.gov/fair-use/.

