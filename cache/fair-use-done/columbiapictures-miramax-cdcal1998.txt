Columbia Pictures Indus., Inc. v. Miramax Films Corp.,
11 F. Supp. 2d 1179 (C.D. Cal. 1998)

Year

1998

Court

United States District Court for the Central District of California

Key
Facts

Defendant Miramax Films Corp. created an advertising poster and trailer for
the Michael Moore documentary The Big One that incorporated elements of
advertising previously used for the film Men In Black. Posters for both films
featured figures carrying a large object standing in front of a nighttime New
York skyline, and used similar colors and layout. The two films’ trailers had
“a virtually identical theme, format, pace, and sequence of events,” and used
the same music. Defendant admitted that the advertising campaign for The
Big One was intended to parody Men In Black. Plaintiff Columbia Pictures
Industries, Inc. filed for a preliminary injunction, alleging that defendant’s
poster and trailer infringed its copyright for Men In Black.

Issue

Whether defendant’s use of expressive elements from the Men In Black
advertising poster and trailers in an advertising poster and trailer for a
documentary constituted fair use.

Holding

The court granted the injunction, holding that defendant failed to establish a
likelihood of success on the merits of its fair use defense. It held that
defendant’s advertisements were not transformative because they “cannot
reasonably be perceived as commenting on or criticizing the ads for ‘Men In
Black’” because they merely incorporated elements of plaintiff’s
advertisements. The court further held that the amount and substantiality of
the portion used was not reasonable, finding that defendant’s proffered
justifications were unpersuasive given the non-transformative nature of the
use. Finally, the court held that defendant failed to meet its burden of
providing affirmative evidence relating to market harm.

Tags

Ninth Circuit; Film/Audiovisual; Painting/Drawing/Graphic; Parody/Satire

Outcome

Fair use not found

Source: U.S. Copyright Office Fair Use Index. For more information, see http://copyright.gov/fair-
use/index.html.

