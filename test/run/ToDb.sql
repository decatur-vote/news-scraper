
-- @query(create, -- END)
DROP TABLE IF EXISTS `article`;

CREATE TABLE `article` (
    `id` int PRIMARY KEY AUTO_INCREMENT,
    `title` VARCHAR(512),
    `date` DATETIME,
    `url` VARCHAR(512),
    `description` VARCHAR(2048),
    `photo_url` VARCHAR(512),

    `outlet` VARCHAR(256),
    `feed` VARCHAR(256)
);

-- END
