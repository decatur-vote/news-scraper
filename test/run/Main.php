<?php

namespace DecaturVote\NewsScraper\Test;

class Main extends \Tlf\Tester {

    /**
     * Add built-in sources to the scraper
     *
     * @param $scraper NewsScraper to add sources to
     */
    public function add_sources(\DecaturVote\NewsScraper $scraper){
        $source_dirs = [
            $this->file('source/v1/'),
            $this->file('source/v2/'),
        ];

        foreach ($source_dirs as $source_dir){

            $which = $this->cli->args['source_name'] ?? false;
            if ($which != false){
                $file = $source_dir.$which;
                if (!file_exists($file))continue;
                $json = file_get_contents($file);
                $source = json_decode($json, true);
                if ($source==null){
                    echo "Source $source not found";
                    continue;
                }
                $scraper->add_source($source, $which);
                $scraper->run();
                continue;
            }

            // add all json files in source/*[org_name]*/*.json
            foreach (scandir($source_dir) as $file){
                if ($file=='.'||$file=='..')continue;
                $dir = $source_dir.$file;
                if (!is_dir($dir))continue;
                foreach(scandir($dir) as $json_file){
                    if (substr($json_file,-5)!='.json')continue;
                    $json = file_get_contents($dir.'/'.$json_file);
                    $source = json_decode($json, true);
                    if ($source==null)continue;
                    $scraper->add_source($source, $file.'/'.$json_file);
                }
            }
        }
    }


    public function testChunckFairUseCaseSummmaries(){

        echo "\n\n\n\n testChunckFairUseCaseSummmaries is disabled. Edit the file & remove the return; line from it to download case summaries\n\n\n\n";

        return;

        $in_dir = $this->file('cache/fair-use-done/');
        $out_dir = $this->file('cache/fair-use-json/');


        $br = new \Breg();
        $regex = $br->parse(
        <<<REGEX
            /
                # Starts with case name
                (^\s*Year\s*$)
                |(^\s*Court\s*$)
                |(\n^\s*Key(\s|\n)Facts\s*$\n)
                |(\n^\s*Issues?\s*$\n)
                |(\n^\s*Holdings?\s*$\n)
                |(\n^\s*Tags\s*$\n)
                |(\n^\s*Outcome(\s*|\s\|)$\n)
                |(\n^\s*Source:\n)
            /m
        REGEX
        );

        //echo "\n\n".$regex."\n\n";

        $keys = [
            'Case',
            'Year',
            'Court',
            'Key Facts',
            'Issue',
            'Holding',
            'Tags',
            'Outcome',
            'Source',
        ];

        $out_file = $this->file('cache/index/fair-use-cases.json');
        $fh = fopen($out_file, 'w');
        fwrite($fh, "[\n");

        $failures = [];

        foreach (scandir($in_dir) as $file){
            if (substr($file,-4)!='.txt')continue;

            $text = file_get_contents($in_dir.$file);
            $text = preg_split("/(\n\n)?\n\n------------\n\n/", $text);
            //$text = explode("\n\n------------\n\n", $text);
            $text = implode("\n", $text);


            $parts = preg_split(
                $regex,
                $text
            );

            if (count($parts)!=9){

                print_r($parts);
                $failures[] = $file;

                echo "\n\nFix cache/fair-use-done/$file\n\n";
                //exit;

                continue;
                throw new \Exception("File '$file' is not 9 parts");
            }


            $info = array_combine($keys, $parts);
            $info = array_map('trim', $info);
            $info['file'] = $file;

            //fwrite($fh, json_encode($info, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES).",\n");
            fwrite($fh, json_encode($info, JSON_UNESCAPED_SLASHES).",\n");

        }

        fseek($fh,-2, SEEK_CUR);
        fwrite($fh,"\n]");

        fclose($fh);

        $failures = array_map(function($v){return 'cache/fair-use-done/'.$v;}, $failures);

        
        array_map(function($v){echo "\n$v";}, $failures);
        //print_r($failures);
    }

    
    public function testOCRFairUseCaseSummaries(){

        echo "\n\n\n\n testOCRFairUseCaseSummaries is disabled. Edit the file & remove the return; line from it to download case summaries\n\n\n\n";

        //////
        // WARNING - Some of the files were manually fixed to work with the chunking. Running this method will overwrite them, and mean more manual work is required (like 10 or 15 files)
        ///////

        return;


        ob_end_clean();

        $in_dir = $this->file('cache/fair-use-summaries/');
        $out_dir = $this->file('cache/fair-use-images/');
        $txt_dir = $this->file('cache/fair-use-texts/');
        $actual_text_out_dir = $this->file('cache/fair-use-done/');
        foreach (scandir($in_dir) as $file){
            if (substr($file,-4)!='.pdf')continue;
            $in_file = $in_dir.$file;
            $filename = substr($file,0,-4);

            $final_out_file = $actual_text_out_dir.$filename.'.txt';
            if (file_exists($final_out_file))continue;
            echo "\nConverting '".$file."' to .txt";

            $img_path = $out_dir.$filename.'/';

            if (!file_exists($img_path))mkdir($img_path);

            system("pdftoppm -png \"$in_file\" \"$img_path\"");

            $text = [];
            foreach (scandir($img_path) as $file){
                if (substr($file,-4)!='.png')continue;
                $in_file = $img_path.$file;
                $out_file = $txt_dir.$filename.'/'.$file.'.txt';
                if (!file_exists(dirname($out_file)))mkdir(dirname($out_file));
                system("tesseract \"$in_file\" \"$out_file\"");
            }

            $full_text = [];
            foreach (scandir($txt_dir.$filename) as $file){
                if (substr($file,-4)!='.txt')continue;
                $in_file = $txt_dir.$filename.'/'.$file;
                $full_text[] = file_get_contents($in_file);
            }

            $out_file = $actual_text_out_dir.$filename.'.txt';
            if (file_exists($out_file))continue;
            file_put_contents($out_file, implode("\n\n------------\n\n", $full_text));
        }

    }

    public function testRenameFairUseCases(){

        echo "\n\n\n\n testRenameFairUseCases is disabled. Edit the file & remove the return; line from it to download case summaries\n\n\n\n";

        return;

        $fair_use_index_file = $this->file('cache/index/fair-use-pages.json');
        $cases = json_decode(file_get_contents($fair_use_index_file), true);

        $download_dir = $this->file('cache/fair-use-pages/');
        $rename_dir = $this->file('cache/fair-use-summaries/');
        foreach ($cases as $file_name=>$case){
            $src_file = $download_dir.$file_name;       
            $target_name = basename($case['url']);
            $target_file = $rename_dir.$target_name;
            if (!file_exists($target_file)){
                copy($src_file, $target_file);
                echo "\ncopyto $target_file";
            }
        }
    }

    public function testDownloadFairUseCases(){

        echo "\n\n\n\n testDownloadFairUseCases is disabled. Edit the file & remove the return; line from it to download case summaries\n\n\n\n";

        return;
        $scraper = new \DecaturVote\NewsScraper();
        $this->add_sources($scraper);

        $articles = $scraper->get_articles_for_source('us-copyright/fair-use-index.json');

        $dir = dirname($scraper->get_cached_path('whatever','fair-use-pages'));
        if (!is_dir($dir))mkdir($dir);

        $index = [];

        ob_end_clean();
        foreach ($articles['articles'] as $article){
            $url = $article['url'];
            $cached_page_path = $scraper->get_cached_path($url,'fair-use-pages').'.pdf';
            $index[basename($cached_page_path)] = $article;
            if (file_exists($cached_page_path))continue;
            echo "\nDownload $url";
            $html = $scraper->download_page($url);
            file_put_contents($cached_page_path, $html);
            usleep(10000);
        }
        $dir = dirname($scraper->get_cached_path('fair-use-pages','index'));
        file_put_contents($dir.'/fair-use-pages.json', json_encode($index, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        print_r($articles);

    }

    /**
     *
     * @usage `phptest -test Main`
     * @usage `phptest -test Main -source_name herald-review/education.json`
     */
    public function testMain(){

        $scraper = new \DecaturVote\NewsScraper();
        $this->add_sources($scraper);


        $scraper->run();

    }
}
