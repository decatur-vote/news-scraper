<?php

namespace DecaturVote\NewsScraper\Test;

class ToDb extends \Tlf\Tester {

    public function insert_json_articles(string $file_path){
        $json = json_decode(file_get_contents($file_path), true);
        $version = (int)($json['source']['version'] ?? 1);
        if ($version != 1)return;
        if (isset($json['articles'][0]['author_name']))return;

        $outlet = $json['source']['outlet_name'] ?? 'unknown-outlet';
        $feed_name = $json['source']['feed_name'] ?? 'unknown-feed';

        echo "load $file_path\n";

        $c = json_decode(file_get_contents($this->file('config/secret.json')));
        $lildb = \Tlf\LilDb::new($c->user,$c->password,$c->db,$c->host);




        $articles = $json['articles'];
        do {
            //echo "Article\n";
            /** @arg \DecaturVote\NewsScraper\Article1 */
            $article = current($articles);
            $key = key($articles);
            $date = $article['date'] ?? '1970-01-01 00:00:00';
            echo "\n  Date: $date";
            $dt = \DateTime::createFromFormat('c', $date);
            if (is_bool($dt))$dt = \DateTime::createFromFormat('Y.m.d', $date);
            if (is_bool($dt) && preg_match("/[0-9]{1,2} [0-9]{4}/", $date)){
                $date = "01 ".$date;
                $dt = \DateTime::createFromFormat('n d Y', $date);
            }
            if (is_bool($dt)){
                if (strlen($date)==25){
                    $date = substr($date,0,-6);
                }
                $date = str_replace("T", " ", $date);
            } else {
                $date = $dt->format('Y-m-d H:i:s');
            }
            if (preg_match('/[a-zA-Z]/',$date))$date = '1970-01-01 00:00:00';
            if (preg_match('/[0-9]{4}.*[0-9]{4}/',$date))$date = '1970-01-01 00:00:00';
            $article['date'] = $date;
            $article['outlet'] = $outlet;
            $article['feed'] = $feed_name;
            $articles[$key] = $article;
        } while (next($articles));

        $lildb->insertAll('article', $articles);


    }

    public function testInsertInDb(){

        echo "\n\n";
        
        $dir = $this->file("cache/article");
        $org_dirs = scandir($dir);
        foreach ($org_dirs as $org_dir){
            if ($org_dir=='.'||$org_dir=='..')continue;
            $article_list_files = scandir($dir.'/'.$org_dir.'/');
            foreach ($article_list_files as $article_list_file){
                if (substr($article_list_file,-5)!='.json')continue;
                $this->insert_json_articles($dir.'/'.$org_dir.'/'.$article_list_file);
            }
        }


    }

}
