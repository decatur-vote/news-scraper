<?php

$root = dirname(__DIR__,4);

$pdo = $lia->pdo;

$searchDb = new \DecaturVote\SearchDb($pdo);

$searchDb->delete('search', ['type'=>'fair-use-case']);
$searchDb->delete('tag', ['tag_type'=>'case-outcome']);


$case_info = json_decode(file_get_contents("$root/cache/article/4203cf1fabe937df57805e47b93e5fe305a528ba.json"), true);
foreach ($case_info['articles'] as $article){
}

$cases = json_decode(file_get_contents($root.'/cache/index/fair-use-cases.json'), true);

$tags = [];
foreach ($cases as $case){
    $tags[$case['Outcome']] = $case['Outcome'];
}

$tag_objects = [];
foreach ($tags as $tag){
    $uuid = uniqid();
    $searchable = $searchDb->add_searchable($uuid, 'tag-reference', 'n/a', 'n/a', '#nothing');
    $tag_objects[$tag] = $searchDb->create_tag($uuid,$tag,'case-outcome', 'case outccome was '.$tag);
}

foreach ($cases as $case){
    
    $url = 
        "https://www.copyright.gov/fair-use/summaries/"
        . substr($case['file'],0,-4).'.pdf';

    //print_r($case);
    //$searchDb->add_searchable($uuid=uniqid('search'), 'test-item', 'Title 1 x', 'Summary v', $url);  
    $searchable = $searchDb->add_searchable(uniqid(), 'fair-use-case', $case['Case'], $case['Key Facts'], $url);
    $searchable->add_tag($tag_objects[$case['Outcome']]);

}

