<?php

$root = dirname(__DIR__,2);

require("$root/vendor/autoload.php");

$lia = new \Lia();


$lia->pdo = require(__DIR__.'/pdo.php');


// Add the built-in App, which provides all the web-server features.  
$server_app = new \Lia\Package\Server($lia, $fqn='lia:server');  // dir & base_url not required  
$lia->addResourceFile(__DIR__.'/site/style/main.css');
$lia->set('lia:server.resources.useCache', false);

// Add your app, providing home page & other core pages & features  
$site_app = new \Lia\Package\Server($lia, 'decaturvote:scraper', __DIR__.'/site/'); 

$integration = new \Dv\Integration\SearchIntegration($lia);  
$site_app = new \DecaturVote\Search\LiaisonPackage($lia, $integration);  
  
$lia->addResourceFile(\Tlf\Js\Autowire::filePath());  


$lia->deliver();
