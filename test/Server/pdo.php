<?php

$c = json_decode(file_get_contents(__DIR__.'/secret.json'), true);

$pdo = new \PDO("mysql:dbname=".$c['database'].';host='.$c['host'],$c['user'], $c['password']);

return $pdo;
