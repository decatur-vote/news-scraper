<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Available Scraper Sources   
Currently, there is no system for tracking externally-available scraper sources, and there likely are none. We will address this at a later time.  
  
**LEGAL DISCLAIMER:** I am not a lawyer. In some cases, scraping (and/or how you use the scraped data) may violate copyright law, contracts, or other law. YOU are responsible for following the law, and should probably consult a lawyer.     
  
## Also See  
- [Run the Scraper](/docs/use/RunScraper.md) - Download and scrape article indexes & page data  
- TODO [Add a news website feed]() - Configure a a search-results url and xpath queries to get article metadata.  
- TODO [Add a simple php parser]() - Parse a page PHP script  
- TODO [Add an article parser]() - Configure a host url, v1 feed name, and xpath queries to parse full articles.  
  
## Built-in Sources  
These sources are built-in to the current version of the scraper:  
```bash  
  decatur-tribune  
      city-beat -  Decatur Tribune - City Beat  
      here-and-there -  Decatur Tribune - Here & There  
      news -  Decatur Tribune - News  
  decatur-tribune-full  
      news -  655d2efbcb95f  
  decatur-vote  
      bits -  Decatur Vote - Tidbits  
      news -  Decatur Vote - News  
      sources -  Decatur Vote - Sources  
      topics -  Decatur Vote - Topics  
  decatur-vote-full  
      bits -  655d2efbcb966  
      news -  655d2efbcb968  
      sources -  655d2efbcb96b  
      topics -  655d2efbcb96d  
  herald-review  
      education -  Herald & Review - Education  
      local -  Herald & Review - Education  
  school-board  
      board-of-education -  DPS 61 School Board - Board of Education Meetings  
      facility-agendas -  DPS 61 School Board - Facility Committee Agendas  
      facility-minutes -  DPS 61 School Board - Facility Committee Minutes  
      finance-agendas -  DPS 61 School Board - Finance Committee Agendas  
      finance-minutes -  DPS 61 School Board - Finance Committee Minutes  
      financial-information -  ERROR json failed to load  
      mpsed-financial-information -  ERROR json failed to load  
      policy-agendas -  DPS 61 School Board - Policy Committee Agendas  
  us-copyright  
      fair-use-index -  U.S. Copyright Office - Fair Use Index  
  wand  
      decatur -  WAND TV - Decatur  
```  
