<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Run Decatur Vote's News Scraper  
The News Scraper is easily run via CLI.   
  
## Also See  
- [Find Sources](/docs/use/FindSources.md) - Find pre-existing sources to scrape.  
- TODO [Add a news website feed]() - Configure a a search-results url and xpath queries to get article metadata.  
- TODO [Add a simple php parser]() - Parse a page PHP script  
- TODO [Add an article parser]() - Configure a host url, v1 feed name, and xpath queries to parse full articles.  
- TODO [Scrape in PHP]() - Setup and run the Scraper with PHP instead of cli.  
  
## Using the CLI  
First git clone & `cd` into the root dir of this repo.  
  
`bin/scrape help`  
```bash  
    help - show this help menu  
    main - no-args: Show help menu; w/ args: run scrape  
    list-sources - List available sources  
    scrape - `scrape [org-name]/[source-name] [-page_depth int] [-print_results true]` Scrape a single source. `-` configs are optional  
```  
  
## Example  
`bin/scrape decatur-vote/news`:  
```bash  
...  
SLEEP for 1ms  
  
   --download_fresh_copy--    
Page 2 processed  
  Original Article Count: 125  
  Total Article Count: 125  
  Total Articles Parsed: 90  
  Total NEW Articles: 0  
  Articles Parsed this page: 30  
  New Articles this page: 0  
```  
  
  
