<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# ChangeLog  
  
## Other Notes  
- (none yet)  
  
## Git Log  
`git log` on Tue Nov 21 04:28:11 PM CST 2023`  
- 7ab7e04  (HEAD -> v0.1, origin/v0.1) minor language changes. run scrawl. [2 minutes ago]  
- 12a4631  progress on documentation & some minor cleanup [18 minutes ago]  
- aa3f817  some new docs & docs scaffolding [42 minutes ago]  
- c66c431  add docs url comment to scrawl.json [86 minutes ago]  
- 7e61c9f  run scrawl [2 hours ago]  
- 8d42fd6  composer update [2 hours ago]  
- 0b4e71f  move docsrc and config to non-hidden dirs. update scrawl settings to accomodate. [2 hours ago]  
- 0980fcd  add legal disclaimer to readme [2 hours ago]  
- a5e1036  notes [9 days ago]  
- e448096  500 pages of herald & review search results. notes. [9 days ago]  
- 170d5d9  scraped all the wand decatur search pages. 18000+ articles! [9 days ago]  
- 0bdf978  add herald-review/local, and scrape the search pages to a depth of 100. add decatur vote full soruces, tribune full sources, wand decatur scraping [9 days ago]  
- 85aa07f  web pages now download into subdirectories named after their host. renamedd v3 dv stuff. wrote bin/move-files [9 days ago]  
- 355b7b3  notes [10 days ago]  
- ebb684c  write v3 source for scraping individual article pages whose urls were found by the v1 parser. functional v3 parser & decatur-vote/full-articles.json v3 source is fully functional [10 days ago]  
- 552bb1e  notes [10 days ago]  
- 3ec7ff5  7201 herald & review education articles are parsed into json! (just metadata) [10 days ago]  
- 6ef947c  add 2550 herald & review education articles [10 days ago]  
- 09d6b8a  add all decatur vote news articles [10 days ago]  
- f9ba2d0  add all decatur tribune articles [10 days ago]  
- 61a1e7b  update h&r and tribune sources for pagination delays & bug fixes [10 days ago]  
- 52ca63f  Add pagination support & some quality of life stuff [10 days ago]  
- 5a2c1f6  robust pagination support w/ cli option to set pagination depth. add ParseResult to help & some methods to SourceINterface & ParserInterface. only Parse1/Source1 have been updated w/ pagination support. The Error & v2 both have just placeholders declining pagination. [10 days ago]  
- 7938113  - I added real date support. [11 days ago]  
- 9bdbe40  add cli that lists available sources & can scrape [11 days ago]  
- 7a5426a  remove cache/ from gitignore so cache/article will be updated [3 weeks ago]  
- d7ff115  add cache invalidation to aprser 1. take notes. gitignore page cache [3 weeks ago]  
- a147567  store source output using original rel_path, except inside cache/araticle dir [3 weeks ago]  
- c008e62  added another not a lawyer disclaimer [3 weeks ago]  
- 8acad72  notes on remaining legal cases (NOT A LAWYER) [3 weeks ago]  
- 8430f12  status notes [3 weeks ago]  
- 543fbba  disclaimer/notes [3 weeks ago]  
- 98f52e6  updated table of contents [3 weeks ago]  
- dfb5653  add fair use search server, plus update documentation [3 weeks ago]  
- d46792b  working on search setup [3 weeks ago]  
- 354047b  quick note [3 weeks ago]  
- b14dd8a  download all the cases from the us copyright office's fair use index [3 weeks ago]  
- 1238b7f  add a v2 parser that allows custom php parsing w/ minimal setup [3 weeks ago]  
- fe9dede  todo notes [3 weeks ago]  
- 498b67a  notes [3 weeks ago]  
- e3836aa  add data sources to readme [3 weeks ago]  
- 6c2c97a  some documentation improvements [3 weeks ago]  
- 6c450c6  sources are moved into a versioned dir. Documentation is somewaht thorough [3 weeks ago]  
- f15528c  gitignore vendor dir [3 weeks ago]  
- 02ef40e  major refactor. delete vendor dir. [3 weeks ago]  
- b9c227a  some minor bugfixes. urls without scheme/host will now have scheme/host added. added a herald & review source and several shcool board sources. still need to add finance sources for school board [4 weeks ago]  
- 58cdc25  bugfix for when photo url not found. add wand decatur source. notes & todos [4 weeks ago]  
- 00ee00a  refactored a little. added 2 more decatur trib sources [4 weeks ago]  
- 79d0a9b  Project Initialized from local [4 weeks ago]  
