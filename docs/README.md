<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# News Scraper  
Scrape web pages for lists of articles.  
  
Designed for the easiest possible setup of data sources. As a result, writing new Parsers is a bit complex.  
  
**LEGAL DISCLAIMER:** I am not a lawyer. In some cases, scraping (and/or how you use the scraped data) may violate copyright law, contracts, or other law. YOU are responsible for following the law, and should probably consult a lawyer.   
  
**UNDER DEVELOPMENT:** Project is in early stages & many things may change.  
  
## Documentation  
NOTICE: Documentation is under development. Some documentation is below.  
  
- Usage  
    - [Find Sources](/docs/use/FindSources.md) - Find pre-existing sources to scrape.  
    - [Run the Scraper](/docs/use/RunScraper.md) - Download and scrape article indexes & page data  
    - TODO [Add a news website feed]() - Configure a a search-results url and xpath queries to get article metadata.  
    - TODO [Add a simple php parser]() - Parse a page with a PHP script  
    - TODO [Add an article parser]() - Configure a host url, v1 feed name, and xpath queries to parse full articles.  
- Development  
    - TODO [Scrape in PHP]() - Setup and run the Scraper with PHP instead of cli.  
    - TODO [Create a Parser]() - Core parsing logic. Interfaces with Scraper.  
    - TODO [Create a Source]() - Interfaces with Parser. One instance represents one config file.   
    - TODO [Create an Article]() - Value object representing a parsed article or other item.  
- Other  
    - [Known Issues](/docs/KnownIssues.md)  
    - TODO [Fair Use Index]() - Searchable index of the U.S. Copyright Office's Fair Use Index.  
    - TODO [LICENSE]() - probably MIT license, possibly another free license.  
    - [Development Notes](/Status.md)   
    - [Changelog](/docs/Changelog.md)  
  
  
## Old Documentation  
The below documentation may not be accurate.  
  
- Install  
- NOTICE - Warnings & Stuff  
- Data Sources & Article Lists  
- Usage - Scrape Existing Sources  
- Usage - Create New Sources  
- Develop - Create New Parsers  
- Develop - Work with parsed article lists   
- Develop - Additional Notes  
- Fair Use Index  
    - Searchable Index  
    - Cases to Consider (I AM NOT A LAWYER. NOT LEGAL ADVICE.)  
  
## Install  
COMPOSER/PACKAGIST IS NOT SETUP YET  
```bash  
composer require decatur-vote/news-scraper v0.1.x-dev   
```  
or in your `composer.json`  
```json  
{"require":{ "decatur-vote/news-scraper": "v0.1.x-dev"}}  
```  
  
  
## NOTICE - Warnings & Stuff  
This project is in early stages, is under development, and details may change.  
- Pagination is not implemented  
- Cache invalidation is not implemented (i.e. pages will never be re-downloaded unless the cached copies are manually deleted)  
- Parser1 will have new features added to it  
- Current interfaces may change  
- Current Article output format may change, and migrations are not yet implemented.  
  
## Data Sources & Article Lists  
Several data sources are already implemented & have cached article lists stored in this repo.  
- Decatur Tribune: City Beat, Here & There, and News  
- Herald & Review: Education  
- WAND TV: Decatur News Articles  
- DPS61 School Board: Board Meetings, Finance Committee Agendas & Minutes, Facility Commitee Agendas & Minutes, Policy Commitee Agendas & Minutes, (Todo: financial information, mpsed financial information)  
  
## Usage - Scrape Existing Sources  
To run all sources within this repo, use `phptest -test Main`.   
To scrape a single source within this repo, use `phptest -test Main -source_name org-name/source-name.json`  
  
To scrape sources manually via php:  
```php  
<?php  
$scraper = new \DecaturVote\NewsScraper();  
$source = json_decode(file_get_contents($PATH_TO_EXISTING_SOURCE));  
$scraper->add_source($source); //you can add MANY sources  
$scraper->run();  
```  
  
## Usage - Create New Sources  
Source definitions depend upon the parser your source will use. You can write your own parser (*see below*).  
  
Within this repo, sources are at `source/v1/org-name/source-name.json`. You can add sources outside this repo too.  
  
### Parser1  
Parser 1 uses json files containing mostly xpath queries.  
  
Example source that works with Parser1:  
```json  
{  
    "outlet_name": "Decatur Tribune",  
    "feed_name": "City Beat",   
  
    "url": "https://www.decaturtribune.com/category/osborne-online/",  
  
    "initial_page_depth": 10,  
    "standard_page_depth": 2,  
    "max_poll_frequency": 360,  
  
    "article_selector": "//article[@itemtype=\"http://schema.org/BlogPosting\"]",  
  
    "title_selector": "//header/h2/a",  
    "url_selector": "//header/h2/a",  
    "description_selector": "//div[@itemprop=\"text\"]/p",  
    "photo_selector": "//div[@class=\"row\"]/div[1]/div/a/img[@itemprop=\"image\"]",  
    "old_next_page_selector":"//div[@class=\"fl-page\"]/div[@class=\"fl-page-content\"]/div[@itemtype=\"http://schema.org/Blog\"]/nav/div[@class=\"fl-archive-nav-next\"]/a",  
    "next_page_selector":"//nav/div[@class=\"fl-archive-nav-next\"]/a",  
  
    "date_selector": "//header/div/span[1]",  
    "date_format": "Y-m-d",  
      
    "pagination_delay_ms": 5000  
}  
```  
  
For details on each key/value see property docblocks in [Source1.php](src/Parser1/Source1.php).  
  
### Parser2  
Parser2 uses a json file config, plus a php file with custom parsing that returns an array of data in no specific format.   
  
Config file:  
```json  
{  
    "version": "2",  
    "outlet_name": "U.S. Copyright Office",  
    "feed_name": "Fair Use Index",  
    "url": "https://copyright.gov/fair-use/fair-index.html",  
    "parser": "us-copyright/fair-use-index.php"  
}  
```  
  
PHP Parser (*This can be anything you want it to be*):  
```php  
<?php  
/**  
 *  
 * Get all entries in the fair use index  
 *  
 * @param Source2 $source  
 * @param \Taeluf\PHTML $doc  
 *  
 * @return array<  
 */  
  
$keys = [];  
$tr_list = $doc->xpath('//table/thead/tr');  
foreach ($tr_list[0]->children as $td){  
    if ($td->nodeName!='th')continue;  
    $keys[] = property_exists($td,'innerText') ? $td->innerText : $td->textContent;  
}  
  
$tr_list = $doc->xpath('//table/tbody/tr');  
  
$data = [];  
foreach ($tr_list as $tr_node){  
    $tr = new \Taeluf\PHTML(''.$tr_node);  
    $row = [];  
    $tds = $tr->xpath('//td');  
    $row['url'] = 'https://www.copyright.gov'.$tr->xpath('//td/a')[0]->href;  
    foreach ($tds as $index => $td){  
        $key = $keys[$index];  
        $row[$key] = property_exists($td,'innerText') ? $td->innerText : $td->textContent;  
    }  
    $data[$row['url']] = new \DecaturVote\NewsScraper\Article2($row);  
}  
  
return $data;  
```  
  
## Develop - Create New Parsers  
To develop a new parser, you must:  
1. Modify `NewsScraper::source_version_map`   
2. Extend from `DecaturVote\NewsScraper\SourceInterface` to represent the source config as php  
3. Extend from `DecaturVote\NewsScraper\ParserInterface` to actually parse html into article lists  
4. Extend from `DecaturVote\NewsScraper\ArticleInterface` to represent the article information  
  
Note: You CAN create a new source and use an existing Parser &/or Article, if you just want a different source definition.   
  
## Develop - Work with parsed article lists  
The NewsScraper simply stores article lists on disk as json, and you can load these articles lists & do with them as you please.  
  
Articles are stored at `cache/article/[sha1_hash].json`, where the sha1 hash is representative of the source that creates the article list.  
  
The articles are stored in the folowing format: (*Note: This format is subject to change*)  
```json  
{  
    "source": {  
        "outlet_name": "DPS 61 School Board",  
        "feed_name": "Finance Committee Agendas",  
        "url": "https://www.dps61.org/Page/69",  
        "initial_page_depth": 10,  
        "standard_page_depth": 2,  
        "max_poll_frequency": 3600,  
        "article_selector": "//div[@id=\"content-accordion-panel-173-205\"]/ul/li",  
        "title_selector": "//li/a",  
        "url_selector": "//li/a",  
        "description_selector": "//li/a",  
        "photo_selector": null,  
        "date_selector": "//li/a",  
        "next_page_selector": null  
    },  
    "articles": {  
        "https://www.dps61.org/cms/lib/IL50010903/Centricity/Domain/30/Agenda%2008-01-2023.pdf": {  
            "title": "Finance Committee Agenda - August 1, 2023",  
            "date": "",  
            "url": "https://www.dps61.org/cms/lib/IL50010903/Centricity/Domain/30/Agenda%2008-01-2023.pdf",  
            "description": "Finance Committee Agenda - August 1, 2023",  
            "photo_url": null  
        },  
        "https://www.dps61.org/cms/lib/IL50010903/Centricity/Domain/30/Agenda%2009-05-2023.pdf": {  
            "title": "Finance Committee Agenda - September 5, 2023",  
            "date": "",  
            "url": "https://www.dps61.org/cms/lib/IL50010903/Centricity/Domain/30/Agenda%2009-05-2023.pdf",  
            "description": "Finance Committee Agenda - September 5, 2023",  
            "photo_url": null  
        }  
    }  
}  
```  
  
Notice the 'source' key is typically the exact same as the original source definition. The 'articles' key contains an array of articles, where each article's key is the url of the article.  
  
## Develop - Additional Notes  
- `cache/article/` contains article lists  
- `cache/page/` contains cached copies of html pages  
- `source/v1/` contains v1 source definitions  
- `src/` contains the actual code  
- `test/run/Main.php` will run all the built-in sources.  
  
## Fair Use Index  
I created a searchable database of the fair use index. The only searchable part is the case name and the Key Facts. Other portions are not in the search database, but you can grep in the `cache/fair-use-done/` directory for a FULL text search.  
  
NOTICE: There is no guarantee that this db is comprehensive. The original source is at https://www.copyright.gov/fair-use/fair-index.html ... the searchable content is built from running OCR software on PDF files, then additional automated processing, so text may contain errors.  
  
To use this index  
1. `git clone https://gitlab.com/decatur-vote/news-scraper.git` and `cd news-scraper`  
1. `composer install`  
2. Create `test/Server/secret.json` with keys 'database', 'host', 'user', 'password' (for a mysql connection)  
3. Run vendor/bin/dv-search create-db -pdo test/Server/pdo.php  
4. `php -S localhost:3000 test/Server/deliver.php`  
5. Visit http://localhost:3000/  
6. Click the 'Build Fair Use Index' link  
7. Visit the 'Search Page' link & do your search.  
  
To search the fair use index:   
run `phptest server`  
  
### Legal Cases to Consider  
I am NOT a lawyer. THIS IS NOT LEGAL ADVICE. I AM NOT LIABLE FOR YOUR USE OF THIS INFORMATION. CONSULT AN ATTORNEY.  
  
I downloaded the entirety of the Fair Use Index on Oct 28, 2023 to help me determine what scraping is legal & what kind of search functions might be legal. Some cases I've looked that seem relevant are below.  
  
I am NOT a lawyer. THIS IS NOT LEGAL ADVICE. I AM NOT LIABLE FOR YOUR USE OF THIS INFORMATION. CONSULT AN ATTORNEY.  
  
The summary notes should also be taken with caution.  
  
Also the .txt files are generated via OCR software and may contain errors.  
  
Easiest way to read these is to `cd cache/fair-use-done/` then `cat [file_name]` (put quotes around the MidlevelU filename)  
  
NOT A LAWYER. NOT LEGAL ADVICE  
  
- MidlevelU_Inc.%20v.%20ACI%20Info.%20Grp._989%20F.3d%201205%20(11th%20Cir.%202021).txt - declines fair use, in part bc full copies of articles were made available. But it also served a search function, and the business being competitve to the business being scraped was also a factor.   
- ap-meltwater-sdny2013.txt - declines search function being fair use  
- authorsguild-google-2dcir2015.txt - Appears strongly in favor of search function being fair use, but its based on books  
- authorsguild-hathitrust-2dcir2014.txt - A win for search, but its based on books  
- fox-news-network-tveyes-02272018.txt - TVEye's scanned videos to make them searchable, archivable, watchable, and shareable. Search was fair use. Share & watch were NOT.   
- katz-google-2015.txt - Using an embarassing photo in commentary blog content & google image search results. Fair Use Found.  
- kelly-arriba-9thcir2003.txt - thumbnail copies of images in search results. Fair Use Found  
- perfect10-amazon-9thcir2007.txt - Google showing thumbnail copies of nude images in search results. Fair use likely found, but ruling is preliminary.  
- philpot-wosinc-wdtexas2019.txt - A small media company used creative commons photos in articles on their website. Preliminary ruling, Fair Use not found, because work was not transformative, among other reasons.  
- vhtinc-zillowgroup-9thcir2019.txt - Zillow used professional real-estate photos on its website in the 'Digs' section about Home Improvement. Fair Use Not Found. Even the Searchability feature was found Not Fair Use, bc Zillow's use differed from that of a typical search engine. "In contrast to internet-wide search engines that direct to the original source of photos or use thumbnail images, Zillow only included photographs from its own database and used full-size versions of the photographs for the same purpose as the originals, that is, “to artfully depict rooms and properties."   
- white-west-draft-no12cv1340-sdny2014.txt - Copywritten legal briefs, publicly filed, scraped from PACER, and used in a text-searchable database. Fair Use Found.  
  
In my cursory search for "search" related cases, I found no other relevant cases. There could be other relevant cases.  
