<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ParserArticles/SourceArticles.php  
  
# class DecaturVote\NewsScraper\SourceArticles  
  
See source code at [/src/ParserArticles/SourceArticles.php](/src/ParserArticles/SourceArticles.php)  
  
## Constants  
  
## Properties  
- `public string $name;` string org-name/source-name  
- `public array $feed_names;` array<int index, string org-name/feed-name  
- `public int $max_poll_frequency;` How frequently the $url should be re-downloaded, in minutes  
- `public string $description_selector;` xpath selector of a meta node whose content attribute is a summary/description  
- `public string $article_selector;` xpath selector that returns an array of article nodes, containing article information.  
All other selectors are executed from this node, NOT from the page source. (next_page_selector is from page source though)  
- `public string $title_selector;` xpath selector that returns a node containing a title  
- `public string $url_selector;` xpath selector that returns an <a> tag where the href is the url of the article  
- `public string $photo_selector;` xpath selector where img.src is the url of an image  
- `public string $created_date_selector;` xpath selector where <time datetime="ISO 8601 time">  
- `public string $updated_date_selector;` xpath selector where <time datetime="ISO 8601 time">  
- `public string $body_selector;` inner text is the body  
- `public string $author_selector;` inner text is the author_name  
- `public string $author_url_selector;` href is author_url  
- `public string $host_url;` url of host, like https://www.decaturvote.com  
- `public int $delay_ms_between_downloads = 100;` Number of milliseconds to wait before downloading the next paginated page. Default is 100ms  
  
## Methods   
- `public function set_data(array $source_data)`   
- `public function get_data(): array`   
- `public function get_parser(): ParserArticles`   
- `public function get_unique_name(): string` Get a string name that should be unique across all sources of the given version  
- `public function get_unique_human_name(): string` Get a human friendly string name that should be unique across all sources of the given version  
- `public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool`   
- `public function set_runtime_options(array $args)` Set runtime options, such as from cli arguments  
  
  
