<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ParserArticles/ParserArticles.php  
  
# class DecaturVote\NewsScraper\ParserArticles  
  
See source code at [/src/ParserArticles/ParserArticles.php](/src/ParserArticles/ParserArticles.php)  
  
## Constants  
  
## Properties  
- `public $fh;` File handle, from fopen  
- `public int $articles_written = 0;`   
  
## Methods   
- `public function __construct(protected SourceArticles $source)`   
- `public function __destruct()`   
- `public function goto_next_page(string $html): bool`   
- `public function get_page_urls(): array`   
- `public function should_download_file(string $url, int $filemtime, int $current_time): bool`   
- `protected function clean_text(string $text_to_clean): string` Return a clean version of the text with certain special characters removed or replaced.  
  
- `public function get_articles(string $html, string $page_url): array`   
- `public function delay_before_downloading()` sleep for ms defined on sources as 'pagination_delay_ms'.  
  
