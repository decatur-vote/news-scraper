<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ParserArticles/ArticleArticles.php  
  
# class DecaturVote\NewsScraper\ArticleArticles  
  
See source code at [/src/ParserArticles/ArticleArticles.php](/src/ParserArticles/ArticleArticles.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct(protected array $data)`   
- `public function get_unique_name(): string` Get a unique name for an article  
- `public function get_data(): array` Data varies by source  
  
  
