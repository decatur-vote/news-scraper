<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Cli.php  
  
# class DecaturVote\NewsScraper\Cli  
  
See source code at [/src/Cli.php](/src/Cli.php)  
  
## Constants  
  
## Properties  
- `protected \DecaturVote\NewsScraper $scraper;`   
- `protected array $config = [];`   
- `protected array $commands = [  
            'list-sources' => "List available sources",  
            'scrape' => "`scrape [org-name]/[source-name] [-page_depth int] [-print_results true]` Scrape a single source. `-` configs are optional",  
        ];`   
- `protected \PDO $pdo;`   
  
## Methods   
- `public function __construct()`   
- `public function init()`   
- `public function cmd_scrape(array $args, array $named_args)` Query via stored SQL statement and print results.  
  
- `public function cmd_list_sources()`   
- `public function getPdo(): \PDO` Get a PDO instance from stored config. Does not load config or perform any checks.  
  
- `public function getBigDb(): \Tlf\BigDb` Get a BigDb instance from stored config. Does not load config or perform any checks.  
  
- `public function call(\DecaturVote\NewsScraper\Cli $cli, array $args)` Call a cli command  
- `public function setup(\DecaturVote\NewsScraper\Cli $cli, array $args)` Setup config / database credentials  
  
  
