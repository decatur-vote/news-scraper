<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/NewsScraper.php  
  
# class DecaturVote\NewsScraper  
  
See source code at [/src/NewsScraper.php](/src/NewsScraper.php)  
  
## Constants  
  
## Properties  
- `protected array $sources = [];` array<string rel_file_path, SourceInterface $source>  
- `protected array $source_version_map = [  
        'error'=>'DecaturVote\\NewsScraper\\SourceError',  
        '1'=>'DecaturVote\\NewsScraper\\Source1',  
        '2'=>'DecaturVote\\NewsScraper\\Source2',  
        '3' => 'DecaturVote\\NewsScraper\\SourceArticles',  
    ];` array<string version_name, string php_class_name> map of source versions to class names  
- `public array $runtime_options = [];` Array of runtime options to pass to sources to do with as they please.  
  
## Methods   
- `public function getSources(): array`   
- `public function add_source_directory(string $absolute_path)` Add all sources within a directory.  
Directory must contain sub-dirs like 'us-copyright-office' that contain files like 'fair-use-index.json'  
Format 'org-name/source-name.json'  
- `public function add_source(array $source_data, string $source_name)`   
- `public function get_articles_for_source(string $source_name): array`   
- `public function get_cached_path(string $unique_data, string $cache_dir = 'unsorted'): string`   
- `public function get_cached_page_path(string $url): string`   
- `public function get_cached_articles_path(NewsScraper\SourceInterface $source): string` Get path to cached articles list  
  
- `public function get_stored_articles(NewsScraper\SourceInterface $source): array`   
- `public function download_page(string $page_url, int $milliseconds_sleep=500): string` Download a page url & return the result  
  
- `public function run(array $sources_to_run = []): array`   
- `public function run_as_generator(array $sources_to_run = [])`   
  
