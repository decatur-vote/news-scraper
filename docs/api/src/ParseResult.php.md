<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ParseResult.php  
  
# class DecaturVote\NewsScraper\ParseResult  
Gives details about a parsing event, like how many new articles there are.  
Same ParseResult object is updated on each pagination loop  
New ParseResult is instantiated for each source being parsed.  
See source code at [/src/ParseResult.php](/src/ParseResult.php)  
  
## Constants  
  
## Properties  
- `public int $pagination_index = -1;` Which page is being / was just processed. 0 indicates initial page.  
- `public int $original_articles_count = 0;` Total number of articles already cached before running the parser.  
- `public int $total_article_count = 0;` Total number of articles for the given source, including previously stored articles  
- `public int $total_articles_parsed_count = 0;` Total number of articles parsed, NOT including previously stored articles  
- `public int $total_new_article_count = 0;` Total number of NEW articles found, across all pages.  
- `public int $num_articles_parsed = 0;` Number of articles found on the current page  
- `public int $num_new_articles_this_page = 0;` Number of articles found on the current page and not previously stored  
  
## Methods   
- `public function debug_msg(): string` Get a debug message  
  
