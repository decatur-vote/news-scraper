<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Parser1/Source1.php  
  
# class DecaturVote\NewsScraper\Source1  
  
See source code at [/src/Parser1/Source1.php](/src/Parser1/Source1.php)  
  
## Constants  
  
## Properties  
- `public string $outlet_name;`   
- `public string $feed_name;`   
- `public string $url;` Page to scrape for a list of articles  
- `public int $initial_page_depth;` On the first-scrape, how many 'next' pages should be scraped  
- `public int $standard_page_depth;` On regular re-scrapes, how many 'next' pages should be scraped  
- `public int $max_poll_frequency;` How frequently the $url should be re-downloaded, in minutes  
- `public string $article_selector;` xpath selector that returns an array of article nodes, containing article information.  
All other selectors are executed from this node, NOT from the page source. (next_page_selector is from page source though)  
- `public string $title_selector;` xpath selector that returns a node containing a title  
- `public string $url_selector;` xpath selector that returns an <a> tag where the href is the url of the article  
- `public string $description_selector;` xpath selector where innerText is the description of the article  
- `public string $photo_selector;` xpath selector where img.src is the url of an image  
- `public string $date_selector;` xpath selector where ... idk actually  
- `public string $date_type = 'datetime';` inner_text, or datetime  
- `public string $date_pattern;` a regex pattern where the first parentheses match is the datetime string  
- `public string $date_format;` (optional) if the datetime string is not your desired format, this should be the format to give to DateTime::createFromFormat() for the input date, then output an ISO 8601 date  
- `public string $next_page_selector;` xpath selector that returns an <a> node where the href points to the next page, listing articles  
- `public int $pagination_delay_ms = 100;` Number of milliseconds to wait before downloading the next paginated page. Default is 100ms  
  
## Methods   
- `public function set_data(array $source_data)`   
- `public function get_data(): array`   
- `public function get_parser(): Parser1`   
- `public function get_unique_name(): string` Get a string name that should be unique across all sources of the given version  
- `public function get_unique_human_name(): string` Get a human friendly string name that should be unique across all sources of the given version  
- `public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool`   
- `public function set_runtime_options(array $args)`   
  
