<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Parser1/Article1.php  
  
# class DecaturVote\NewsScraper\Article1  
  
See source code at [/src/Parser1/Article1.php](/src/Parser1/Article1.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct(protected array $data)`   
- `public function get_unique_name(): string` Get a unique name for an article  
- `public function get_data(): array` Get array of article data  
  
  
