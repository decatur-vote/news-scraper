<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Parser1/Parser1.php  
  
# class DecaturVote\NewsScraper\Parser1  
  
See source code at [/src/Parser1/Parser1.php](/src/Parser1/Parser1.php)  
  
## Constants  
  
## Properties  
- `protected string $active_url;`   
- `protected bool $is_paginating = false;` Indicate that we are NOT working on the first page.  
  
## Methods   
- `public function __construct(protected Source1 $source)`   
- `public function goto_next_page(string $html): bool`   
- `public function get_page_urls(): array`   
- `public function should_download_file(string $url, int $filemtime, int $current_time): bool`   
- `public function delay_before_downloading()` sleep for ms defined on sources as 'pagination_delay_ms'.  
- `protected function clean_text(string $text_to_clean): string` Return a clean version of the text with certain special characters removed or replaced.  
  
- `public function get_next_page_url(string $html): string`   
- `public function get_articles(string $html, string $page_url): array`   
  
