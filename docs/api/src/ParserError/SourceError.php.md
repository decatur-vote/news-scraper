<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ParserError/SourceError.php  
  
# class DecaturVote\NewsScraper\SourceError  
  
See source code at [/src/ParserError/SourceError.php](/src/ParserError/SourceError.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function set_data(array $source_data)`   
- `public function get_data(): array`   
- `public function get_parser(): ParserError`   
- `public function get_unique_name(): string` Get a string name that should be unique across all sources of the given version  
- `public function get_unique_human_name(): string` Get a human friendly string name that should be unique across all sources of the given version  
- `public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool`   
- `public function set_runtime_options(array $args)` Set runtime options, such as from cli arguments  
  
  
