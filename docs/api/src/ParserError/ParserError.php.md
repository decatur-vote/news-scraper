<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/ParserError/ParserError.php  
  
# class DecaturVote\NewsScraper\ParserError  
  
See source code at [/src/ParserError/ParserError.php](/src/ParserError/ParserError.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function goto_next_page(string $html): bool`   
- `public function get_page_urls(): array` error  
  
- `public function should_download_file(string $url, int $filemtime, int $current_time): bool` error  
  
- `public function get_articles(string $html, string $page_url): array` error  
  
- `public function delay_before_downloading()` sleep for ms defined on sources as 'pagination_delay_ms'.  
  
