<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Parser2/Article2.php  
  
# class DecaturVote\NewsScraper\Article2  
  
See source code at [/src/Parser2/Article2.php](/src/Parser2/Article2.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct(protected array $data)`   
- `public function get_unique_name(): string` Get a unique name for an article  
- `public function get_data(): array` Data varies by source  
  
  
