<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Parser2/Parser2.php  
  
# class DecaturVote\NewsScraper\Parser2  
  
See source code at [/src/Parser2/Parser2.php](/src/Parser2/Parser2.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct(protected Source2 $source)`   
- `public function goto_next_page(string $html): bool`   
- `public function get_page_urls(): array`   
- `public function should_download_file(string $url, int $filemtime, int $current_time): bool`   
- `protected function clean_text(string $text_to_clean): string` Return a clean version of the text with certain special characters removed or replaced.  
  
- `public function get_articles(string $html, string $page_url): array`   
- `public function delay_before_downloading()` sleep for ms defined on sources as 'pagination_delay_ms'.  
  
