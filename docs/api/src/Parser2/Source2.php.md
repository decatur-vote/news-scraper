<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Parser2/Source2.php  
  
# class DecaturVote\NewsScraper\Source2  
  
See source code at [/src/Parser2/Source2.php](/src/Parser2/Source2.php)  
  
## Constants  
  
## Properties  
- `public string $outlet_name;`   
- `public string $feed_name;`   
- `public string $parser;` php file inside the source/v2/ dir that will return an array of Article2 objects  
- `public string $url;` Page to scrape for a list of articles  
  
## Methods   
- `public function set_data(array $source_data)`   
- `public function get_data(): array`   
- `public function get_parser(): Parser2`   
- `public function get_unique_name(): string` Get a string name that should be unique across all sources of the given version  
- `public function get_unique_human_name(): string` Get a human friendly string name that should be unique across all sources of the given version  
- `public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool`   
- `public function set_runtime_options(array $args)` Set runtime options, such as from cli arguments  
  
  
