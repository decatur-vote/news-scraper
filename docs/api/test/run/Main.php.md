<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/Main.php  
  
# class DecaturVote\NewsScraper\Test\Main  
  
See source code at [/test/run/Main.php](/test/run/Main.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function add_sources(\DecaturVote\NewsScraper $scraper)` Add built-in sources to the scraper  
  
- `public function testChunckFairUseCaseSummmaries()`   
- `public function testOCRFairUseCaseSummaries()`   
- `public function testRenameFairUseCases()`   
- `public function testDownloadFairUseCases()`   
- `public function testMain()`   
  
