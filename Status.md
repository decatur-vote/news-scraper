# Status & Development Notes

## Dec 7, 2023
I want to... figure out what to do with the output. I know I have the h&R problem to fix still, and probably other things. Idc.

I want to handle the output files. How?

It probably depends on the source. Let's think about news article lists from v1. Let's say I download metadata for 300 tribune articles. It's the first download of them. What do I do?
Well, what problem am I solving?
I want feeds of articles from various local sources, so ... I put them in a database.

How do I prevent duplicates? Hold on, let me put them in a database first.

...

Okay. See test/run/ToDb.php & ToDb.sql

I have it working to insert all of my version 1 articles into the database. There's no uniqueness or anything or only inserting new rows. That'll be a later issue. 

## Nov 12, 2023
I made a private repo for storing cached copies of pages. I downloaded all tribune/news.json articles. I downloaded all wand/decatur.json search pages. I downloaded about 60,000 search results from herald-review's local ... but ran out of memory so we crashed. I re-ran with a page-depth of 500, so herald-review has about 50,000 articles indexed. I'll have to solve the memory problem. I scraped all the h&r education search results (about 17,000 articles i think)

When I finish the h&r local scraping, I'll need to make sure I'm not re-downloading any search pages. Currently the max poll frequency is 6 hours, which is fine for getting new articles, but I don't need to re-download 600 or so search result pages that I've already downloaded. 

I added new v3/ sources for trib & dv. added new v1 hr local.json source.

I made a few minor updates to the scraper, i think & to the articles parser. idr what.

I need to move the sources & articles into their own repo, so that the scraper can be a standalone piece of software without baked-in sources. It's a bit of a ways off from being a nice, robust, standalone foss project though, probably.

## Nov 11, 2023 LATER in the day
I added a v3 parser which will download every article from a v1 source, then parse them w/ the body. There is a standard output to cache/article with the body removed, and a private output to cache/full-articles/* that is gitignored ... we can't save full copies of people's articles, but we should be okay to keep them privately for Fair Use purposes (building a search index).

I wrote decatur-vote/full-articles.json to scrape all the news.json articles from the site.

The v3 output doesn't have article urls ... need to fix that. And I need to be able to halt processing after a certain number of articles, so that I don't have to download EVERY article in a single sitting.

And I need to check on the poll frequency stuff. Most articles probably shouldn't really ever be downloaded more than once. idk.

## Nov 11, 2023
I added:
- robust pagination support
    - only Parse1/Source1 have been updated w/ pagination support. The Error & v2 both have just placeholders declining pagination.
- ParserResult & debug messaging
- cli can accept `-page_depth [int]`
- error reporting when description not found
- updated some sources
- Downloaded metatdata for all herald & review education articles & for all decatur tribune articles. 

## Nov 10, 2023 (and a lil bit nov 11 2023 after midnight)
- I added real date support. 
- I added a cli `bin/scraper`. It can list built-in sources & can scrape them one at a time.
- Now using `curl` for file downloads bc I was getting 429-too many requests error. That seems to be working so far. I copy+pasted headers from firefox after clearing the cookies in firefox.
- Added `bin/scraper scrape org-name/*` support.
- added date support to board of education scraper 
    - `gp "date\": null"` to find failing dates. there are two in board-of-education.json, but the title is failing on those too ... idk. I need some kind of error detecting/reporting/handling.

Next time:
- pagination
- post-processing articles (like, download every article & save them into a database for search, or just save the articles list itself in a database. or both)

## Nov 1, 2023
- Added cache invalidation to Parser1.
- Article output now goes to cache/article/org-name/file.json, similar to sources.

See Oct 28 notes for TODOs

## Oct 29, 2023
I made a functional search index as a test server for the fair use index. I documented it in the readme.

## Oct 28, 2023 late night
I downloaded all the cases from the fair use index and OCRd them, then grepped through them (for lowercase "search"), read maybe 3 of them, and wrote down the others that I though would be worth reading. This is all done in the Main.php test class file. And there's a bunch of stuff in the cache/ dir for fair use downloads. I saved the json index & the text copies to git.

IF I WANT TO, I can clean up all the fair use code & make it easier to use, so that maybe, at some point, I can very easily download new cases.

Fair use index is at https://www.copyright.gov/fair-use/fair-index.html

## Oct 28, 2023
- MAJOR refactor, with versioned sources/parsers/article classes, based on interfaces.
- Pretty thorough documentation written.
- Scrawl is setup
- Reviewed all notes from Oct 27, 2023 (don't need to look at them ever again)

TODO (Scraper/in general):
- Add CLI
- Implement Pagination
- Setup directory-based source loading (currently it's in the test class)
- MAYBE Create index file describing what cached files exist
- add properties to Article1

TODO (parser1):
- when img.src is a data url, use srcset
- parse datetime strings
- MAYBE allow callback-configuration to edit values with php 
- DONE cache expiration

TODO (parser2):
- cache invalidation

TODO (extra features / new repos):
- Create database tie-in
- Create GUI
- Create Search Integration
- Monitor for new articles
- Create RSS Feed(s)
- Download actual article pages in order to create a full search database. The downloaded pages can NOT be accessible for reading, so they can NOT be cached in an open-source repo. (bc copyright law & stuff)

TODO (Sources):
- Add DecaturVote.com pages
- Handled school-board/financial-information & mpsed financial information. (probably make a 'budgets' source AND a 'financial documents' source rather than trying to make a single source do both with the given structure.

## Oct 27, 2023
Started the repo & prototyped a first-version of the scraper. I have sources for decatur trib & wand started (functioning!)

TODO:
- where img tag is a data url (like with wand), use srcset or something
- enable pagination feature (there's an initial pagination depth AND a recurring pagination depth)
- DONE for urls that don't have scheme/host, prepend it (WAND!)
- MAYBE create some kind of hook system that allows me to edit certain output or manually load information via php, where xpath is insufficient or the source data is weird, like a title that is all caps.
- implement cache expiration
- parse datetime string to a standardized format (likely configured within the source)
- Setup automatic loading of sources based on directory/file structure
- Document it (how to use it, what sources are available, etc)
- Create database tie-in. MAYBE use a different repo for that.
- Create GUI, but use a different repo for sure.
- MAYBE create search integration (probably in a different repo)
- MAYBE create rss feed system (probably as part of search integration)
- Make more robust, so that errors are handled & reported gracefully
- Consider NOT overwriting articles that I've already downloaded.
- Create bin script to run scraping & maybe to help with adding new sources? idk. Currently just running via phptest. Would like to have simple runner though. 
- Consider organizing cache (of pages) & article lists by the news organization, or having more human friendly names
- Maybe create index file where url points to the sha1 hash, if I continue to use sha1 for cached file names.

I'm working in:
- src/NewsScraper.php
- test/run/Main.php
- source/*

Note:
- Articles output to `articles/sha1(feed_url)`
- Pages are cached to `cache/page/*` & these are committed to git.
- I CANNOT freely store & share article content ... but there may be some (probably closed-source) approach that I could use to make them full text searchable w/out exposing the article content itself.
- I should add my own site's news sources.

