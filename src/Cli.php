<?php


namespace DecaturVote\NewsScraper;

class Cli extends \Tlf\Cli {


    protected \DecaturVote\NewsScraper $scraper;
    protected array $config = [];

    protected array $commands = [
            'list-sources' => "List available sources",
            'scrape' => "`scrape [org-name]/[source-name] [-page_depth int] [-print_results true]` Scrape a single source. `-` configs are optional",
        ];


    protected \PDO $pdo;

    public function __construct(){
        parent::__construct();

    }


    public function init(){
        $cli = $this;
        $call = [$cli, 'call'];

        $cli->load_command('main',
            function($cli, $args){
                if (isset($args['--'][0])){
                    $cli->command = 'scrape';
                    $this->call($cli, $args);
                } else {
                    $cli->call_command('help');
                }
            }, "no-args: Show help menu; w/ args: run scrape"
        );

        foreach ($this->commands as $command => $help_message){
            $this->load_command($command, $call, $help_message);
        }

        $this->scraper = new \DecaturVote\NewsScraper();
        $this->scraper->add_source_directory(dirname(__DIR__).'/source/v1/');
        $this->scraper->add_source_directory(dirname(__DIR__).'/source/v2/');
        $this->scraper->add_source_directory(dirname(__DIR__).'/source/v3/');


    }

    /**
     * Query via stored SQL statement and print results.
     *
     * @param $args value0 must be `org-name/source-name` or `org-name/*`
     * @param $named_args MAY contain `page_depth int` and `print_results true`
     */
    public function cmd_scrape(array $args, array $named_args){
        $sources = [];
        $print_results = $named_args['print_results'] ?? false;
        unset($named_args['print_results']);
        if ($print_results == 'true')$print_results = true;
        $this->scraper->runtime_options[$args[0].'.json'] = $named_args;
        foreach ($this->scraper->run_as_generator([$args[0]]) as $source_name => $source_meta ){
            $sources[$source_name] = $source_meta;

            if ($print_results){
                echo "\n\n ##### $source_name ##### \n\n";
                echo json_encode($source_meta,JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);
            }
        }

        //return $sources;
    }

    public function cmd_list_sources(){
        $sources = $this->scraper->getSources();
        //print_r($sources);
        $grouped_sources = [];
        /** @var \DecaturVote\NewsScraper\SourceInterface $source */
        foreach ($sources as $source_name => $source){
            $parts = explode("/",$source_name);
            $group = $parts[0];
            $source_name = $parts[1];
            //if (!isset($grouped_sources[$group]))$grouped_sources[$group] = [];
            $grouped_sources[$group][$source_name] = $source;
        }

        ksort($grouped_sources);
        foreach ($grouped_sources as $group=>$source_list){
            echo "\n  $group";
            foreach ($source_list as $source_file=>$source){
                //var_dump($source);
                $source_file = substr($source_file,0,-5);
                $source_name = $source->get_unique_human_name();
                echo "\n      $source_file -  $source_name";
            }
        }
    }

    /**
     * Get a PDO instance from stored config. Does not load config or perform any checks.
     *
     * @return PDO
     */
    public function getPdo(): \PDO {
        if (isset($this->pdo))return $this->pdo;
        $config = $this->config;
        $this->pdo = new \PDO('mysql:dbname='.$config['mysql.database'].';host='.$config['mysql.host'], $config['mysql.user'], $config['mysql.password']);  
        return $this->pdo;
    }

    /**
     * Get a BigDb instance from stored config. Does not load config or perform any checks.
     *
     * @TODO allow for multiple BigDb apps 
     * @TODO scan for BigDb subclasses (instead of using config). Or maybe use array-based config? idk.
     */
    public function getBigDb(): \Tlf\BigDb {
        $config = $this->config;
        $pdo = $this->getPdo();
        $class = $config['app.class'];
        $dir = $config['app.dir'];

        $db = new $class($pdo); // this is part of BigDb's tests.  
        $db->addSqlDir($dir.'/sql/', $force_recompile = false); // To add your own sql from `.sql` files on-disk. set force_recompile `true` during development of these files.  

        return $db;
    }





    /**
     * Call a cli command
     * @param $cli arg not used, because it should always be this object
     * @param $args array of args, with $args['--'] containing unnamed args.
     */
    public function call(\DecaturVote\NewsScraper\Cli $cli, array $args){
        // SETUP
        $command = $this->command;
        $unnamed_args = $args['--'] ?? [];
        unset($args['--']);

        $func = 'cmd_'.str_replace('-','_',$command);
        if (!method_exists($this, $func)){
            echo "\nCommand '$command' does not exist\n";
            return;
        }

        //// LOAD CONFIG
        //$config_file = $this->pwd.'/.config/bigdb.json';
        //if (!file_exists($config_file)){
            //echo ".config/bigdb.json doesn't exist. Run 'bigdb setup'";
            //return;
        //}
        //$this->config = json_decode(file_get_contents($config_file), true);


        // RUN
        echo "\n";
        $this->$func($unnamed_args, $args);
        echo "\n";
    }

    /**
     * Setup config / database credentials
     *
     * @param $cli cli instance
     * @param $args array of args, with $args['--'] containing unnamed args.
     */
    public function setup(\DecaturVote\NewsScraper\Cli $cli, array $args){
        echo "setup disabled temporarily";
        return;
        $config = getcwd().'/.config/bigdb.json';
        if (!file_exists($config)){
            $make = $cli->ask("Create .config/bigdb.json");
        } else {
            $make = $cli->ask("Overwrite .config/bigdb.json");
        }
        if (!$make){
            echo "\nConfig file cannot be written to. Cannot setup mysql.";
            return;
        }

        if (!is_dir(getcwd().'/.config/')){
            mkdir(getcwd().'/.config/', 0754);
        }

        //@TODO Allow multiple app dirs, and add scanning for app dirs (within vendor?)
        $app_dir = $cli->prompt("BigDb App Dir(relative to current dir)");

        $host = $cli->prompt("MySql Host");
        $database = $cli->prompt("MySql Database");
        $user = $cli->prompt("MySql User");
        // @TODO hide user input when prompting for mysql password
        $password = $cli->prompt("MySql Password");

        file_put_contents($config, 
            json_encode(
                [ 
                  'app.dir'=> $app_dir,
                  'mysql.host'=>$host,
                  'mysql.database'=>$database,
                  'mysql.user' => $user,
                  'mysql.password' => $password,
                ],
                JSON_PRETTY_PRINT
            )
        );

        $ignore = $cli->ask("Add .config/bigdb.json to gitignore");
        if ($ignore){
            $ignore_file = getcwd().'/.gitignore';
            $fh = fopen($ignore_file, 'a');
            try {
                fwrite($fh, "\n.config/bigdb.json");
            } 
            catch (\Exception $e){} 
            finally {
                fclose($fh);
            }
        }

    } 
    
}
