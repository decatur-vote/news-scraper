<?php

namespace DecaturVote\NewsScraper;

class Source2 implements SourceInterface {

    public string $outlet_name;
    public string $feed_name;
    /** php file inside the source/v2/ dir that will return an array of Article2 objects */
    public string $parser; 

    /** Page to scrape for a list of articles*/
    public string $url;

    /**
     * @param $source_data array<string key, string value> where keys are property names on this class. Extra keys (that don't map to properties) are allowed.
     */
    public function set_data(array $source_data){
        foreach ($source_data as $key=>$value){
            if (!property_exists($this, $key))continue;
            $this->$key = $value;
        }
    }

    public function get_data(): array {
        return get_object_vars($this);
    }

    public function get_parser(): Parser2 {
        return new Parser2($this);
    }

    /** Get a string name that should be unique across all sources of the given version */
    public function get_unique_name(): string {
        return $this->outlet_name.'-'.$this->feed_name.'-'.$this->url;
    }

    /** Get a human friendly string name that should be unique across all sources of the given version */
    public function get_unique_human_name(): string {
        return $this->outlet_name.' - '.$this->feed_name;
    }


    /**
     * @param $parse_result
     * @param $is_initial_load bool whether this source has any cached scraper results, since you may want to scan more pages the first time yous crape a website.
     */
    public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool{
        return false;
    }

    /*
     * Set runtime options, such as from cli arguments
     *
     * @param $args array<string key, mixed value> 
     * @key page_depth int number of pages to do for pagination.
     */
    public function set_runtime_options(array $args){
    }
}
