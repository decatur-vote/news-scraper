<?php

namespace DecaturVote\NewsScraper;

class Parser2 implements ParserInterface {

    public function __construct(protected Source2 $source){
    }


    public function goto_next_page(string $html): bool
    {
        return false;
    }
    public function get_page_urls(): array {
        return [$this->source->url];
    }

    public function should_download_file(string $url, int $filemtime, int $current_time): bool{
        // @TODO add cache invalidation support to Parser1
        return false;
    }

    /**
     * Return a clean version of the text with certain special characters removed or replaced.
     *
     * @return string
     */
    protected function clean_text(string $text_to_clean): string {
        $str = $text_to_clean;
        // replace angled single quotes
        $str = str_replace("\u{00e2}\u{0080}\u{0099}", "'", $str);
        $str = str_replace("\u{00e2}\u{0080}\u{0098}", "'", $str);
        // replace angled double quotes
        $str = str_replace("\u{201C}", "\"", $str);
        $str = str_replace("\u{201D}", "\"", $str);

        $str = preg_replace('/[^a-zA-Z0-9\'\"\s,_\-\&\%\$\#\@\!\?\(\)]/', ' ', $str);
        $str = preg_replace('/\s+/', ' ', $str);
        $str = trim($str);
        return $str;
    }

    /**
     *
     * @return array<string url, ArticleInterface $article> array of articles
     */
    public function get_articles(string $html, string $page_url): array {

        $src_dir = dirname(__DIR__,2).'/source/';
        $source = $this->source;
        $doc = new \Taeluf\PHTML($html);

        $articles = require($src_dir.'/v2/'.$source->parser);


        return $articles;









        return [];
        $articles = $doc->xpath($source->article_selector);

        echo "\n\n\nArticles:\n\n\n";

        //echo $articles[0];
        //exit;

        echo "\n\n\n";

        $article_list = [];
        foreach ($articles as $article){

            $article_doc = new \Taeluf\PHTML(''.$article);

            echo $article."\n\n";

            echo $source->title_selector."\n\n";;
            $title_node = $article_doc->xpath($source->title_selector)[0] ?? null;
            if ($title_node ==null)continue;
            $title = $article_doc->xpath($source->title_selector)[0]->innerText;
            $title = $this->clean_text($title);

            $date_node = $article_doc->xpath($source->date_selector)[0];
            $date = $date_node->getAttribute('datetime');
            $description = $article_doc->xpath($source->description_selector)[0]->innerText;
            $description = $this->clean_text($description);

            $photo_url = null;
            if ($source->photo_selector!=null){
                $photo_node_list = $article_doc->xpath($source->photo_selector);
                if (count($photo_node_list)>0)$photo_url = trim($photo_node_list[0]->src);
            }
            


            $url = trim($article_doc->xpath($source->url_selector)[0]->href);
            $host = parse_url($url, PHP_URL_HOST);
            if ($host==null){
                $source_url = parse_url($source->url);
                $url = $source_url['scheme'].'://'.$source_url['host'].$url;
            }

            $article_list[] = 
                new Article1(
                    [
                    'title'=>$title,
                    'date'=>$date,
                    'url'=>$url,
                    'description'=>$description,
                    'photo_url'=>$photo_url,
                    ]
                )
                ;

            //echo "\n\n\n------\n\n\n";
            //echo $article;
            //echo "\n\n";
        }

            //echo "\n\n\nARTICLES\n\n\n";
            print_r($article_list);


            //if ($source->next_page_selector!=null){
                //// get next page url
                //$next_page_url = $doc->xpath($source->next_page_selector)[0]->href;

                //echo "\n\nNEXT PAGE: $next_page_url\n\n";
            //}

            return $article_list;
    }

    /** sleep for ms defined on sources as 'pagination_delay_ms'. */
    public function delay_before_downloading(){
        return;
        if ($this->is_paginating){
            $delay_ms = $this->source->pagination_delay_ms;
            echo "\nSLEEP for {$delay_ms}ms\n";
            usleep($delay_ms * 1000);
        }
    }
}
