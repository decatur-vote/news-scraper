<?php

namespace DecaturVote\NewsScraper;

class Article2 implements ArticleInterface {


    /**
     * @param $data varies by source
     */
    public function __construct(protected array $data){

    }
    /**
     * Get a unique name for an article
     * @return a unique name, usually a url
     */
    public function get_unique_name(): string{
        return $this->data['url'];
    }

    
    /**
     * Data varies by source
     *
     * @return array of article data
     */
    public function get_data(): array{
        return $this->data;
    }

}
