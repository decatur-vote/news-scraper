<?php

namespace DecaturVote;

class NewsScraper {

    /**
     * array<string rel_file_path, SourceInterface $source>
     */
    protected array $sources = [];

    /**
     * array<string version_name, string php_class_name> map of source versions to class names
     */
    protected array $source_version_map = [
        'error'=>'DecaturVote\\NewsScraper\\SourceError',
        '1'=>'DecaturVote\\NewsScraper\\Source1',
        '2'=>'DecaturVote\\NewsScraper\\Source2',
        '3' => 'DecaturVote\\NewsScraper\\SourceArticles',
    ];

    /**
     * Array of runtime options to pass to sources to do with as they please.
     * 
     */
    public array $runtime_options = [];

    public function getSources(): array {
        return $this->sources;
    }

    /**
     * Add all sources within a directory.
     * Directory must contain sub-dirs like 'us-copyright-office' that contain files like 'fair-use-index.json'
     * Format 'org-name/source-name.json'
     */
    public function add_source_directory(string $absolute_path){
        foreach (scandir($absolute_path) as $file){
            if ($file=='.'||$file=='..')continue;
            //echo "\nLoad $absolute_path/$file\n";
            //echo "\n\n\n\n\n";
            //var_dump($absolute_path.'/'.$file);
            //echo "\n\n\n\n\n";
            if (!is_dir($absolute_path.'/'.$file))continue;

            foreach (scandir($absolute_path.'/'.$file) as $json_file){
                if (substr($json_file,-5)!='.json')continue;
                $source_name = $file.'/'.$json_file;
                $content = file_get_contents($absolute_path.'/'.$file.'/'.$json_file);
                try {
                    $data = json_decode($content, true, 512, JSON_THROW_ON_ERROR);
                } catch (\Exception $e) {
                    //error_log("JSON Failed to decode in file ".basename($absolute_path).'/'.$file.'/'.$json_file);
                    //echo "\nJSON Failed to decode in file ".basename($absolute_path).'/'.$file.'/'.$json_file."\n";
                    $this->add_source(['version'=>'error'], $source_name);
                    continue;
                }
                $this->add_source($data, $source_name);
                //var_dump($data);
            }

            echo "\n\n";

        }

    } 
    /**
     * @param $source_data array of data valid with one of the available source classes.
     * @param $source_name relative file path like 'us-copyright/fair-use-index.json'
     */
    public function add_source(array $source_data, string $source_name){
        $version = $source_data['version'] ?? '1';
        $class = $this->source_version_map[$version];
        $source = new $class();
        $source->set_data($source_data);
        $this->sources[$source_name] = $source;
    }

    /**
     * @param $source_name relative file path like 'us-copyright/fair-use-index.json'
     */
    public function get_articles_for_source(string $source_name): array {
        $cached_file = $this->get_cached_articles_path($this->sources[$source_name]);
        $json = file_get_contents($cached_file);
        return json_decode($json, true);
    }

    public function get_cached_path(string $unique_data, string $cache_dir = 'unsorted'): string{
        $hash = sha1($unique_data);
        $path = dirname(__DIR__)."/cache/$cache_dir/".$hash.'.txt';

        return $path;
    
    }

    public function get_cached_page_path(string $url): string{
        $hash = sha1($url);
        $host = parse_url($url, PHP_URL_HOST);
        $parts = explode('.',$host);
        if ($parts[0]=='www')array_shift($parts);
        if (in_array($parts[count($parts)-1],['com','org','net','gov'])){
            array_pop($parts);
        }
        $host = implode('.', $parts);

        $dir = dirname(__DIR__).'/cache/page/'.$host.'/';
        $path = $dir.$host.'-'.$hash.'.html';

        return $path;
    }

    /**
     * Get path to cached articles list
     *
     * @param $source the source the articles come from
     * @return string absolute file path, whether file exists or not
     */
    public function get_cached_articles_path(NewsScraper\SourceInterface $source): string {
        $rel_path = array_search($source, $this->sources);
        if (!is_string($rel_path)){
            throw new \Exception("Cannot find source name");
        }

        $dir = dirname(__DIR__).'/cache/article/';
        $path = $dir.$rel_path;
        if (!is_dir(dirname($path)))mkdir(dirname($path), 0751, true);
        return $path;
        //$source_name = get_class($source).'::'.$source->get_unique_name();
        //$articles_file = $dir.sha1($source_name).'.json';
        //return $articles_file;
    }

    public function get_stored_articles(NewsScraper\SourceInterface $source): array {
        $articles_file = $this->get_cached_articles_path($source);
        
        if (!file_exists($articles_file)){
            error_log("Articles Data File does not exist. File: '$articles_file'");
            return [];
        }
        $data = json_decode(file_get_contents($articles_file), true);
        if (!is_array($data)){
            error_log("Articles Data File does not contain valid json. File: '$articles_file'");
            return [];
        }
        return $data;
    }

    /**
     * Download a page url & return the result
     *
     * @param $page_url url to download
     * @param $milliseconds_sleep number of milliseconds to sleep for before downloading. Useful to avoid spamming. Default is 500
     *
     * @return html string
     */
    public function download_page(string $page_url, int $milliseconds_sleep=500): string {
        usleep($milliseconds_sleep * 1000);
        //$html = file_get_contents($page_url);

        $ch = curl_init();

        $url = $page_url; 
        //var_dump($page_url);
        //exit;

        $host = parse_url($url, PHP_URL_HOST);
        $headers = [
            "Accept" => "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
            "Accept-Encoding" => "gzip, deflate, br", 
            "Accept-Language" => "en-US,en;q=0.5", 
            "Cache-Control" => "no-cache", 
            "Connection" => "keep-alive", 
            "Host" => $host, //"herald-review.com", 
            "Pragma" => "no-cache", 
            "Sec-Fetch-Dest" => "document", 
            "Sec-Fetch-Mode" => "navigate", 
            "Sec-Fetch-Site" => "none", 
            "Sec-Fetch-User" => "?1", 
            "TE" => "trailers", 
            "Upgrade-Insecure-Requests" => "1", 
            "User-Agent" => "Mozilla/5.0 (X11; Linux x86_64; rv:120.0) Gecko/20100101 Firefox/120.0", 
        ];

        $curl_headers = [];
        foreach ($headers as $key=>$value){
            if (is_int($key)){
                $curl_headers[] = $value;
                continue;
            }
            $curl_headers[] = $key.': '.$value;
        }

        curl_setopt_array($ch,
            [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => false,
            CURLOPT_HEADER => true,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTPHEADER => $curl_headers,
            ]
        );


        //if (count($curl_opts) > 0 ){
            //curl_setopt_array($ch, $curl_opts);
        //}

        $browser = new \Tlf\Tester\CurlBrowser('');
        $response = $browser->curl_get_response($ch);

        curl_close($ch);


        if (isset($response['headers']['content-encoding'])&&$response['headers']['content-encoding']=='gzip'){
            $response['body'] = gzdecode($response['body']);
        }

        return $response['body'];
    }


    /**
     *
     *
     * @param $sources_to_run array<int index, string org-name/source-name> array of source names to run, or empty array to run all
     * @return array<string source_name, int num_articles>
     */
    public function run(array $sources_to_run = []): array {

        $sources_ran = [];
        foreach ($this->run_as_generator($sources_to_run) as $source_name => $source_meta){
            $sources_ran[$source_name] = count($source_meta['articles']);
        }
        return $sources_ran;
    }

    /**
     *
     *
     * @param $sources_to_run array<int index, string org-name/source-name> array of source names to run, or empty array to run all
     * @yield 'source_name'=>$article_meta array like `['source'=>$source_data, 'articles'=>$articles_list]`
     */
    public function run_as_generator(array $sources_to_run = []){
        //$dir = dirname(__DIR__).'/articles2/';

        if (count($sources_to_run)==0){
            $source_list = $this->sources;
        } else {
            $source_list = [];
            foreach ($sources_to_run as $index => $source_name){
                $json_name = $source_name;
                if (substr($json_name,-5)!='.json')$json_name.='.json';
                if (substr($source_name,-1)=='*'){
                    foreach ($this->sources as $json_name=>$source){
                        $search = substr($source_name,0,-1);
                        $len = strlen($search);
                        if (substr($json_name,0,$len)==$search){
                            $source_list[$json_name] = $this->sources[$json_name];
                        }
                    }                      
                } else {
                    $source_list[$json_name] = $this->sources[$json_name];
                }
            }
        }

        foreach ($source_list as $json_name => $source){
            $wildcard_name = explode('/',$json_name)[0].'/*.json';
            if (isset($this->runtime_options[$json_name])){
                $source->set_runtime_options($this->runtime_options[$json_name]);
            } else if (isset($this->runtime_options[$wildcard_name])){
                $source->set_runtime_options($this->runtime_options[$wildcard_name]);
            }
            $pagination_index = -1;
            $is_first_scrape = false;
            $parser = $source->get_parser();


            $existing_articles_meta = $this->get_stored_articles($source);
            $existing_articles = $existing_articles_meta['articles'] ?? [];

            $parse_result = new \DecaturVote\NewsScraper\ParseResult();
            $parse_result->original_articles_count = count($existing_articles);
            $parse_result->total_article_count = count($existing_articles);

            do {
                $parse_result->pagination_index++;
                $pages = $parser->get_page_urls();
                $parse_result->num_articles_parsed = 0;
                $parse_result->num_new_articles_this_page = 0;


                echo "\n\nSTART LOOP ".$parse_result->pagination_index;

                foreach ($pages as $page_url){
                    echo "\n  PARSE PAGE URL: $page_url";
                    $cache_file = $this->get_cached_page_path($page_url);
                    $mtime = file_exists($cache_file) ? filemtime($cache_file) : false;
                    if ($mtime===false||$parser->should_download_file($page_url, $mtime, time())){
                        $parser->delay_before_downloading();
                        echo "\n   --download_fresh_copy--  ";
                        $html = $this->download_page($page_url);
                        file_put_contents($cache_file, $html);
                    } else {
                        echo "\n   --load_cached_copy--  ";
                        $html = file_get_contents($cache_file);
                    }
                    //$phtml = new \Taeluf\PHTML($html);
                    $articles = $parser->get_articles($html, $page_url);
                    $num_parsed = count($articles);
                    $parse_result->total_articles_parsed_count += $num_parsed;
                    $parse_result->num_articles_parsed += $num_parsed;
                    foreach ($articles as $sort_index => $article){
                        // @TODO add article data versioning
                        $data = $article->get_data();
                        //$data['_sort_index_'] = $sort_index;
                        $unique_name = $article->get_unique_name();
                        if (!isset($existing_articles[$unique_name])){
                            $parse_result->num_new_articles_this_page++;
                            $parse_result->total_new_article_count++;
                            //var_dump($unique_name);
                            //exit;
                        }
                        $existing_articles[$unique_name] = $data;
                    }
                    $parse_result->total_article_count = count($existing_articles);
                }

                echo $parse_result->debug_msg();

            } while ($source->should_scrape_next_page($parse_result, $is_first_scrape) && $parser->goto_next_page($html));

            //echo "\n\n\n\n\n";
            //echo "SEARCHFORTHISSTRING";
            //echo "\n\n\n\n\n";
            //exit;

                //uksort($existing_articles,
                    //function ($a_key, $b_key) use ($existing_articles) {
                        //$a = $existing_articles[$a_key];
                        //$b = $existing_articles[$b_key];
                        //$a_index = $a['_sort_index_'];
                        //$b_index = $b['_sort_index_'];
                        //unset($a['_sort_index_']);
                        //unset($b['_sort_index_']);
                        //if ($a_index > $b_index) return 1;
                        //if ($a_index < $b_index) return -1;
                        //return 0;
                    //}
                //);

            $article_meta = [
                'source'=>$source->get_data(),
                'articles'=>$existing_articles,
            ];

            $articles_json = json_encode($article_meta, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES);

            file_put_contents($this->get_cached_articles_path($source), $articles_json);

            yield $source->get_unique_human_name() => $article_meta;
        }

    }

}
