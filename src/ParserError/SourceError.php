<?php

namespace DecaturVote\NewsScraper;

class SourceError implements SourceInterface {


    /**
     * @param $source_data array<string key, string value> where keys are property names on this class. Extra keys (that don't map to properties) are allowed.
     */
    public function set_data(array $source_data){
        //error_log("Cannot set data. Source failed to decode.");
    }

    public function get_data(): array {
        error_log("Cannot get data. Source failed to decode.");
        return ['version'=>'error'];
    }

    public function get_parser(): ParserError {
        return new ParserError($this);
    }

    /** Get a string name that should be unique across all sources of the given version */
    public function get_unique_name(): string {
        return 'ERROR json failed to load'. uniqid();
    }

    /** Get a human friendly string name that should be unique across all sources of the given version */
    public function get_unique_human_name(): string {
        return 'ERROR json failed to load';
    }


    /**
     * @param $parse_result
     * @param $is_initial_load bool whether this source has any cached scraper results, since you may want to scan more pages the first time yous crape a website.
     */
    public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool{
        return false;
    }

    /*
     * Set runtime options, such as from cli arguments
     *
     * @param $args array<string key, mixed value> 
     * @key page_depth int number of pages to do for pagination.
     */
    public function set_runtime_options(array $args){
    }
}
