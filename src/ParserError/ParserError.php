<?php

namespace DecaturVote\NewsScraper;

class ParserError implements ParserInterface {

    public function goto_next_page(string $html): bool
    {
        return false;
    }
    /**
     * error
     *
     * @return array<int index, string url> empty array
     */
    public function get_page_urls(): array{return [];}

    /**
     * error
     *
     * @param $url the url to download
     * @param $filemtime the unix int time a file was last modified
     * @param $current_time the current unix int time
     * @return bool false, always
     */
    public function should_download_file(string $url, int $filemtime, int $current_time): bool {
        return false;
    }

    /**
     * error
     *
     * @param html document to parse for articles
     * @return array<string url, ArticleInterface $article> empty array
     */
    public function get_articles(string $html, string $page_url): array {
        return [];
    }



    /** sleep for ms defined on sources as 'pagination_delay_ms'. */
    public function delay_before_downloading(){
        return;
        if ($this->is_paginating){
            $delay_ms = $this->source->pagination_delay_ms;
            echo "\nSLEEP for {$delay_ms}ms\n";
            usleep($delay_ms * 1000);
        }
    }
}
