<?php

namespace DecaturVote\NewsScraper;

class Source1 implements SourceInterface {

    public string $outlet_name;
    public string $feed_name;

    /** Page to scrape for a list of articles*/
    public string $url;
    /** On the first-scrape, how many 'next' pages should be scraped */
    public int $initial_page_depth;
    /** On regular re-scrapes, how many 'next' pages should be scraped */
    public int $standard_page_depth;
    /** How frequently the $url should be re-downloaded, in minutes */ 
    public int $max_poll_frequency;

    /** xpath selector that returns an array of article nodes, containing article information. 
     * All other selectors are executed from this node, NOT from the page source. (next_page_selector is from page source though)
     */
    public string $article_selector;
    /** xpath selector that returns a node containing a title */
    public string $title_selector;
    /** xpath selector that returns an <a> tag where the href is the url of the article */
    public string $url_selector;
    /** xpath selector where innerText is the description of the article */
    public string $description_selector;
    /** xpath selector where img.src is the url of an image */
    public ?string $photo_selector;

    /** xpath selector where ... idk actually */
    public string $date_selector;
    /** inner_text, or datetime */
    public string $date_type = 'datetime';
    /** a regex pattern where the first parentheses match is the datetime string*/
    public string $date_pattern;
    /** (optional) if the datetime string is not your desired format, this should be the format to give to DateTime::createFromFormat() for the input date, then output an ISO 8601 date */
    public string $date_format;

    /** xpath selector that returns an <a> node where the href points to the next page, listing articles */
    public ?string $next_page_selector;

    /** Number of milliseconds to wait before downloading the next paginated page. Default is 100ms */
    public int $pagination_delay_ms = 100;



    /**
     * @param $source_data array<string key, string value> where keys are property names on this class. Extra keys (that don't map to properties) are allowed.
     */
    public function set_data(array $source_data){
        foreach ($source_data as $key=>$value){
            if (!property_exists($this, $key))continue;
            $this->$key = $value;
        }
    }

    public function get_data(): array {
        return get_object_vars($this);
    }

    public function get_parser(): Parser1 {
        return new Parser1($this);
    }

    /** Get a string name that should be unique across all sources of the given version */
    public function get_unique_name(): string {
        return $this->outlet_name.'-'.$this->feed_name.'-'.$this->url.'-'.$this->article_selector;
    }

    /** Get a human friendly string name that should be unique across all sources of the given version */
    public function get_unique_human_name(): string {
        return $this->outlet_name.' - '.$this->feed_name;
    }


    /**
     * @param $parse_result
     * @param $is_initial_load bool whether this source has any cached scraper results, since you may want to scan more pages the first time yous crape a website.
     */
    public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool
    {
        $page_num = $parse_result->pagination_index;
        $page_depth = $this->standard_page_depth;

        if ($parse_result->num_articles_parsed == 0)return false;

        if ($is_initial_load && $page_num < $this->initial_page_depth)return true;
        else if (!$is_initial_load && $page_num < $this->standard_page_depth)return true;

        return false;
    }

    /**
     *  @param $args array<string key, mixed value> 
     *  @key page_depth int number of pages to do for pagination.
     */
    public function set_runtime_options(array $args){
        if (isset($args['page_depth']))$this->standard_page_depth = $args['page_depth'];
    }
}
