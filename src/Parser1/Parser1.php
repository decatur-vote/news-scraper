<?php

namespace DecaturVote\NewsScraper;

class Parser1 implements ParserInterface {

    protected string $active_url;
    /** Indicate that we are NOT working on the first page. */
    protected bool $is_paginating = false;

    public function __construct(protected Source1 $source){
        $this->active_url = $source->url;
    }


    public function goto_next_page(string $html): bool
    {
        $next = $this->get_next_page_url($html);
        if ($next == null)return false;
        $this->is_paginating = true;
        $this->active_url = $next;
        return true;
    }
    public function get_page_urls(): array {
        return [$this->active_url];
    }

    public function should_download_file(string $url, int $filemtime, int $current_time): bool{

        $seconds_age = $current_time - $filemtime;
        $minutes_age = $seconds_age / 60;
        //$poll_freq_in_millis = $this->source->max_poll_frequency * 60 * 1000;

        if ($minutes_age >= $this->source->max_poll_frequency){
            return true;
        }

        return false;
    }

    /** sleep for ms defined on sources as 'pagination_delay_ms'. */
    public function delay_before_downloading(){
        if ($this->is_paginating){
            $delay_ms = $this->source->pagination_delay_ms;
            echo "\nSLEEP for {$delay_ms}ms\n";
            usleep($delay_ms * 1000);
        }
    }

    /**
     * Return a clean version of the text with certain special characters removed or replaced.
     *
     * @return string
     */
    protected function clean_text(string $text_to_clean): string {
        $str = $text_to_clean;
        // replace angled single quotes
        $str = str_replace("\u{00e2}\u{0080}\u{0099}", "'", $str);
        $str = str_replace("\u{00e2}\u{0080}\u{0098}", "'", $str);
        // replace angled double quotes
        $str = str_replace("\u{201C}", "\"", $str);
        $str = str_replace("\u{201D}", "\"", $str);

        $str = preg_replace('/[^a-zA-Z0-9\'\"\s,_\-\&\%\$\#\@\!\?\(\)]/', ' ', $str);
        $str = preg_replace('/\s+/', ' ', $str);
        $str = trim($str);
        return $str;
    }

    /**
     *
     * @return string url or null
     */
    public function get_next_page_url(string $html): ?string {

        $source = $this->source;
        $doc = new \Taeluf\PHTML($html);
        $next_page_node = $doc->xpath($source->next_page_selector)[0] ?? null;
        if ($next_page_node == false)return false;
        $href = $next_page_node->getAttribute('href');
        if ($href=='')return null;

        $original_url = $this->source->url;
        $parsed_original = parse_url($original_url);
        $parsed = parse_url($href);

        $final = $parsed;
        if (!isset($final['scheme']))$final['scheme'] = $parsed_original['scheme'];
        if (!isset($final['host']))$final['host'] = $parsed_original['host'];
        if (!isset($final['path']))$final['path'] = $parsed_original['path'];
        if (isset($final['query']))$final['query'] = '?'.$final['query'];
        else $final['query'] = '';

        $full_url = 
            $final['scheme'].'://'
            .$final['host']
            .$final['path']
            .$final['query'];

        echo "\nNEXT PAGE URL\n";
        echo "  $full_url";

        return $full_url;
    }
    /**
     *
     * @return array<string url, ArticleInterface $article> array of articles
     */
    public function get_articles(string $html, string $page_url): array {

        $source = $this->source;
        $doc = new \Taeluf\PHTML($html);
        $articles = $doc->xpath($source->article_selector);

        //echo "\n\n\nArticles:\n\n\n";

        //echo $articles[0];
        //exit;

        //echo "\n\n\n";

        $article_list = [];
        $i = 0;
        foreach ($articles as $article){
            $sort_index = $i;
            $i++;

            $article_doc = new \Taeluf\PHTML(''.$article);

            //echo $article."\n\n";

            //echo $source->title_selector."\n\n";;
            $title_node = $article_doc->xpath($source->title_selector)[0] ?? null;
            if ($title_node ==null)continue;
            $title = $article_doc->xpath($source->title_selector)[0]->innerText;
            $title = $this->clean_text($title);

            $date_type = $source->date_type;
            $date_node = $article_doc->xpath($source->date_selector)[0];
            switch ($date_type){
            case "datetime":
                $date_text = $date_node->getAttribute('datetime');
                break;
            case "inner_text":
                $date_text = $date_node->innerText;
                break;
            }

            //$date = $date_node."\n\n\n\n\n".$date_node->innerText;

            $date = $date_text;
            if (isset($source->date_pattern)){
                $did_match = preg_match($source->date_pattern,$date_text, $matches);
                if (!$did_match){
                    error_log("Failed pattern matching '$date_text' w/ pattern ".$source->date_pattern);
                }
                $date = $matches[1];
            }

            if (isset($source->date_format)){
                $dt = \DateTime::createFromFormat($source->date_format, $date);
                if ($dt instanceof \DateTime){
                    $date = $dt->format('c');
                    $utc = $dt->format('U');
                    while(isset($article_list[$utc])){
                        $utc++;
                    }
                    $sort_index = $utc;
                }
            }

            $description_nodes = $article_doc->xpath($source->description_selector);
            if (count($description_nodes)==0){
                echo "\n\n\n\n----------\n";
                echo "\nFAILED to find/parse description\n";
                echo "\nSelector: ".$source->description_selector;
                //echo "\n\nARTICLE DOC:"
                    //.$article_doc
                    //."\n\n----------\n\n";

                $description = "[NO-DESCRIPTION-FOUND]";
            } else {
                $description = $description_nodes[0]->innerText;
            }

            $description = $this->clean_text($description);


            $photo_url = null;
            if ($source->photo_selector!=null){
                $photo_node_list = $article_doc->xpath($source->photo_selector);
                if (count($photo_node_list)>0)$photo_url = trim($photo_node_list[0]->src);
            }
            


            $url = trim($article_doc->xpath($source->url_selector)[0]->href);
            $host = parse_url($url, PHP_URL_HOST);
            if ($host==null){
                $source_url = parse_url($source->url);
                $url = $source_url['scheme'].'://'.$source_url['host'].$url;
            }

            $article_list[$sort_index] = 
                new Article1(
                    [
                    'title'=>$title,
                    'date'=>$date,
                    'url'=>$url,
                    'description'=>$description,
                    'photo_url'=>$photo_url,
                    ]
                )
                ;

            //echo "\n\n\n------\n\n\n";
            //echo $article;
            //echo "\n\n";
        }


        ksort($article_list);

            //echo "\n\n\nARTICLES\n\n\n";
            //print_r($article_list);


            //if ($source->next_page_selector!=null){
                //// get next page url
                //$next_page_url = $doc->xpath($source->next_page_selector)[0]->href;

                //echo "\n\nNEXT PAGE: $next_page_url\n\n";
            //}

        return $article_list;
    }

}
