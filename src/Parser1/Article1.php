<?php

namespace DecaturVote\NewsScraper;

class Article1 implements ArticleInterface {

    /** title of the article. */
    public string $title;

    /** date and time the article was published. */
    public string $date;

    /** URL of the article. */
    public string $url;

    /** description of the article. */
    public string $description;

    /** URL of the main photo associated with the article. */
    public string $photo_url;

    public function __construct(protected array $data){

    }
    /**
     * Get a unique name for an article
     * @return a unique name, usually a url
     */
    public function get_unique_name(): string{
        return $this->data['url'];
    }

    
    /**
     * Get array of article data
     *
     * @return array of article data
     */
    public function get_data(): array{
        return $this->data;
    }

}
