<?php

namespace DecaturVote\NewsScraper;

interface ArticleInterface {

    /**
     * Get a unique name for an article
     * @return a unique name, usually a url
     */
    public function get_unique_name(): string;

    /**
     * Get array of article data
     *
     * @return array of article data
     */
    public function get_data(): array;

}
