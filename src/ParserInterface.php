<?php

namespace DecaturVote\NewsScraper;

interface ParserInterface {

    /**
     * Get an array of initial page urls (This method is NOT used for pagination). 
     * Often, this is just one url.
     *
     * @return array<int index, string url>
     */
    public function get_page_urls(): array;

    /**
     * Check if an already cached file should be re-downloaded.
     *
     * @param $url the url to download
     * @param $filemtime the unix int time a file was last modified
     * @param $current_time the current unix int time
     * @return true do download file. false to use cached file.
     */
    public function should_download_file(string $url, int $filemtime, int $current_time): bool;

    /**
     * Get an array of articles
     *
     * @param $html string document to parse for articles
     * @param $page_url string url of the page this html came from
     * @return array<string url, ArticleInterface $article> array of articles
     */
    public function get_articles(string $html, string $page_url): array;

    /**
     * Informs the source the next page will be loaded. On the next loop. 
     * If the 2nd page uses the same source & parser, then you shouldn't need to do anything here.
     *
     * @param string $html the html of the current page, where a pagination link should be found
     *
     * @return bool true ALWAYS return true, because should_scrape_next_page() SHOULD be the one to stop processing. But goto_next_page() can return false to stop processing.
     */
    public function goto_next_page(string $html): bool;

}
