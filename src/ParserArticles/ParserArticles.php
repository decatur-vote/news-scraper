<?php

namespace DecaturVote\NewsScraper;

class ParserArticles implements ParserInterface {

    /** File handle, from fopen */
    public $fh;

    public int $articles_written = 0;

    public function __construct(protected SourceArticles $source){
        $file_path = dirname(__DIR__,2).'/cache/full-articles/'.$source->name.'.json';
        if (!is_dir(dirname($file_path)))mkdir(dirname($file_path),0754);
        $this->fh = fopen($file_path,'w');
        fwrite($this->fh, "{\n");
        fwrite($this->fh, '"source": '.json_encode($this->source->get_data(), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
        fwrite($this->fh, ",\n\"articles\": [\n");
    }

    public function __destruct(){
        fwrite($this->fh, "\n]\n}");
        fclose($this->fh);
    }


    public function goto_next_page(string $html): bool
    {
        return false;
    }
    public function get_page_urls(): array {

        $cache_dir = dirname(__DIR__, 2).'/cache/article/';

        $urls = [];

        $feeds_to_scan = $this->source->feed_names;
        foreach ($feeds_to_scan as $index=>$name){
            $file = $cache_dir.$name.'.json';
            $meta = json_decode(file_get_contents($file), true);

            foreach ($meta['articles'] as $index_or_key => $article){
                $urls[] = $article['url'];
            }

        }

        return $urls;
    }

    public function should_download_file(string $url, int $filemtime, int $current_time): bool{
        return false;
    }

    /**
     * Return a clean version of the text with certain special characters removed or replaced.
     *
     * @return string
     */
    protected function clean_text(string $text_to_clean): string {
        $str = $text_to_clean;
        // replace angled single quotes
        $str = str_replace("\u{00e2}\u{0080}\u{0099}", "'", $str);
        $str = str_replace("\u{00e2}\u{0080}\u{0098}", "'", $str);
        // replace angled double quotes
        $str = str_replace("\u{201C}", "\"", $str);
        $str = str_replace("\u{201D}", "\"", $str);

        $str = preg_replace('/[^a-zA-Z0-9\'\"\s,_\-\&\%\$\#\@\!\?\(\)]/', ' ', $str);
        $str = preg_replace('/\s+/', ' ', $str);
        $str = trim($str);
        return $str;
    }

    /**
     *
     * @return array<string url, ArticleInterface $article> array of articles
     */
    public function get_articles(string $html, string $page_url): array {

        $src_dir = dirname(__DIR__,2).'/source/';
        $source = $this->source;
        $doc = new \Taeluf\PHTML($html);

        echo "\n\n\nArticles:\n\n\n";

        echo "\n\n\n";

        $description_node = $doc->xpath($source->description_selector)[0];
        if (strtoupper($description_node->nodeName) == 'META'){
            $description = $description_node->content;
        } else if (strtoupper($description_node->nodeName) == '#TEXT'){
            $description = $description_node->textContent;
        }

        $articles = $doc->xpath($source->article_selector);



        $article_list = [];
        foreach ($articles as $article){

            $article_doc = new \Taeluf\PHTML(''.$article);

            //echo $article."\n\n";

            //echo $source->title_selector."\n\n";;
            $title_node = $article_doc->xpath($source->title_selector)[0] ?? null;
            if ($title_node ==null)continue;
            $title = $article_doc->xpath($source->title_selector)[0]->innerText;
            $title = $this->clean_text($title);

            $body_node = $article_doc->xpath($source->body_selector)[0];
            $body = $body_node->innerText;

            if ($source->author_selector == null)$author_name = null;
            else {
                $author_node = $article_doc->xpath($source->author_selector)[0];
                $author_name = $author_node->innerText;
            }

            if ($source->author_url_selector == null)$author_url = null;
            else {
                $author_url_node = $article_doc->xpath($source->author_url_selector)[0];
                $author_url = $author_url_node->href;
                $host = parse_url($author_url, PHP_URL_HOST);
                if ($host==null){
                    $source_url = parse_url($source->host_url);
                    $author_url = $source_url['scheme'].'://'.$source_url['host'].$author_url;
                }
            }


            $created_date_node = $article_doc->xpath($source->created_date_selector)[0];
            $created_date = $created_date_node->getAttribute('datetime');

            if ($source->updated_date_selector == null)$updated_date = null;
            else {
                $updated_date_node = $article_doc->xpath($source->updated_date_selector)[0];
                $updated_date = $updated_date_node->getAttribute('datetime');
            }

            $photo_url = null;
            if ($source->photo_selector!=null){
                $photo_node_list = $article_doc->xpath($source->photo_selector);
                if (count($photo_node_list)>0){
                    $photo_url = trim($photo_node_list[0]->src);
                    $host = parse_url($photo_url, PHP_URL_HOST);
                    if ($host==null){
                        $source_url = parse_url($source->host_url);
                        $photo_url = $source_url['scheme'].'://'.$source_url['host'].$photo_url;
                    }
                }

            }
            
            $full_article = 
                [
                'url'=>$page_url,
                'title'=>$title,
                'description'=>$description,
                'photo_url'=>$photo_url,
                'author_name'=>$author_name,
                'author_url'=>$author_url,
                'created_date'=>$created_date,
                'updated_date'=>$updated_date,
                'body'=>$body,
                //'url'=>$url,
                'description'=>$description,
                'photo_url'=>$photo_url,
                ];

            if ($this->articles_written > 0){
                fwrite($this->fh, ",\n".json_encode($full_article, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
            } else {
                fwrite($this->fh, json_encode($full_article, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));
            }

            $this->articles_written++;
            
            // store full article to disk.


            unset($full_article['body']);
            $article_list[] = 
                new ArticleArticles($full_article);

            //echo "\n\n\n------\n\n\n";
            //echo $article;
            //echo "\n\n";
        }

            //echo "\n\n\nARTICLES\n\n\n";
            //print_r($article_list);


            //if ($source->next_page_selector!=null){
                //// get next page url
                //$next_page_url = $doc->xpath($source->next_page_selector)[0]->href;

                //echo "\n\nNEXT PAGE: $next_page_url\n\n";
            //}

            return $article_list;
    }

    /** sleep for ms defined on sources as 'pagination_delay_ms'. */
    public function delay_before_downloading(){
        $delay_ms = $this->source->delay_ms_between_downloads;
        echo "\nSLEEP for {$delay_ms}ms\n";
        usleep($delay_ms * 1000);
    }
}
