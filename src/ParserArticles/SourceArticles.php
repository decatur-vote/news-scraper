<?php

namespace DecaturVote\NewsScraper;

class SourceArticles implements SourceInterface {

    /**
     * string org-name/source-name
     */
    public string $name;
    /**
     * array<int index, string org-name/feed-name
     */
    public array $feed_names;

    /** How frequently the $url should be re-downloaded, in minutes */ 
    public int $max_poll_frequency;


    /** xpath selector of a meta node whose content attribute is a summary/description */
    public string $description_selector;

    /** xpath selector that returns an array of article nodes, containing article information. 
     * All other selectors are executed from this node, NOT from the page source. (next_page_selector is from page source though)
     */
    public string $article_selector;
    /** xpath selector that returns a node containing a title */
    public string $title_selector;
    /** xpath selector that returns an <a> tag where the href is the url of the article */
    public string $url_selector;
    /** xpath selector where img.src is the url of an image */
    public ?string $photo_selector;

    /** xpath selector where <time datetime="ISO 8601 time"> */
    public string $created_date_selector;

    /** xpath selector where <time datetime="ISO 8601 time"> */
    public ?string $updated_date_selector;

    /** inner text is the body */
    public string $body_selector;
    /** inner text is the author_name */
    public ?string $author_selector;
    /** href is author_url */
    public ?string $author_url_selector;

    /** url of host, like https://www.decaturvote.com */
    public string $host_url;




    /** Number of milliseconds to wait before downloading the next paginated page. Default is 100ms */
    public int $delay_ms_between_downloads = 100;

    /**
     * @param $source_data array<string key, string value> where keys are property names on this class. Extra keys (that don't map to properties) are allowed.
     */
    public function set_data(array $source_data){
        foreach ($source_data as $key=>$value){
            if (!property_exists($this, $key))continue;
            $this->$key = $value;
        }
    }

    public function get_data(): array {
        return get_object_vars($this);
    }

    public function get_parser(): ParserArticles {
        return new ParserArticles($this);
    }

    /** Get a string name that should be unique across all sources of the given version */
    public function get_unique_name(): string {
        return uniqid();
        //return $this->outlet_name.'-'.$this->feed_name.'-'.$this->url;
    }

    /** Get a human friendly string name that should be unique across all sources of the given version */
    public function get_unique_human_name(): string {
        return uniqid();
        //return $this->outlet_name.' - '.$this->feed_name;
    }


    /**
     * @param $parse_result
     * @param $is_initial_load bool whether this source has any cached scraper results, since you may want to scan more pages the first time yous crape a website.
     */
    public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load ): bool{
        return false;
    }

    /*
     * Set runtime options, such as from cli arguments
     *
     * @param $args array<string key, mixed value> 
     * @key page_depth int number of pages to do for pagination.
     */
    public function set_runtime_options(array $args){
    }
}
