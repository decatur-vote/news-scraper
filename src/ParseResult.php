<?php

namespace DecaturVote\NewsScraper;

/**
 * Gives details about a parsing event, like how many new articles there are.
 * Same ParseResult object is updated on each pagination loop
 * New ParseResult is instantiated for each source being parsed.
 */
class ParseResult {

    /** Which page is being / was just processed. 0 indicates initial page. */
    public int $pagination_index = -1;

    /** Total number of articles already cached before running the parser. */
    public int $original_articles_count = 0;

    /** Total number of articles for the given source, including previously stored articles */
    public int $total_article_count = 0;

    /** Total number of articles parsed, NOT including previously stored articles */
    public int $total_articles_parsed_count = 0;

    /** Total number of NEW articles found, across all pages. */
    public int $total_new_article_count = 0;

    /** Number of articles found on the current page */
    public int $num_articles_parsed = 0;

    /** Number of articles found on the current page and not previously stored */
    public int $num_new_articles_this_page = 0;

    /** 
     * Get a debug message
     * @return string message
     */
    public function debug_msg(): string {

        $str = "Page ". $this->pagination_index. " processed";
        $parts = [
                "Original Article Count: ".$this->original_articles_count,
                "Total Article Count: ".$this->total_article_count,
                "Total Articles Parsed: ".$this->total_articles_parsed_count,

                "Total NEW Articles: ".$this->total_new_article_count,

                "Articles Parsed this page: ". $this->num_articles_parsed,
                "New Articles this page: ".$this->num_new_articles_this_page,
        ];

        $msg = "\n$str\n  "
            .implode("\n  ", $parts);

        return $msg;
    }

}
