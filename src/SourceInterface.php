<?php

namespace DecaturVote\NewsScraper;

interface SourceInterface {

    public function set_data(array $source_data);
    /** Return a parser, initialized & ready to go. */
    public function get_parser(): ParserInterface;

    /** Get a string name that should be unique across all sources of the given version */
    public function get_unique_name(): string;

    /** Get a human friendly string name that should be unique across all sources of the given version */
    public function get_unique_human_name(): string;


    /**
     * Get the data necessary to re-construct this source.
     *
     * @return array<string key, mixed value> array of source data.
     */
    public function get_data(): array;

    /**
     * Check if the next page (in pagination) should be scraped. Processing stops if not. Only called after the first, main page url is processed.
     *
     * @param $parse_result \DecaturVote\NewsScraper\ParseResult results of the just-completed parsing operation.
     * @param $is_initial_load bool whether this source has any cached scraper results, since you may want to scan more pages the first time yous crape a website.
     *
     * @return bool true or false
     */
    public function should_scrape_next_page(ParseResult $parse_result, bool $is_initial_load): bool;

    /**
     * Set runtime options, such as from cli arguments
     *
     * @param $args array<string key, mixed value> 
     * @key page_depth int number of pages to do for pagination.
     */
    public function set_runtime_options(array $args);
    
}
