# Available Scraper Sources 
Currently, there is no system for tracking externally-available scraper sources, and there likely are none. We will address this at a later time.

**LEGAL DISCLAIMER:** I am not a lawyer. In some cases, scraping (and/or how you use the scraped data) may violate copyright law, contracts, or other law. YOU are responsible for following the law, and should probably consult a lawyer.   

## Also See
- [Run the Scraper](/docs/use/RunScraper.md) - Download and scrape article indexes & page data
- TODO [Add a news website feed]() - Configure a a search-results url and xpath queries to get article metadata.
- TODO [Add a simple php parser]() - Parse a page PHP script
- TODO [Add an article parser]() - Configure a host url, v1 feed name, and xpath queries to parse full articles.

## Built-in Sources
These sources are built-in to the current version of the scraper:
```bash
@system(bin/scrape list-sources, trim_empty_lines)
```
