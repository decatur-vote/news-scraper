# Run Decatur Vote's News Scraper
The News Scraper is easily run via CLI. 

## Also See
- [Find Sources](/docs/use/FindSources.md) - Find pre-existing sources to scrape.
- TODO [Add a news website feed]() - Configure a a search-results url and xpath queries to get article metadata.
- TODO [Add a simple php parser]() - Parse a page PHP script
- TODO [Add an article parser]() - Configure a host url, v1 feed name, and xpath queries to parse full articles.
- TODO [Scrape in PHP]() - Setup and run the Scraper with PHP instead of cli.

## Using the CLI
First git clone & `cd` into the root dir of this repo.

`bin/scrape help`
```bash
@system(bin/scrape, trim_empty_lines)
```

## Example
`bin/scrape decatur-vote/news`:
```bash
...
@system(bin/scrape decatur-vote/news, trim_empty_lines,last_x_lines,10)
```


