# Known Issues
Bugs & problems with the Decatur Vote News Scraper that we know about, and whether or not we plan to fix them.

## Will Fix
- php out of memory for large article datasets: I downloaded around 60,000 articles before receiving an out-of-memory error. Currently all articles are added to an array, so memory capacity must be increased.
- Currently, there is no system for tracking externally-available scraper sources. We will likely add a json file that tracks has urls for people's own lists of sources.

## Won't Fix
- (idk yet)

## Might Fix
- Sources list for decatur-vote-full shows uids
- Sources list does not show source version number, or adequate information in general.
